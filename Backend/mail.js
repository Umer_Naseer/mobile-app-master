const nodemailer = require("nodemailer");
// const hbs = require("nodemailer-handlebars");

const auth = {
        host: "smtp.zoho.eu",
        port: 465,
        secure: true, // true for 465, false for other ports
        auth: {
          user: "hello@lingafriq.com",
          pass: "Friqfriq2019."
        }
};

const transporter = nodemailer.createTransport(auth);

/* transporter.use('compile', hbs({
    viewEngine: 'express-handlebars',
    viewPath: './views/'
})) */

const sendMail = (email, language, callback) => {
    var mailList = [
        'admin@lingafriq.com'
    ];
    let mailOptions = {
    from: '"LingAfriq"<hello@lingafriq.com>', // sender address
    to: email + ',' + mailList, // list of receivers
    subject: "Welcome to LingAfriq! 👻", // Subject line
    html: `<h3>Hi there, ${language} !</h3><br>
    <p>this is Fabian from LingAfriq. We are delighted to have you here! Thank you for giving us a chance to provide you
    with a platform like no other, that takes your knowledge of Afrika to the next level.<br>
    <br>
    I would like to stay in touch with you and keep you updated on our progress to making this possible for you. You shall be getting
    updates that keep you in the loop, especially regarding the launch of the mobile app!<br>
    <br>
    Once again, I am excited to have you on board!<br>
    <br>
    Kind Regards,<br>
    <br>
    Fabian Umole<br>

    </p>

    <p>
    If you got this message in error, please send an email to hello@lingafriq.com
    </p>`

    };

    transporter.sendMail(mailOptions, function(err, data){
        if(err){
            callback(err, data);
        } else{
            callback(null, data);
        }
    });
};

module.exports = sendMail;
