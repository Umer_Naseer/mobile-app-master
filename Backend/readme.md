![code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)

## Technical info

#### This project is powered by:

- [Django](https://www.djangoproject.com/)
- [Django Rest Framework](https://www.django-rest-framework.org/)

 `/project` modules

 - **core** - global settings
 - **accounts** - models, configs and API views for authentication and account management in general
 - **lessons** - models, configs and partly API views for languages, sections, lessons and their content
 - **api** - configs and API views (both for admin and mobile) for languages, sections, lessons and their content
 - **templates** - utilitary folder
