const express = require('express');
const sendmail = require('./mail');
const bodyParser = require("body-parser");
const path = require('path');
const app = express();


const log = console.log;
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader(
      "Access-Control-Allow-Headers",
      "Origin, X-requested-With, Content-Type, Accept"
    );
    res.setHeader(
      "Access-Control-Allow-Methods",
      "GET, POST, PATCH, DELETE, OPTIONS"
    );
    next();
  
  })
  

const PORT = 8080;

app.post("/api/sendmail", (req, res) => {
    const {email, language} = req.body;
    console.log('User Data: ', req.body);

    sendmail(email, language, function(err, data) {
        if (err) {
            res.status(500).json({message: 'Server Error'});
            console.log("Message Error: ", data);
        } else {
            res.json({message: 'Email Sent!!'})
            console.log("Message Sent");
        }
    });
});

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'views', 'index.html'))
})

app.listen(PORT, () => {
    log(`Server running on port ${PORT}`);
});

module.exports = {
    app
};