from rest_framework import serializers
from lessons import models as lessons_models


class ChangeResourceSerializer(serializers.ModelSerializer):
    default_error_messages = {
        "too_much": "Сan be attached only one object",
        "empty": "Must be attached to image, audio or text.",
    }

    class Meta:
        model = lessons_models.LessonResource
        fields = ("id", "text", "audio", "image", "video", "index")

    def validate(self, attrs):
        count_none = sum(
            1
            for i in [
                attrs.get("text"),
                attrs.get("image"),
                attrs.get("audio"),
                attrs.get("video"),
            ]
            if i is not None and i != ""
        )
        if count_none > 1:
            self.fail("too_much")
        elif count_none < 1:
            self.fail("empty")
        return super().validate(attrs)


class ResourceSerializer(serializers.ModelSerializer):
    type_obj = serializers.ChoiceField(
        ["lesson", "question", "option"], write_only=True, required=False
    )
    id_obj = serializers.IntegerField(write_only=True, required=False)
    type_content = serializers.SerializerMethodField(read_only=True)
    content = serializers.SerializerMethodField(read_only=True)

    default_error_messages = {
        "too_much": "Сan be attached only one object",
        "empty": "Must be attached to image, audio or text.",
        "not_found": "Object is not found.",
    }

    def get_type_content(self, obj):
        if obj.audio.name:
            return "audio"
        if obj.video.name:
            return "video"
        elif obj.image.name:
            return "image"
        else:
            return "text"

    def get_content(self, obj):
        if obj.audio.name:
            return self.context.get("request").build_absolute_uri(obj.audio.url)
        if obj.video.name:
            return self.context.get("request").build_absolute_uri(obj.video.url)
        elif obj.image.name:
            return self.context.get("request").build_absolute_uri(obj.image.url)
        else:
            return obj.text

    class Meta:
        model = lessons_models.LessonResource
        fields = (
            "id",
            "text",
            "audio",
            "image",
            "video",
            "type_obj",
            "id_obj",
            "type_content",
            "content",
            "index",
        )
        extra_kwargs = {
            "id": {"read_only": True, "required": False},
            "text": {"write_only": True, "required": False},
            "image": {"write_only": True, "required": False},
            "audio": {"write_only": True, "required": False},
            "video": {"write_only": True, "required": False},
        }

    def validate(self, attrs):
        type_model = {
            "lesson": lessons_models.Lesson,
            "question": lessons_models.LessonMessage,
            "option": lessons_models.MessageOption,
        }
        if (
            type_model[attrs["type_obj"]].objects.filter(id=attrs["id_obj"]).exists()
            is False
        ):
            self.fail("not_found")
        count_none = sum(
            1
            for i in [
                attrs.get("text"),
                attrs.get("image"),
                attrs.get("audio"),
                attrs.get("video"),
            ]
            if i is not None and i != ""
        )
        if count_none > 1:
            self.fail("too_much")
        elif count_none < 1:
            self.fail("empty")
        return super().validate(attrs)

    def create(self, validated_data):
        validated_data.update(
            {f'{validated_data.pop("type_obj")}_id': validated_data.pop("id_obj")}
        )
        res = lessons_models.LessonResource(**validated_data)
        res.save()
        return res


class LanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = lessons_models.Language
        fields = ("id", "name", "background", "description")


class SectionListSerializer(serializers.ModelSerializer):
    class Meta:
        model = lessons_models.Section
        fields = (
            "id",
            "name",
        )


class SectionPageSerializer(serializers.ModelSerializer):
    class Meta:
        model = lessons_models.SectionPage
        fields = (
            "id",
            "message",
            "page_number",
            "congrats",
        )
        extra_kwargs = {"id": {"read_only": False, "required": False}}


class SectionSerializer(serializers.ModelSerializer):
    pages = SectionPageSerializer(many=True)

    class Meta:
        model = lessons_models.Section
        fields = (
            "id",
            "name",
            "pages",
        )

    def update(self, instance, validated_data):
        instance.name = validated_data.get("name", instance.name)
        instance.save()
        pages = validated_data.get("pages")
        for page in pages:
            page_id = page.get("id", None)
            if not page_id:
                raise serializers.ValidationError({"id": ["This field is required"]})
            try:
                obj = instance.pages.get(id=page_id)
            except lessons_models.SectionPage.DoesNotExist:
                raise serializers.ValidationError({"id": ["This Page does not exist"]})
            obj.message = page.get("message", obj.message)
            obj.save()
        return instance


class LessonListSerializer(serializers.ModelSerializer):
    class Meta:
        model = lessons_models.Lesson
        fields = (
            "id",
            "name",
            "ready",
            "is_published"
            # 'lesson_type',
        )


class MessageOption(serializers.ModelSerializer):
    option_resources = ResourceSerializer(many=True, read_only=True, required=False)

    class Meta:
        model = lessons_models.MessageOption
        fields = ("id", "message", "correct_answer", "option_resources")
        extra_kwargs = {
            "id": {"read_only": True, "required": False},
            "message": {"required": False},
            "correct_answer": {"required": False},
        }


class LessonMessageSerializer(serializers.ModelSerializer):
    options = MessageOption(many=True, read_only=True, required=False)
    question_resources = ResourceSerializer(many=True, read_only=True, required=False)

    class Meta:
        model = lessons_models.LessonMessage
        fields = ("id", "message", "hint", "options", "question_resources")
        extra_kwargs = {
            "id": {"read_only": True, "required": False},
            "message": {"required": False},
            "hint": {"required": False},
        }


class LessonSerializer(serializers.ModelSerializer):
    messages = LessonMessageSerializer(many=True, read_only=True)
    lesson_resources = serializers.SerializerMethodField(
        read_only=True
    )  # ResourceSerializer(many=True, read_only=True)
    lesson_type_display = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = lessons_models.Lesson
        fields = (
            "id",
            "name",
            "ready",
            "lesson_type",
            "lesson_type_display",
            "messages",
            "info_message",
            "lesson_resources",
            "fun_fact",
            "fun_fact_image",
            "is_published",
        )
        extra_kwargs = {"ready": {"required": False}}

    def validate(self, attrs):
        fun_fact = attrs.get("fun_fact")
        if fun_fact is not None and all(
            True if i["text"].replace(" ", "") == "" else False
            for i in fun_fact["blocks"]
        ):
            attrs["fun_fact"] = None
        return super().validate(attrs)

    def get_lesson_type_display(self, obj):
        return obj.get_lesson_type_display()

    def get_lesson_resources(self, object):
        return ResourceSerializer(
            object.lesson_resources.all().order_by("index"),
            many=True,
            context=self.context,
        ).data


class IndexOrderResourceSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    index = serializers.IntegerField()


class IndexResourceSerializer(serializers.Serializer):
    type_obj = serializers.ChoiceField(["lesson", "question", "option"])
    id_obj = serializers.IntegerField()
    list_index = IndexOrderResourceSerializer(many=True)

    default_error_messages = {"not_found": "Object is not found."}

    def validate(self, attrs):
        type_model = {
            "lesson": lessons_models.Lesson,
            "question": lessons_models.LessonMessage,
            "option": lessons_models.MessageOption,
        }
        if (
            type_model[attrs["type_obj"]].objects.filter(id=attrs["id_obj"]).exists()
            is False
        ):
            self.fail("not_found")
        return super(IndexResourceSerializer, self).validate(attrs)
