from rest_framework import generics, status, views
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination
from django.db.models import Prefetch
from django.shortcuts import get_object_or_404

from core.permissions import IsAdmin

from . import serializers
from lessons import models as lessons_models


class LessonCreateListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated, IsAdmin)
    serializer_class = serializers.LessonListSerializer

    def get_object(self):
        return get_object_or_404(lessons_models.Section, pk=self.kwargs["pk"])

    def get_queryset(self):
        section = self.get_object()
        return lessons_models.Lesson.objects.filter(section_id=section.pk).order_by(
            "index"
        )

    def post(self, request, *args, **kwargs):
        lesson = lessons_models.Lesson.objects.create(section=self.get_object())
        serializer = serializers.LessonSerializer(lesson)
        return Response(serializer.data)


class LessonView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated, IsAdmin)
    serializer_class = serializers.LessonSerializer

    def get_queryset(self):
        return (
            lessons_models.Lesson.objects.all()
            .order_by("index")
            .prefetch_related(
                Prefetch(
                    "messages",
                    queryset=lessons_models.LessonMessage.objects.all().prefetch_related(
                        "options"
                    ),
                )
            )
        )


class AddMediaView(generics.CreateAPIView):
    permission_classes = (IsAuthenticated, IsAdmin)
    serializer_class = serializers.ResourceSerializer


class IndexMediaView(generics.CreateAPIView):
    permission_classes = (IsAuthenticated, IsAdmin)
    serializer_class = serializers.ResourceSerializer


class MediaView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated, IsAdmin)
    serializer_class = serializers.ChangeResourceSerializer
    queryset = lessons_models.LessonResource.objects.all()

    def get_object(self):
        return get_object_or_404(lessons_models.LessonResource, pk=self.kwargs["pk"])

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = serializers.ResourceSerializer(
            instance, context={"request": request}
        )
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop("partial", False)
        instance = self.get_object()
        if partial:
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            for k, v in serializer.data.items():
                if v is not None or k in ["audio", "image", "text", "video"]:
                    setattr(instance, k, v)
            instance.save()
        else:
            serializer = self.get_serializer(
                instance, data=request.data, partial=partial
            )
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            if getattr(instance, "_prefetched_objects_cache", None):
                instance._prefetched_objects_cache = {}
        serializer = serializers.ResourceSerializer(
            instance, context={"request": request}
        )
        return Response(serializer.data)


class MediaIndexView(generics.GenericAPIView):
    permission_classes = (IsAuthenticated, IsAdmin)
    serializer_class = serializers.IndexResourceSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        for el in serializer.data["list_index"]:
            q = lessons_models.LessonResource.objects.filter(id=el["id"])
            if q.exists():
                t = q[0]
                t.index = el["index"]
                t.save()
        return Response({"ok": True})


class ListCreateQuestionView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated, IsAdmin)
    serializer_class = serializers.LessonMessageSerializer

    def get_object(self):
        return get_object_or_404(lessons_models.Lesson, pk=self.kwargs["pk"])

    def get_queryset(self):
        lesson = self.get_object()
        return lessons_models.LessonMessage.objects.filter(lesson=lesson)

    def post(self, request, *args, **kwargs):
        question = lessons_models.LessonMessage.objects.create(lesson=self.get_object())
        serializer = self.get_serializer(question)
        return Response(serializer.data)


class QuestionView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated, IsAdmin)
    serializer_class = serializers.LessonMessageSerializer
    queryset = lessons_models.LessonMessage.objects.all()

    def get_object(self):
        return get_object_or_404(lessons_models.LessonMessage, pk=self.kwargs["pk"])

    # def get_queryset(self):
    #     lesson = self.get_object()
    #     return lessons_models.LessonMessage.objects.filter(lesson=lesson)


class ListCreateOptionView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated, IsAdmin)
    serializer_class = serializers.MessageOption

    def get_object(self):
        return get_object_or_404(lessons_models.LessonMessage, pk=self.kwargs["pk"])

    def get_queryset(self):
        lesson_message = self.get_object()
        return lessons_models.MessageOption.objects.filter(
            lesson_message=lesson_message
        )

    def post(self, request, *args, **kwargs):
        option = lessons_models.MessageOption.objects.create(
            lesson_message=self.get_object()
        )
        serializer = self.get_serializer(option)
        return Response(serializer.data)


class OptionView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated, IsAdmin)
    serializer_class = serializers.MessageOption

    def get_queryset(self):
        return lessons_models.MessageOption.objects.all()


class SectionIndexView(generics.GenericAPIView):
    permission_classes = (IsAuthenticated, IsAdmin)
    serializer_class = serializers.IndexOrderResourceSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, many=True)
        serializer.is_valid(raise_exception=True)
        for el in serializer.data:
            q = lessons_models.Section.objects.filter(id=el["id"])
            if q.exists():
                t = q[0]
                t.index = el["index"]
                t.save()
        return Response({"ok": True})


class LessonIndexView(generics.GenericAPIView):
    permission_classes = (IsAuthenticated, IsAdmin)
    serializer_class = serializers.IndexOrderResourceSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, many=True)
        serializer.is_valid(raise_exception=True)
        for el in serializer.data:
            q = lessons_models.Lesson.objects.filter(id=el["id"])
            if q.exists():
                t = q[0]
                t.index = el["index"]
                t.save()
        return Response({"ok": True})


class LessonClearMediaView(views.APIView):
    permission_classes = (IsAuthenticated, IsAdmin)

    def delete(self, request, *args, **kwargs):
        less = get_object_or_404(lessons_models.Lesson, pk=self.kwargs["pk"])
        less.lesson_resources.clear()
        return Response({"ok": True})
