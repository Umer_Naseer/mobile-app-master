from rest_framework.generics import RetrieveAPIView
from rest_framework.permissions import IsAuthenticated
from lessons.models import Section
from .serializers import SectionActiveLessonsSerializer


class SectionActiveLessonsAPIView(RetrieveAPIView):
    """All active lessons for chosen section"""

    queryset = Section.objects.all().order_by("index")
    serializer_class = SectionActiveLessonsSerializer
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        # if self.get_object() not in request.user.sections.all():
        #     request.user.sections.add(self.get_object())
        return super().get(request, *args, **kwargs)
