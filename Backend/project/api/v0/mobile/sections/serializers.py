from rest_framework import serializers
from lessons.models import Section


class ActiveSectionSerializers(serializers.ModelSerializer):
    complete = serializers.SerializerMethodField()

    class Meta:
        model = Section
        fields = ["id", "name", "complete"]

    def get_complete(self, obj):
        try:
            return round(
                self.context["request"]
                .user.lessons.filter(ready=True, lesson__section=obj)
                .count()
                / obj.lessons.filter(ready=True).count()
                * 100
            )
        except ZeroDivisionError:
            return 0
        # return 1 if object in self.context['request'].user.sections.all() else 0


class SectionShortSerializers(serializers.ModelSerializer):
    class Meta:
        model = Section
        fields = ["id", "name"]


class SectionActiveLessonsSerializer(serializers.ModelSerializer):
    lessons = serializers.SerializerMethodField()

    class Meta:
        model = Section
        fields = [
            "name",
            "lessons",
        ]

    def get_lessons(self, obj):
        return ["TODO: lessons"]
