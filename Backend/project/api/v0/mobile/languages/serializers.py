from rest_framework import serializers
from lessons.models import Language, StudentLanguage
from api.v0.mobile.sections.serializers import (
    ActiveSectionSerializers,
    SectionShortSerializers,
)


class ActiveLanguageSerializer(serializers.ModelSerializer):
    in_active = serializers.SerializerMethodField()

    class Meta:
        model = Language
        fields = [
            "id",
            "name",
            "background",
            "description",
            "in_active",
        ]

    def get_in_active(self, obj):
        return self.context.get("request").user.languages.filter(language=obj).exists()


class ShortLanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Language
        fields = [
            "id",
            "name",
        ]


class FullLanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Language
        fields = [
            "id",
            "name",
            "background",
            "description",
        ]


class LanguageActiveSectionSerializer(serializers.ModelSerializer):
    sections = ActiveSectionSerializers(many=True)

    class Meta:
        model = Language
        fields = ["name", "sections"]


class LanguageSectionSerializer(serializers.ModelSerializer):
    # sections = SectionShortSerializers(many=True)
    sections = serializers.SerializerMethodField()

    class Meta:
        model = Language
        fields = ["name", "sections"]

    def get_sections(self, obj):
        return SectionShortSerializers(
            obj.sections.all().order_by("id"), many=True
        ).data
