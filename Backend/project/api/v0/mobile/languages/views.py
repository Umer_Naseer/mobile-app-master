from rest_framework.generics import ListAPIView, RetrieveAPIView, GenericAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from lessons.models import Language, Lesson, StudentLanguage
from .serializers import ActiveLanguageSerializer, FullLanguageSerializer
from api.v0.mobile.languages.serializers import (
    LanguageActiveSectionSerializer,
    LanguageSectionSerializer,
)
from django.shortcuts import get_object_or_404
from django.db.models import Count


class ActiveLanguagesAPIView(ListAPIView):
    """Active languages for current user """

    serializer_class = ActiveLanguageSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return Language.objects.filter(
            students__in=self.request.user.languages.all(), is_published=True,
        )


class AllLanguagesAPIView(ListAPIView):
    """All languages in application """

    queryset = Language.objects.annotate(
        lessons=Count("sections__lessons", is_published=True, ready=True)
    ).filter(is_published=True, lessons__gt=0)
    serializer_class = ActiveLanguageSerializer
    permission_classes = (IsAuthenticated,)


class LanguageIntroAPIView(RetrieveAPIView):
    """Introduction for a new language"""

    queryset = Language.objects.filter(is_published=True,)
    # serializer_class = FullLanguageSerializer
    serializer_class = ActiveLanguageSerializer
    permission_classes = (IsAuthenticated,)


class LanguageActiveSectionsAPIView(RetrieveAPIView):
    """All sections with progress for chosen active language"""

    serializer_class = LanguageActiveSectionSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return Language.objects.filter(
            students__in=self.request.user.languages.all(), is_published=True,
        )


class LanguageSectionsAPIView(RetrieveAPIView):
    """All sections for chosen language"""

    # queryset = Language.objects.all()
    queryset = Language.objects.filter(
        id__in=set(
            Lesson.objects.filter(ready=True, is_published=True).values_list(
                "section__language", flat=True
            )
        ),
        is_published=True,
    )
    serializer_class = LanguageSectionSerializer
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        # if self.get_object() not in request.user.languages.all():
        #     request.user.languages.add(self.get_object())
        return super().get(request, *args, **kwargs)
