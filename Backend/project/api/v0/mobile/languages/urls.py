from django.urls import path

from .views import (
    ActiveLanguagesAPIView,
    AllLanguagesAPIView,
    LanguageIntroAPIView,
    LanguageActiveSectionsAPIView,
    LanguageSectionsAPIView,
)

urlpatterns = [
    # path('active/', ActiveLanguagesAPIView.as_view()),
    path("", AllLanguagesAPIView.as_view()),
    path("<int:pk>/", LanguageIntroAPIView.as_view()),
    # path('<int:pk>/active-sections/', LanguageActiveSectionsAPIView.as_view()),
    path("<int:pk>/sections/", LanguageSectionsAPIView.as_view()),
]
