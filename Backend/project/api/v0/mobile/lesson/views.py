from rest_framework import generics, status, views
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from django.db.models import Prefetch
from django.shortcuts import get_object_or_404

from . import serializers
from lessons import models as lessons_models


class MobileLessonListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.MobileLessonListSerializer

    def get_object(self):
        return get_object_or_404(lessons_models.Section, pk=self.kwargs["pk"])

    def get_queryset(self):
        section = self.get_object()
        return lessons_models.Lesson.objects.filter(
            section_id=section.pk, ready=True, is_published=True,
        ).order_by("index")


class MobileLessonView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.MobileLessonSerializer

    def get_queryset(self):
        return lessons_models.Lesson.objects.filter(is_published=True).prefetch_related(
            Prefetch(
                "messages",
                queryset=lessons_models.LessonMessage.objects.all().prefetch_related(
                    "options"
                ),
            )
        )


class MobileMediaView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.MobileChangeResourceSerializer
    queryset = lessons_models.LessonResource.objects.all()

    def get_object(self):
        return get_object_or_404(lessons_models.LessonResource, pk=self.kwargs["pk"])

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = serializers.MobileResourceSerializer(
            instance, context={"request": request}
        )
        return Response(serializer.data)


class MobileListQuestionView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.MobileLessonMessageSerializer

    def get_object(self):
        return get_object_or_404(lessons_models.Lesson, pk=self.kwargs["pk"])

    def get_queryset(self):
        lesson = self.get_object()
        return lessons_models.LessonMessage.objects.filter(
            lesson=lesson, lesson__ready=True
        )


class MobileQuestionView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.MobileLessonMessageSerializer

    def get_object(self):
        return get_object_or_404(lessons_models.LessonMessage, pk=self.kwargs["pk"])

    def get_queryset(self):
        return lessons_models.LessonMessage.objects.filter(
            lesson=self.get_object(), lesson__ready=True
        )


class MobileListOptionView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.MobileMessageOption

    def get_object(self):
        return get_object_or_404(lessons_models.LessonMessage, pk=self.kwargs["pk"])

    def get_queryset(self):
        lesson_message = self.get_object()
        return lessons_models.MessageOption.objects.filter(
            lesson_message=lesson_message, lesson_message__lesson__ready=True
        )


class MobileOptionView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.MobileMessageOption

    def get_object(self):
        return get_object_or_404(lessons_models.MessageOption, pk=self.kwargs["pk"])

    def get_queryset(self):
        lesson_message = self.get_object()
        return lessons_models.MessageOption.objects.filter(
            lesson_message=lesson_message, lesson_message__lesson__ready=True
        )


class MobileActiveLanguagesAPIView(generics.ListAPIView):
    serializer_class = serializers.MobileActiveLanguageSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return lessons_models.Language.objects.filter(
            students__in=self.request.user.languages.all(), is_published=True,
        )


class MobileLanguageActiveChangeView(generics.GenericAPIView):
    serializer_class = serializers.MobileActiveLanguageSerializer
    queryset = lessons_models.Language.objects
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return get_object_or_404(lessons_models.Language, pk=self.kwargs["pk"])

    def post(self, request, *args, **kwargs):
        lang = self.get_object()
        user = self.request.user
        instance, created = lessons_models.StudentLanguage.objects.get_or_create(
            language=lang, student=user
        )
        if created:
            return Response(self.get_serializer(lang).data)
        else:
            return Response(
                {"id": ["Language have already been added"]},
                status.HTTP_400_BAD_REQUEST,
            )

    def delete(self, request, *args, **kwargs):
        instance = get_object_or_404(
            lessons_models.StudentLanguage,
            language=self.get_object(),
            student=self.request.user,
        )
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class MobileActiveLanguageView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.MobileActiveLanguageSerializer

    def get_queryset(self):
        user = self.request.user
        return lessons_models.Language.objects.filter(
            is_published=True,
            id__in=user.languages.all()
            .order_by("index")
            .values_list("language", flat=True),
        )


class MobileActiveSectionView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.MobileActiveSectionSerializers

    def get_object(self):
        return get_object_or_404(
            lessons_models.StudentLanguage,
            language_id=self.kwargs["pk"],
            student=self.request.user,
        ).language

    def get_queryset(self):
        from django.db.models import Count

        return (
            self.get_object()
            .sections.annotate(lessons_count=Count("lessons", is_published=True))
            .filter(lessons_count__gte=1)
            .order_by("index")
        )

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        return Response({"name": self.get_object().name, "sections": serializer.data})


class MobileActiveLessonListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.MobileActiveLessonListSerializer

    def get_object(self):
        return get_object_or_404(lessons_models.Section, pk=self.kwargs["pk"])

    def get_queryset(self):
        section = self.get_object()
        return lessons_models.Lesson.objects.filter(
            section_id=section.pk, ready=True, is_published=True,
        ).order_by("index")

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        return Response({"name": self.get_object().name, "lessons": serializer.data})


class MobileActiveLessonView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.MobileActiveLessonSerializer
    queryset = lessons_models.Lesson.objects.all()

    def get_object(self):
        return get_object_or_404(lessons_models.Lesson, pk=self.kwargs["pk"])

    def post(self, request, *args, **kwargs):
        instance, created = lessons_models.StudentLessonProgress.objects.get_or_create(
            lesson=self.get_object(), student=self.request.user
        )
        if created:
            return Response(self.get_serializer(self.get_object()).data)
        else:
            return Response(
                {"id": ["Lesson already done."]}, status.HTTP_400_BAD_REQUEST
            )

    def delete(self, request, *args, **kwargs):
        instance = get_object_or_404(
            lessons_models.StudentLessonProgress,
            lesson=self.get_object(),
            student=self.request.user,
        )
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
