from rest_framework import serializers
from lessons import models as lessons_models
from lessons.serializers import SectionPageSerializer


class MobileChangeResourceSerializer(serializers.ModelSerializer):
    default_error_messages = {
        "too_much": "Сan be attached only one object",
        "empty": "Must be attached to image, audio or text.",
    }

    class Meta:
        model = lessons_models.LessonResource
        fields = ("id", "text", "audio", "image", "video", "index")

    def validate(self, attrs):
        count_none = sum(
            1
            for i in [
                attrs.get("text"),
                attrs.get("image"),
                attrs.get("audio"),
                attrs.get("video"),
            ]
            if i is not None and i != ""
        )
        if count_none > 1:
            self.fail("too_much")
        elif count_none < 1:
            self.fail("empty")
        return super().validate(attrs)


class MobileResourceSerializer(serializers.ModelSerializer):
    type_obj = serializers.ChoiceField(
        ["lesson", "question", "option"], write_only=True, required=False
    )
    id_obj = serializers.IntegerField(write_only=True, required=False)
    type_content = serializers.SerializerMethodField(read_only=True)
    content = serializers.SerializerMethodField(read_only=True)

    default_error_messages = {
        "too_much": "Сan be attached only one object",
        "empty": "Must be attached to image, audio or text.",
        "not_found": "Object is not found.",
    }

    def get_type_content(self, obj):
        if obj.audio.name:
            return "audio"
        if obj.video.name:
            return "video"
        elif obj.image.name:
            return "image"
        else:
            return "text"

    def get_content(self, obj):
        if obj.audio.name:
            return self.context.get("request").build_absolute_uri(obj.audio.url)
        if obj.video.name:
            return self.context.get("request").build_absolute_uri(obj.video.url)
        elif obj.image.name:
            return self.context.get("request").build_absolute_uri(obj.image.url)
        else:
            return obj.text

    class Meta:
        model = lessons_models.LessonResource
        ordering = ("index",)
        fields = (
            "id",
            "text",
            "audio",
            "video",
            "image",
            "type_obj",
            "id_obj",
            "type_content",
            "content",
            "index",
        )
        extra_kwargs = {
            "id": {"read_only": True, "required": False},
            "text": {"write_only": True, "required": False},
            "image": {"write_only": True, "required": False},
            "audio": {"write_only": True, "required": False},
            "video": {"write_only": True, "required": False},
        }

    def validate(self, attrs):
        type_model = {
            "lesson": lessons_models.Lesson,
            "question": lessons_models.LessonMessage,
            "option": lessons_models.MessageOption,
        }
        if (
            type_model[attrs["type_obj"]].objects.filter(id=attrs["id_obj"]).exists()
            is False
        ):
            self.fail("not_found")
        count_none = sum(
            1
            for i in [
                attrs.get("text"),
                attrs.get("image"),
                attrs.get("audio"),
                attrs.get("video"),
            ]
            if i is not None and i != ""
        )
        if count_none > 1:
            self.fail("too_much")
        elif count_none < 1:
            self.fail("empty")
        return super().validate(attrs)

    def create(self, validated_data):
        validated_data.update(
            {f'{validated_data.pop("type_obj")}_id': validated_data.pop("id_obj")}
        )
        res = lessons_models.LessonResource(**validated_data)
        res.save()
        return res


class MobileLanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = lessons_models.Language
        fields = ("id", "name", "background", "description")


class MobileSectionListSerializer(serializers.ModelSerializer):
    class Meta:
        model = lessons_models.Section
        fields = (
            "id",
            "name",
        )


class MobileSectionPageSerializer(serializers.ModelSerializer):
    class Meta:
        model = lessons_models.SectionPage
        fields = (
            "id",
            "message",
            "image",
            "audio",
            "video",
            "page_number",
            "congrats",
        )
        extra_kwargs = {"id": {"read_only": False, "required": False}}


class MobileSectionSerializer(serializers.ModelSerializer):
    pages = MobileSectionPageSerializer(many=True)

    class Meta:
        model = lessons_models.Section
        fields = (
            "id",
            "name",
            "pages",
        )

    def update(self, instance, validated_data):
        instance.name = validated_data.get("name", instance.name)
        instance.save()
        pages = validated_data.get("pages")
        for page in pages:
            page_id = page.get("id", None)
            if not page_id:
                raise serializers.ValidationError({"id": ["This field is required"]})
            try:
                obj = instance.pages.get(id=page_id)
            except lessons_models.SectionPage.DoesNotExist:
                raise serializers.ValidationError({"id": ["This Page does not exist"]})
            obj.message = page.get("message", obj.message)
            obj.save()
        return instance


class MobileLessonListSerializer(serializers.ModelSerializer):
    class Meta:
        model = lessons_models.Lesson
        fields = (
            "id",
            "name",
            # 'lesson_type',
        )


class MobileMessageOption(serializers.ModelSerializer):
    option_resources = MobileResourceSerializer(
        many=True, read_only=True, required=False
    )

    class Meta:
        model = lessons_models.MessageOption
        fields = ("id", "message", "correct_answer", "option_resources")
        extra_kwargs = {
            "id": {"read_only": True, "required": False},
            "message": {"required": False},
            "correct_answer": {"required": False},
        }


class MobileLessonMessageSerializer(serializers.ModelSerializer):
    options = MobileMessageOption(many=True, read_only=True, required=False)
    question_resources = MobileResourceSerializer(
        many=True, read_only=True, required=False
    )

    class Meta:
        model = lessons_models.LessonMessage
        fields = ("id", "message", "hint", "options", "question_resources")
        extra_kwargs = {
            "id": {"read_only": True, "required": False},
            "message": {"required": False},
            "hint": {"required": False},
        }


class MobileLessonSerializer(serializers.ModelSerializer):
    messages = MobileLessonMessageSerializer(many=True, read_only=True)
    lesson_resources = MobileResourceSerializer(many=True, read_only=True)
    lesson_type_display = serializers.SerializerMethodField(read_only=True)
    next = serializers.SerializerMethodField(read_only=True)
    section_id = serializers.SerializerMethodField(read_only=True)
    language_id = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = lessons_models.Lesson
        fields = (
            "id",
            "name",
            "lesson_type",
            "lesson_type_display",
            "messages",
            "info_message",
            "lesson_resources",
            "fun_fact",
            "fun_fact_image",
            "next",
            "section_id",
            "language_id",
        )

    def get_section_id(self, obj):
        return obj.section.id

    def get_language_id(self, obj):
        return obj.section.language.id

    def get_lesson_type_display(self, obj):
        return obj.get_lesson_type_display()

    def get_next(self, object):
        next_qs = lessons_models.Lesson.objects.filter(
            index__gt=object.index,
            section=object.section,
            ready=True,
            is_published=True,
        ).order_by("index")
        if next_qs.exists():
            return next_qs.first().id
        else:
            q = object.section.pages.all().order_by("page_number")
            if q.count() == 3 and all(
                [True if (i["text"] == "") else False for i in q[1].message["blocks"]]
                + [
                    False if q[1].image.name else True,
                    False if q[1].audio.name else True,
                    False if q[1].video.name else True,
                ]
            ):
                q = q.exclude(id=q[1].id)
            return SectionPageSerializer(q, many=True, context=self.context).data


class MobileActiveLessonListSerializer(serializers.ModelSerializer):
    complete = serializers.SerializerMethodField(read_only=True)
    lesson_type_display = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = lessons_models.Lesson
        fields = ("id", "name", "lesson_type", "lesson_type_display", "complete")

    def get_lesson_type_display(self, obj):
        return obj.get_lesson_type_display()

    def get_complete(self, obj):
        return lessons_models.StudentLessonProgress.objects.filter(
            student=self.context.get("request").user, lesson=obj
        ).exists()


class MobileActiveLessonSerializer(serializers.ModelSerializer):
    complete = serializers.SerializerMethodField(read_only=True)
    messages = MobileLessonMessageSerializer(many=True, read_only=True)
    lesson_resources = MobileResourceSerializer(many=True, read_only=True)
    lesson_type_display = serializers.SerializerMethodField(read_only=True)
    next = serializers.SerializerMethodField(read_only=True)
    section_id = serializers.SerializerMethodField(read_only=True)
    language_id = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = lessons_models.Lesson
        fields = (
            "id",
            "name",
            "lesson_type",
            "lesson_type_display",
            "messages",
            "info_message",
            "lesson_resources",
            "complete",
            "fun_fact",
            "fun_fact_image",
            "next",
            "section_id",
            "language_id",
        )
        extra_kwargs = {
            "id": {"read_only": True, "required": False},
            "lesson_type": {"read_only": True, "required": False},
            "info_message": {"read_only": True, "required": False},
            "name": {"read_only": True, "required": False},
            "messages": {"read_only": True, "required": False},
            "fun_fact": {"read_only": True, "required": False},
            "fun_fact_image": {"read_only": True, "required": False},
        }

    def get_section_id(self, obj):
        return obj.section.id

    def get_language_id(self, obj):
        return obj.section.language.id

    def get_complete(self, obj):
        return lessons_models.StudentLessonProgress.objects.filter(
            student=self.context.get("request").user, lesson=obj
        ).exists()

    def get_lesson_type_display(self, obj):
        return obj.get_lesson_type_display()

    def get_next(self, object):
        next_qs = lessons_models.Lesson.objects.filter(
            index__gt=object.index,
            section=object.section,
            ready=True,
            is_published=True,
        ).order_by("index")
        if next_qs.exists():
            return next_qs.first().id
        else:
            q = object.section.pages.all().order_by("page_number")
            if q.count() == 3 and all(
                [True if (i["text"] == "") else False for i in q[1].message["blocks"]]
                + [
                    False if q[1].image.name else True,
                    False if q[1].audio.name else True,
                    False if q[1].video.name else True,
                ]
            ):
                q = q.exclude(id=q[1].id)
            return SectionPageSerializer(q, many=True, context=self.context).data


class MobileIndexOrderResourceSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    index = serializers.IntegerField()


class MobileIndexResourceSerializer(serializers.Serializer):
    type_obj = serializers.ChoiceField(["lesson", "question", "option"])
    id_obj = serializers.IntegerField()
    list_index = MobileIndexOrderResourceSerializer(many=True)

    default_error_messages = {"not_found": "Object is not found."}

    def validate(self, attrs):
        type_model = {
            "lesson": lessons_models.Lesson,
            "question": lessons_models.LessonMessage,
            "option": lessons_models.MessageOption,
        }
        if (
            type_model[attrs["type_obj"]].objects.filter(id=attrs["id_obj"]).exists()
            is False
        ):
            self.fail("not_found")
        super(MobileIndexResourceSerializer, self).validate(attrs)


class MobileActiveLanguageSerializer(serializers.ModelSerializer):
    complete = serializers.SerializerMethodField()

    class Meta:
        model = lessons_models.Language
        fields = ["id", "name", "background", "complete"]

    def get_complete(self, obj):
        try:
            return round(
                self.context["request"]
                .user.lessons.filter(
                    lesson__ready=True,
                    lesson__is_published=True,
                    lesson__section__language=obj,
                )
                .count()
                / lessons_models.Lesson.objects.filter(
                    ready=True, is_published=True, section__language=obj
                ).count()
                * 100
            )
        except ZeroDivisionError:
            return 0


class MobileActiveSectionSerializers(serializers.ModelSerializer):
    complete = serializers.SerializerMethodField()
    pages = serializers.SerializerMethodField()
    # pages = SectionPageSerializer(many=True, )

    class Meta:
        model = lessons_models.Section
        fields = ["id", "name", "complete", "pages"]

    def get_pages(self, obj):

        q = obj.pages.all().order_by("page_number")
        if q.count() == 3 and all(
            [True if (i["text"] == "") else False for i in q[1].message["blocks"]]
            + [
                False if q[1].image.name else True,
                False if q[1].audio.name else True,
                False if q[1].video.name else True,
            ]
        ):
            q = q.exclude(id=q[1].id)
        return SectionPageSerializer(q, many=True, context=self.context).data

    def get_complete(self, obj):
        try:
            return round(
                self.context["request"]
                .user.lessons.filter(
                    lesson__ready=True, lesson__is_published=True, lesson__section=obj
                )
                .count()
                / obj.lessons.filter(ready=True, is_published=True).count()
                * 100
            )
        except ZeroDivisionError:
            return 0
