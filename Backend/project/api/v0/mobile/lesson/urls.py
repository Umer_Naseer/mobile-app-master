from django.urls import path

from api.v0.mobile.lesson import views as lessons_view


urlpatterns = [
    # path('section/<int:pk>/lesson/', lessons_view.MobileLessonListView.as_view()),
    path("sections/<int:pk>/lessons/", lessons_view.MobileLessonListView.as_view()),
    path("lesson/<int:pk>/", lessons_view.MobileLessonView.as_view()),
    path("lesson/<int:pk>/questions/", lessons_view.MobileListQuestionView.as_view()),
    path("question/<int:pk>/", lessons_view.MobileQuestionView.as_view()),
    path("question/<int:pk>/option/", lessons_view.MobileListOptionView.as_view()),
    path("option/<int:pk>/", lessons_view.MobileOptionView.as_view()),
    path("resource/<int:pk>/", lessons_view.MobileMediaView.as_view()),
    # active languages region
    path("active/languages/", lessons_view.MobileActiveLanguageView.as_view()),
    path(
        "active/language/<int:pk>/",
        lessons_view.MobileLanguageActiveChangeView.as_view(),
    ),
    path(
        "active/language/<int:pk>/sections/",
        lessons_view.MobileActiveSectionView.as_view(),
    ),
    path(
        "active/section/<int:pk>/lessons/",
        lessons_view.MobileActiveLessonListView.as_view(),
    ),
    path("active/lesson/<int:pk>/", lessons_view.MobileActiveLessonView.as_view()),
    path(
        "active/lesson/<int:pk>/questions/",
        lessons_view.MobileListQuestionView.as_view(),
    ),
    path("active/question/<int:pk>/", lessons_view.MobileQuestionView.as_view()),
    path(
        "active/question/<int:pk>/options/", lessons_view.MobileListOptionView.as_view()
    ),
    path("active/option/<int:pk>/", lessons_view.MobileOptionView.as_view()),
    path("active/resource/<int:pk>/", lessons_view.MobileMediaView.as_view()),
]
