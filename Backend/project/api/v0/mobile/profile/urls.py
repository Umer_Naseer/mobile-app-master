from django.urls import path

from .views import ProfileEmailAPIView, ChangePasswordView

urlpatterns = [
    path("email/", ProfileEmailAPIView.as_view()),
    path("change-password/", ChangePasswordView.as_view()),
]
