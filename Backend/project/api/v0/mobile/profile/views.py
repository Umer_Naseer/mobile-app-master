from rest_framework.generics import RetrieveAPIView, GenericAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from accounts.models import User
from rest_framework.permissions import IsAuthenticated
from .serializers import ProfileEmailSerializer, PasswordChangeSerializer
from drf_yasg.utils import swagger_auto_schema
from accounts.serializers import MessageSerializer


class ProfileEmailAPIView(RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = ProfileEmailSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return self.request.user


class ChangePasswordView(GenericAPIView):
    serializer_class = PasswordChangeSerializer
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(responses={200: MessageSerializer})
    def put(self, request, *args, **kwargs):
        serializer: PasswordChangeSerializer = self.get_serializer(
            request.user, data=request.data
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({"message": "password changed"})
