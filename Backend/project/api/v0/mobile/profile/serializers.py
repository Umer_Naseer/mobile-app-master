from rest_framework import serializers
from accounts.models import User
from django.contrib.auth.password_validation import (
    validate_password,
    ValidationError as DjangoValidationError,
)
from django.contrib.auth.hashers import check_password
from collections import defaultdict


class ProfileEmailSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["email"]


class PasswordChangeSerializer(serializers.ModelSerializer):
    old_password = serializers.CharField(
        write_only=True,
        required=True,
        style={"input_type": "password", "placeholder": "Old password"},
    )
    new_password = serializers.CharField(
        write_only=True,
        required=True,
        style={"input_type": "password", "placeholder": "New password"},
    )
    repeat_password = serializers.CharField(
        write_only=True,
        required=True,
        style={"input_type": "password", "placeholder": "Repeat password"},
    )

    class Meta:
        model = User
        fields = ["old_password", "new_password", "repeat_password"]

    def validate(self, attrs):
        errors = defaultdict(list)
        user = self.instance
        if attrs["new_password"] != attrs["repeat_password"]:
            errors["new_password"].append("Passwords must match")
            errors["repeat_password"].append("Passwords must match")
        try:
            validate_password(attrs["new_password"])
        except DjangoValidationError as e:
            errors["new_password"] += e.messages
        if not check_password(attrs["old_password"], user.password):
            errors["old_password"].append("Invalid password")
        elif attrs["old_password"] == attrs["new_password"]:
            errors["new_password"].append("New password must not match old password")
        if errors:
            raise serializers.ValidationError(errors)
        return super().validate(attrs)

    def update(self, instance: User, validated_data):
        instance.set_password(validated_data["new_password"])
        instance.save()
        return instance
