# Generated by Django 3.0.7 on 2020-06-24 10:12

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("lessons", "0003_auto_20200619_1304"),
    ]

    operations = [
        migrations.AddField(
            model_name="language",
            name="students",
            field=models.ManyToManyField(
                blank=True, related_name="languages", to=settings.AUTH_USER_MODEL
            ),
        ),
    ]
