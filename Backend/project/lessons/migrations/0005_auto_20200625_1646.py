# Generated by Django 3.0.7 on 2020-06-25 16:46

from django.db import migrations
import draftjs_editor.fields


class Migration(migrations.Migration):

    dependencies = [
        ("lessons", "0004_language_students"),
    ]

    operations = [
        migrations.AlterField(
            model_name="sectionpage",
            name="message",
            field=draftjs_editor.fields.EditorJSONField(
                blank=True, null=True, verbose_name="Message"
            ),
        ),
    ]
