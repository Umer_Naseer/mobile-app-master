from rest_framework import serializers
from . import models as lessons_models
from drf_extra_fields.fields import Base64ImageField, Base64FileField


class LessonResourceSerializer(serializers.ModelSerializer):
    audio = Base64FileField(required=False)
    image = Base64ImageField(required=False)

    class Meta:
        model = lessons_models.LessonResource
        fields = ("id", "audio", "text", "image")
        extra_kwargs = {
            "id": {"read_only": True, "required": False},
            "text": {"required": False},
        }


class LanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = lessons_models.Language
        fields = ("id", "name", "background", "description", "is_published")


class SectionListSerializer(serializers.ModelSerializer):
    class Meta:
        model = lessons_models.Section
        fields = (
            "id",
            "name",
        )


class SectionPageSerializer(serializers.ModelSerializer):
    class Meta:
        model = lessons_models.SectionPage
        fields = ("id", "message", "page_number", "congrats", "image", "video", "audio")
        extra_kwargs = {"id": {"read_only": False, "required": False}}


class SectionSerializer(serializers.ModelSerializer):
    pages = SectionPageSerializer(many=True)

    class Meta:
        model = lessons_models.Section
        fields = (
            "id",
            "name",
            "pages",
        )

    def update(self, instance, validated_data):
        instance.name = validated_data.get("name", instance.name)
        instance.save()
        pages = validated_data.get("pages")
        for page in pages:
            page_id = page.get("id", None)
            if not page_id:
                raise serializers.ValidationError({"id": ["This field is required"]})
            try:
                obj = instance.pages.get(id=page_id)
            except lessons_models.SectionPage.DoesNotExist:
                raise serializers.ValidationError({"id": ["This Page does not exist"]})
            obj.message = page.get("message", obj.message)
            obj.save()
        return instance


class LessonListSerializer(serializers.ModelSerializer):
    class Meta:
        model = lessons_models.Lesson
        fields = (
            "id",
            "name",
            # 'lesson_type',
        )


class MessageOption(serializers.ModelSerializer):
    option_resources = LessonResourceSerializer(many=True, required=False)

    class Meta:
        model = lessons_models.MessageOption
        fields = ("id", "message", "correct_answer", "option_resources")
        extra_kwargs = {"id": {"read_only": False, "required": False}}


class LessonMessageSerializer(serializers.ModelSerializer):
    options = MessageOption(many=True, required=False)
    question_resources = LessonResourceSerializer(many=True, required=False)

    class Meta:
        model = lessons_models.LessonMessage
        fields = ("id", "message", "hint", "options", "question_resources")
        extra_kwargs = {"id": {"read_only": False, "required": False}}


class LessonSerializer(serializers.ModelSerializer):
    messages = LessonMessageSerializer(many=True, read_only=True)
    lesson_resources = LessonResourceSerializer(many=True, read_only=True)
    lesson_type_display = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = lessons_models.Lesson
        fields = (
            "id",
            "name",
            "lesson_type",
            "lesson_type_display",
            "messages",
            "info_message",
            "lesson_resources",
        )

    def get_lesson_type_display(self, obj):
        return obj.get_lesson_type_display()

    def update(self, instance, validated_data):
        instance.name = validated_data.get("name", instance.name)
        instance.info_message = validated_data.get(
            "info_message", instance.info_message
        )
        instance.save()
        messages = validated_data.get("messages")
        for ms in messages:
            ms_id = ms.get("id", None)
            if not ms_id:
                raise serializers.ValidationError({"id": ["This field is required"]})
            try:
                message_obj = instance.messages.get(id=ms_id)
            except lessons_models.LessonMessage.DoesNotExist:
                raise serializers.ValidationError(
                    {"id": ["This Message does not exist"]}
                )
            message_obj.message = ms.get("message", message_obj.message)
            message_obj.hint = ms.get("hint", message_obj.hint)
            message_obj.save()
            options = ms.get("options", [])
            for opt in options:
                opt_id = opt.get("id", None)
                if not opt_id:
                    raise serializers.ValidationError(
                        {"id": ["This field is required"]}
                    )
                try:
                    option_obj = message_obj.options.get(id=opt_id)
                except lessons_models.LessonMessage.DoesNotExist:
                    raise serializers.ValidationError(
                        {"id": ["This Option does not exist"]}
                    )
                option_obj.message = opt.get("message", option_obj.message)
                option_obj.correct_answer = opt.get(
                    "correct_answer", option_obj.correct_answer
                )
                option_obj.save()
        return instance


# class LessonSerializer(serializers.ModelSerializer):
#     messages = LessonMessageSerializer(many=True, read_only=True)
#     lesson_resources = LessonResourceSerializer(many=True, read_only=True)
#     lesson_type_display = serializers.SerializerMethodField(read_only=True)
#
#     class Meta:
#         model = lessons_models.Lesson
#         fields = (
#             'id',
#             'name',
#             'lesson_type',
#             'lesson_type_display',
#             'messages',
#             'info_message',
#             'lesson_resources',
#             'ready'
#         )
#
#     def get_lesson_type_display(self, obj):
#         return obj.get_lesson_type_display()
#
#     def update(self, instance, validated_data):
#         instance.name = validated_data.get('name', instance.name)
#         instance.info_message = validated_data.get('info_message', instance.info_message)
#         instance.save()
#         messages = validated_data.get('messages')
#         for ms in messages:
#             ms_id = ms.get('id', None)
#             if not ms_id:
#                 raise serializers.ValidationError({'id': ['This field is required']})
#             try:
#                 message_obj = instance.messages.get(id=ms_id)
#             except lessons_models.LessonMessage.DoesNotExist:
#                 raise serializers.ValidationError({'id': ['This Message does not exist']})
#             message_obj.message = ms.get('message', message_obj.message)
#             message_obj.hint = ms.get('hint', message_obj.hint)
#             message_obj.save()
#             options = ms.get('options', [])
#             for opt in options:
#                 opt_id = opt.get('id', None)
#                 if not opt_id:
#                     raise serializers.ValidationError({'id': ['This field is required']})
#                 try:
#                     option_obj = message_obj.options.get(id=opt_id)
#                 except lessons_models.LessonMessage.DoesNotExist:
#                     raise serializers.ValidationError({'id': ['This Option does not exist']})
#                 option_obj.message = opt.get('message', option_obj.message)
#                 option_obj.correct_answer = opt.get('correct_answer', option_obj.correct_answer)
#                 option_obj.save()
#         return instance
