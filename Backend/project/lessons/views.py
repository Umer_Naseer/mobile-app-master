from rest_framework import generics, status, views
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination
from django.db.models import Prefetch
from django.shortcuts import get_object_or_404

from core.permissions import IsAdmin

from . import serializers
from . import models as lessons_models
from lessons.models import Section


class LanguagesPaginator(PageNumberPagination):
    page_size = 10


class LanguagesListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated, IsAdmin)
    serializer_class = serializers.LanguageSerializer
    queryset = lessons_models.Language.objects.all()
    pagination_class = LanguagesPaginator


class LanguageCreateView(generics.CreateAPIView):
    permission_classes = (IsAuthenticated, IsAdmin)
    serializer_class = serializers.LanguageSerializer
    queryset = lessons_models.Language.objects.all()


class LanguageView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated, IsAdmin)
    serializer_class = serializers.LanguageSerializer
    queryset = lessons_models.Language.objects.all()


class SectionsListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated, IsAdmin)
    serializer_class = serializers.SectionListSerializer

    def get_queryset(self):
        return lessons_models.Section.objects.filter(
            language_id=self.kwargs["pk"]
        ).order_by("index")


class SectionCreateView(generics.CreateAPIView):
    permission_classes = (IsAuthenticated, IsAdmin)
    serializer_class = serializers.SectionSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        name = serializer.validated_data.get("name")
        language_id = self.kwargs["pk"]
        pages = serializer.validated_data.get("pages")
        if len(pages) != 3:
            raise ValidationError({"pages": ["All pages are required"]})
        try:
            language = lessons_models.Language.objects.get(id=language_id)
        except lessons_models.Language.DoesNotExist:
            raise ValidationError({"id": ["This language does not exist"]})
        section = lessons_models.Section.objects.create(name=name, language=language)
        objs = []
        for page in pages:
            message = page.get("message")
            page_number = page.get("page_number")
            congrats = page.get("congrats")
            objs.append(
                lessons_models.SectionPage(
                    message=message,
                    page_number=page_number,
                    congrats=congrats,
                    section=section,
                )
            )
        lessons_models.SectionPage.objects.bulk_create(objs)
        return Response(data={"id": section.id}, status=status.HTTP_201_CREATED)


class SectionPageUpdateView(generics.RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated, IsAdmin)
    serializer_class = serializers.SectionPageSerializer
    queryset = lessons_models.SectionPage.objects.all()


class SectionView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated, IsAdmin)
    serializer_class = serializers.SectionSerializer
    queryset = lessons_models.Section.objects.all().prefetch_related(
        Prefetch(
            "pages", queryset=lessons_models.SectionPage.objects.order_by("page_number")
        )
    )


class LessonListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated, IsAdmin)
    serializer_class = serializers.LessonListSerializer

    def get_queryset(self):
        section = get_object_or_404(Section, pk=self.kwargs["pk"])
        return lessons_models.Lesson.objects.filter(section_id=section.pk)

    # def list(self, request, *args, **kwargs):
    #     queryset = self.get_queryset()
    #     serializer = self.get_serializer(queryset, many=True)
    #     response = serializer.data
    #     mannerism = {'mannerism_section': False}
    #     message_list = lessons_models.SectionPage.objects.filter(section_id=self.kwargs['pk'], congrats=False).values_list('message', flat=True)
    #     if message_list[0] and message_list[1]:
    #         mannerism = {'mannerism_section': True}
    #     response.append(mannerism)
    #     return Response(response)


class LessonCreateView(generics.CreateAPIView):
    permission_classes = (IsAuthenticated, IsAdmin)
    serializer_class = serializers.LessonSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        name = serializer.validated_data.get("name")
        lesson_type = serializer.validated_data.get("lesson_type")
        section_id = self.kwargs["pk"]
        info_message = serializer.validated_data.get("info_message", "")
        messages = serializer.validated_data.get("messages")
        try:
            section = lessons_models.Section.objects.get(id=section_id)
        except lessons_models.Section.DoesNotExist:
            raise ValidationError({"id": ["This section does not exist"]})
        lesson = lessons_models.Lesson.objects.create(
            name=name,
            lesson_type=lesson_type,
            section=section,
            info_message=info_message,
        )
        for ms in messages:
            message = lessons_models.LessonMessage.objects.create(
                lesson=lesson, message=ms.get("message"), hint=ms.get("hint", ""),
            )
            if lesson_type in [1, 2]:
                option_obj = []
                options = ms.get("options", None)
                if not options:
                    raise ValidationError({"options": ["This field is required"]})
                for opt in options:
                    option_obj.append(
                        lessons_models.MessageOption(
                            lesson_message=message,
                            message=opt.get("message"),
                            correct_answer=opt.get("correct_answer"),
                        )
                    )
                lessons_models.MessageOption.objects.bulk_create(option_obj)
        return Response(status=status.HTTP_201_CREATED)


# class LessonView(generics.RetrieveUpdateDestroyAPIView):
#     permission_classes = (IsAuthenticated, IsAdmin)
#     serializer_class = serializers.LessonSerializer
#
#     def get_queryset(self):
#         return lessons_models.Lesson.objects.all()\
#             .prefetch_related(Prefetch('messages', queryset=lessons_models.LessonMessage.objects.all().prefetch_related('options')))

#
# class LessonCreateListView(generics.ListAPIView):
#     permission_classes = (IsAuthenticated, IsAdmin)
#     serializer_class = serializers.LessonListSerializer
#
#     def get_object(self):
#         return get_object_or_404(Section, pk=self.kwargs['pk'])
#
#     def get_queryset(self):
#         section = self.get_object()
#         return lessons_models.Lesson.objects.filter(section_id=section.pk)
#
#     def post(self, request, *args, **kwargs):
#         lesson = lessons_models.Lesson.objects.create(section=self.get_object())
#         serializer = serializers.LessonSerializer(lesson)
#         return Response(serializer.data)
