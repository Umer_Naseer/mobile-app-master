from django.contrib import admin
from .models import *


class ResourceInline(admin.StackedInline):
    model = LessonResource
    extra = 0


class MessageInline(admin.TabularInline):
    model = LessonMessage
    extra = 0


class StudentLanguageInline(admin.TabularInline):
    model = StudentLanguage
    extra = 0


class StudentLessonProgressInline(admin.TabularInline):
    model = StudentLessonProgress
    extra = 0


class LessonAdmin(admin.ModelAdmin):
    inlines = [ResourceInline, MessageInline]


class LessonMessageAdmin(admin.ModelAdmin):
    inlines = [ResourceInline]


class MessageOptionAdmin(admin.ModelAdmin):
    inlines = [ResourceInline]


class SectionPageInlineAdmin(admin.StackedInline):
    model = SectionPage
    extra = 0


class SectionInlineAdmin(admin.TabularInline):
    model = Section
    extra = 0


class SectionAdmin(admin.ModelAdmin):
    inlines = [SectionPageInlineAdmin]


class LanguageAdmin(admin.ModelAdmin):
    inlines = [SectionInlineAdmin]


admin.site.register(Language, LanguageAdmin)
admin.site.register(Lesson, LessonAdmin)
admin.site.register(Section, SectionAdmin)
admin.site.register(SectionPage)
admin.site.register(LessonMessage, LessonMessageAdmin)
admin.site.register(MessageOption, MessageOptionAdmin)
