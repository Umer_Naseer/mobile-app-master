from django.db import models
from draftjs_editor.fields import EditorJSONField
from accounts.models import User
from django.core.exceptions import ValidationError


class Language(models.Model):
    name = models.CharField(max_length=200, unique=True, verbose_name="Name")
    background = models.ImageField(null=True, blank=True, max_length=300)
    description = EditorJSONField(null=True, blank=True)
    is_published = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Language"
        verbose_name_plural = "Languages"


class Section(models.Model):
    language = models.ForeignKey(
        Language,
        on_delete=models.CASCADE,
        related_name="sections",
        verbose_name="Language",
    )
    name = models.CharField(max_length=200, verbose_name="Name")
    index = models.SmallIntegerField(verbose_name="Index", null=True, blank=True)

    def save(self, request=False, *args, **kwargs):
        if self.id is None and self.index is None:
            self.index = (
                Section.objects.filter(language=self.language).order_by("index").count()
            )
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Section"
        verbose_name_plural = "Sections"


class SectionPage(models.Model):
    section = models.ForeignKey(
        Section, on_delete=models.CASCADE, related_name="pages", verbose_name="Section"
    )
    message = EditorJSONField(null=True, blank=True, verbose_name="Message")
    video = models.FileField(null=True, blank=True, verbose_name="Video")
    audio = models.FileField(verbose_name="Audio", null=True, blank=True)
    image = models.ImageField(
        verbose_name="Image", null=True, blank=True, max_length=300
    )
    page_number = models.PositiveSmallIntegerField(verbose_name="Page number")
    congrats = models.BooleanField(default=False, verbose_name="Congratulation page")

    def __str__(self):
        return "{}".format(self.page_number)

    class Meta:
        verbose_name = "Section Page"
        verbose_name_plural = "Section Pages"


class Lesson(models.Model):
    LESSON_TYPE = (
        (0, "Lesson"),
        (1, "Quiz with instant results"),
        (2, "Ordinary quiz"),
        (3, "Words selection"),
    )
    section = models.ForeignKey(
        Section,
        on_delete=models.CASCADE,
        related_name="lessons",
        verbose_name="Section",
    )
    name = models.CharField(max_length=200, verbose_name="Name")
    lesson_type = models.IntegerField(
        choices=LESSON_TYPE, default=0, verbose_name="Type"
    )
    info_message = models.TextField(blank=True, verbose_name="Hint")
    ready = models.BooleanField(default=False, verbose_name="Lesson is ready")
    fun_fact = EditorJSONField(
        verbose_name="Fun fact", null=True, blank=True
    )  # Main content if used as a words selection
    fun_fact_image = models.ImageField(
        verbose_name="Fun fact image", null=True, blank=True, max_length=300
    )
    index = models.SmallIntegerField(verbose_name="Index", null=True, blank=True)
    is_published = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def save(self, request=False, *args, **kwargs):
        if self.id is None and self.index is None:
            self.index = (
                Lesson.objects.filter(section=self.section).order_by("index").count()
            )
        super().save(*args, **kwargs)

    class Meta:
        verbose_name = "Lesson"
        verbose_name_plural = "Lessons"


class LessonMessage(models.Model):
    lesson = models.ForeignKey(
        Lesson, on_delete=models.CASCADE, related_name="messages", verbose_name="Lesson"
    )
    message = models.TextField(verbose_name="Message")
    hint = models.CharField(max_length=255, blank=True, verbose_name="Hint")

    def __str__(self):
        return "{}".format(self.id)

    class Meta:
        verbose_name = "Message"
        verbose_name_plural = "Messages"


class MessageOption(models.Model):
    lesson_message = models.ForeignKey(
        LessonMessage,
        on_delete=models.CASCADE,
        related_name="options",
        verbose_name="Lesson message",
    )
    message = models.TextField(verbose_name="Message")
    correct_answer = models.BooleanField(default=False, verbose_name="Correct answer")

    def __str__(self):
        return "{}".format(self.id)

    class Meta:
        verbose_name = "Message Option"
        verbose_name_plural = "Message Options"


class LessonResource(models.Model):
    lesson = models.ForeignKey(
        Lesson,
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name="lesson_resources",
        verbose_name="Lesson",
    )
    question = models.ForeignKey(
        LessonMessage,
        on_delete=models.CASCADE,
        related_name="question_resources",
        null=True,
        blank=True,
        verbose_name="Question",
    )
    option = models.ForeignKey(
        MessageOption,
        on_delete=models.CASCADE,
        related_name="option_resources",
        null=True,
        blank=True,
        verbose_name="Option",
    )
    index = models.SmallIntegerField(verbose_name="Index", null=True, blank=True)
    audio = models.FileField(verbose_name="Audio", null=True, blank=True)
    image = models.ImageField(
        verbose_name="Image", null=True, blank=True, max_length=300
    )
    video = models.FileField(null=True, blank=True, verbose_name="Video")
    text = EditorJSONField(verbose_name="Text", null=True, blank=True)

    def __str__(self):
        return "{}".format(self.id)

    def save(self, request=False, *args, **kwargs):
        if self.id is None and self.index is None:
            for k, v in {
                "lesson": Lesson,
                "question": LessonMessage,
                "option": MessageOption,
            }.items():
                if getattr(self, k) is not None:
                    self.index = (
                        LessonResource.objects.filter(**{k: getattr(self, k)})
                        .order_by("index")
                        .count()
                    )
                    break
        super().save(*args, **kwargs)

    class Meta:
        verbose_name = "Lesson Resource"
        verbose_name_plural = "Lesson Resources"


class StudentLessonProgress(models.Model):
    student = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="lessons",
        null=True,
        blank=True,
        verbose_name="Student",
    )
    lesson = models.ForeignKey(
        Lesson,
        on_delete=models.CASCADE,
        related_name="students",
        null=True,
        blank=True,
        verbose_name="Lesson",
    )
    date = models.DateTimeField(auto_now_add=True)


class StudentLanguage(models.Model):
    student = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="languages",
        null=True,
        blank=True,
        verbose_name="Student",
    )
    language = models.ForeignKey(
        Language,
        on_delete=models.CASCADE,
        related_name="students",
        null=True,
        blank=True,
        verbose_name="Language",
    )
    date = models.DateTimeField(auto_now_add=True)
