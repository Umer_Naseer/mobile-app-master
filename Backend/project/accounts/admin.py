from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import Group
from rest_framework.authtoken.models import Token

from lessons.admin import StudentLessonProgressInline, StudentLanguageInline

from .forms import UserChangeForm, UserCreationForm
from .models import ActivationCode, User, PasswordResetCode


class TokenInline(admin.StackedInline):
    model = Token
    classes = ["collapse"]


class ActivationCodeInline(admin.StackedInline):
    model = ActivationCode
    classes = ["collapse"]


class PasswordResetCodeInline(admin.StackedInline):
    model = PasswordResetCode
    classes = ["collapse"]


class UserAdmin(BaseUserAdmin):
    form = UserChangeForm
    add_form = UserCreationForm

    list_display = (
        "email",
        "is_admin",
        "is_active",
        "country",
    )
    list_filter = ("is_admin",)
    fieldsets = (
        (
            None,
            {
                "fields": (
                    "email",
                    "password",
                    "country",
                    "country_code",
                    ("is_admin", "is_active"),
                )
            },
        ),
    )
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": ("email", "password", "password_confirmation"),
            },
        ),
    )
    search_fields = ("email",)
    ordering = ("-id",)
    inlines = (
        TokenInline,
        ActivationCodeInline,
        PasswordResetCodeInline,
        StudentLanguageInline,
        StudentLessonProgressInline,
    )
    filter_horizontal = ()


admin.site.register(User, UserAdmin)
# region unregister
admin.site.unregister(Group)
admin.site.unregister(Token)
# endregion
