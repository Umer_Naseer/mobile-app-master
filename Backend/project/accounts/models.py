import hashlib
from datetime import timedelta, datetime
from string import digits

from django.conf import settings
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser
from django.db import models
from django.utils import timezone
from django.utils.crypto import get_random_string
from django.core.mail import send_mail

__all__ = [
    "User",
]


# region User
class UserManager(BaseUserManager):
    def create_user(self, email, password=None, **kwargs):
        if not email:
            raise ValueError("User must have Email")

        user = self.model(
            # username=username,
            email=email,
            **kwargs,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, username=None):
        user = self.create_user(
            # username=username,
            email=email,
            password=password,
        )
        user.is_admin = True
        user.is_active = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    email = models.EmailField(unique=True, verbose_name="Email")
    # username = models.CharField(max_length=300, null=True, blank=True, verbose_name='Username')

    is_active = models.BooleanField(default=False, verbose_name="Active")
    is_admin = models.BooleanField(default=False, verbose_name="Admin")

    registered = models.DateTimeField(auto_now_add=True, verbose_name="Registered")
    country = models.CharField(max_length=100, blank=True)
    country_code = models.CharField(max_length=10, blank=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def send_email(self, subj, msg, html_msg=None):
        args, kwargs = (
            (subj, msg, settings.DEFAULT_FROM_EMAIL, [self.email]),
            {"html_message": html_msg},
        )
        send_mail(*args, **kwargs)

    objects = UserManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    # region std
    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    # endregion

    def __str__(self):
        return self.email

    @property
    def is_staff(self):
        return self.is_admin

    def email_user(self, subject, message, from_email=None, **kwargs):
        """Send an email to this user."""
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def generate_activation_code(self):
        ActivationCode.objects.filter(user=self).delete()
        return ActivationCode.objects.create(
            user=self, code=get_random_string(6, "0123465789")
        )

    def send_activation_code_email(self):
        self.send_email(
            "New activation code",
            f"Activation code: {self.generate_activation_code().code}",
        )

    def send_user_registered_email(self):
        self.send_email(
            "User registered",
            f"Activation code: {self.generate_activation_code().code}",
        )

    def generate_password_reset_code(self):
        PasswordResetCode.objects.filter(user=self).delete()
        return PasswordResetCode.objects.create(
            user=self, code=get_random_string(6, "0123465789")
        )

    def send_password_reset_code_email(self):
        self.send_email(
            "Password reset code",
            f"Reset code: {self.generate_password_reset_code().code}",
        )

    class Meta:
        verbose_name = "User"
        verbose_name_plural = "Users"


class BaseCodeModel(models.Model):
    code = models.CharField(max_length=6)
    created_at = models.DateTimeField(auto_now_add=True)
    check_count = models.IntegerField(default=0, help_text="times_checked")

    def is_check_count_ok(self):
        return self.check_count < 5

    def can_resend_code(self):
        return self.created_at + timedelta(minutes=1) < timezone.now()

    def increase_check_count(self):
        self.check_count += 1
        self.save(update_fields=["check_count"])

    class Meta:
        abstract = True


class ActivationCode(BaseCodeModel):
    user = models.OneToOneField(
        User, related_name="activation_code", on_delete=models.CASCADE
    )


class PasswordResetCode(BaseCodeModel):
    user = models.OneToOneField(
        User, related_name="password_reset_code", on_delete=models.CASCADE
    )


# endregion
