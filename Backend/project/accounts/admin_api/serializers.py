from rest_framework import serializers

from accounts.models import User


class AllUsersSerializer(serializers.ModelSerializer):
    lessons = serializers.SerializerMethodField()
    languages = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ["id", "email", "registered", "lessons", "languages"]

    def get_lessons(self, object):
        return object.lessons.count()

    def get_languages(self, object):
        return object.languages.values_list("language__name", flat=True)


class CountrySerializer(serializers.Serializer):
    country = serializers.CharField()
    users = serializers.IntegerField()
