from rest_framework.generics import ListAPIView
from django.db.models import Count
from rest_framework.permissions import IsAuthenticated

from .serializers import AllUsersSerializer, CountrySerializer
from accounts.models import User
from core.pagination import AdminPagination
from core.permissions import IsAdmin


class AllUsersAPIView(ListAPIView):
    serializer_class = AllUsersSerializer
    queryset = User.objects.exclude(is_admin=True)
    pagination_class = AdminPagination
    permission_classes = [IsAuthenticated, IsAdmin]


class ActiveUsersAPIView(ListAPIView):
    serializer_class = AllUsersSerializer
    queryset = User.objects.filter(is_active=True, is_admin=False)
    pagination_class = AdminPagination
    permission_classes = [IsAuthenticated, IsAdmin]


class UsersCountriesAPIView(ListAPIView):
    serializer_class = CountrySerializer
    queryset = (
        User.objects.exclude(is_admin=True)
        .values("country")
        .annotate(users=Count("id", distinct=True))
        .order_by("-users")
    )
    permission_classes = [IsAuthenticated, IsAdmin]
    pagination_class = AdminPagination
