from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.authtoken.models import Token
from rest_framework.generics import CreateAPIView, GenericAPIView
from rest_framework.permissions import AllowAny
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND, HTTP_200_OK
from rest_framework.response import Response
from rest_framework.views import APIView

from . import serializers


class LoginView(GenericAPIView):
    """Get auth token using email and password

    You should include this token in Authorization header like "Token 3840e813c57a3a46c8d50304d05bc28cff917437"

    POST /api/v0/auth/login/

        {
            "email": "user@example.com",
            "password": "pXww0DUg3R"
        }

    Response 200:

        {
            "token": "3840e813c57a3a46c8d50304d05bc28cff917437",
            "is_admin": false
        }

    Response 400:

        {
            "error": "Invalid Credentials"
        }
    """

    serializer_class = serializers.LoginSerializer
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data["user"]
        token, _ = Token.objects.get_or_create(user=user)

        return Response(
            {"token": token.key, "is_admin": user.is_admin,}, status=HTTP_200_OK
        )


class RegisterView(CreateAPIView):
    """Register

    POST /api/v0/auth/register/

        {
            "email": "user@example.com",
            "password": "pXww0DUg3R",
        }

    Response 400

        {
            "email": [
                "User with this Email already exists."
            ]
        }

    Response 201

        {
            "email": "user@example.com"
        }

    Email with activation code will be sent to user.
    """

    serializer_class = serializers.RegisterSerializer
    permission_classes = (AllowAny,)


class ActivateView(GenericAPIView):
    """Activate user with code

    POST /api/v0/auth/activate/


        {
            "email": "user@example.com",
            "code": "134645645"
        }


    Response 400:

        {
            "code": [
                "Ensure this field has at least 6 characters."
            ]
        }


        {
            "code": [
                "Ensure this field has no more than 6 characters."
            ]
        }

        {
            "non_field_errors": [
                "Number of attempts to check exceeded. Request a new code"
            ]
        }

    Response 404

    Can occur when You call this API for already active user.

    Response 200

        {
            "token": "3840e813c57a3a46c8d50304d05bc28cff917437",
            "is_admin": false
        }

    """

    serializer_class = serializers.ActivateSerializer
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data["email"]
        user.is_active = True
        user.save()
        user.activation_code.delete()
        token, _ = Token.objects.get_or_create(user=user)
        return Response(
            {"token": token.key, "is_admin": user.is_admin,}, status=HTTP_200_OK
        )


class ResendActivationCodeView(GenericAPIView):
    """Resend activation code

    POST /api/v0/auth/resend-activation-code/

        {
            "email": "user@example.com",
        }

    Response 400:

        {
            "non_field_errors": [
                "Please wait before ask a new code."
            ]
        }

    Response 404

    Can occur when You call this API for already active user.

    Response 200

        {
            "message": "OK"
        }
    """

    serializer_class = serializers.ResendActivationCodeSerializer

    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data["email"]
        user.send_activation_code_email()
        return Response({"message": "OK"})


class ResetPasswordCodeRequestView(GenericAPIView):
    """send password reset code

    POST /api/v0/auth/reset-password-send-code/

        {
            "email": "user@example.com",
        }

    Response 400:

        {
            "non_field_errors": [
                "Please wait before ask a new code."
            ]
        }

    Response 404

    Can occur when You call this API for inactive user.

    Response 200

        {
            "message": "OK"
        }
    """

    serializer_class = serializers.SendPasswordResetCodeSerializer

    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data["email"]
        user.send_password_reset_code_email()
        return Response({"message": "OK"})


class ResetPasswordVerifyCodeView(GenericAPIView):
    """reset password verify code

    POST /api/v0/auth/reset-password-verify-code/

        {
            "email": "user@example.com",
            "code" : "134657"
        }

    Response 400:

        {
          "code": [
            "Ensure this field has at least 6 characters."
          ]
        }

        {
            "non_field_errors": [
                "Number of attempts to check exceeded. Request a new code."
            ]
        }

        {
            "non_field_errors": [
                "This user does not have reset code sent."
            ]
        }

        {
          "non_field_errors": [
            "Wrong code"
          ]
        }

    Response 404

    Can occur when You call this API for inactive user.

    Response 200

        {
            "message": "Code valid"
        }
    """

    serializer_class = serializers.ResetPasswordVerifyCodeSerializer

    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response({"message": "Code valid"})


class ResetPasswordCodeView(GenericAPIView):
    """reset password apply code

        {
            "code": "123456",
            "email": "user@example.com",
            "password": "p@ssw0rd",
            "repeat_password ": "p@ssw0rd"
        }

    Response 400:

        {
            "password": [
                "Weak password"
            ]
        }

        {
            "repeat_password": [
                "Passwords do not match"
            ]
        }

        {
            "non_field_errors": [
                "Number of attempts to check exceeded. Request a new code."
            ]
        }

        {
            "non_field_errors": [
                "This user does not have reset code sent."
            ]
        }

    Response 404

    Can occur when You call this API for inactive user.

    Response 200

        {
            "message": "Password changed"
        }

    """

    serializer_class = serializers.ResetPasswordByCodeSerializer

    permission_classes = (AllowAny,)

    @swagger_auto_schema(responses={200: serializers.MessageSerializer})
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response({"message": "Password changed"})
