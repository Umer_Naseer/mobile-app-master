from django.contrib.auth.password_validation import validate_password
from django.db import transaction
from rest_framework import serializers
from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token

from core.utils import get_client_ip
from . import models
from django.contrib.gis.geoip2 import GeoIP2


class LoginSerializer(
    serializers.Serializer
):  # noqa: E501 pylint: disable=abstract-method
    default_error_messages = {
        "wrong_username_or_password": "Wrong username or password"
    }

    email = serializers.EmailField(required=True, allow_blank=False)
    password = serializers.CharField(
        min_length=8,
        required=True,
        allow_blank=False,
        error_messages={"min_length": "Must be {min_length} characters or more",},
    )

    def validate(self, attrs):
        attrs["user"] = authenticate(
            username=attrs["email"], password=attrs["password"]
        )
        if not attrs["user"]:
            self.fail("wrong_username_or_password")
        return super().validate(attrs)

    class Meta:
        fields = (
            "email",
            "password",
        )


class RegisterSerializer(serializers.ModelSerializer):
    # password_confirm = serializers.CharField(write_only=True)

    def validate_password(self, value):
        validate_password(value)
        return value

    def validate(self, data):
        # if data['password'] != data['password_confirm']:
        #     raise serializers.ValidationError('Password do not match ')
        return data

    def create(self, validated_data):
        data = validated_data.copy()
        g = GeoIP2()
        try:
            country = g.country(get_client_ip(self.context["request"]))
            data["country_code"] = country["country_code"]
            data["country"] = country["country_name"]
        except Exception:
            data["country_name"] = "ERROR"
            data["country_code"] = "ERROR"
        # del data['password_confirm']
        with transaction.atomic():
            user = self.Meta.model.objects.create_user(is_active=False, **data)
            user.send_user_registered_email()
            return user

    class Meta:
        model = models.User
        fields = (
            "email",
            # 'fio',
            # 'birthdate',
            "password",
            # 'password_confirm'
        )
        extra_kwargs = {
            "password": {
                "write_only": True,
                "error_messages": {
                    "min_length": "Must be {max_length} characters or more"
                },
            }
        }


class ActivateSerializer(serializers.Serializer):
    code = serializers.CharField(
        write_only=True,
        max_length=6,
        min_length=6,
        error_messages={"wrong": "Wrong code"},
    )
    email = serializers.SlugRelatedField(
        slug_field="email", queryset=models.User.objects.filter(is_active=False)
    )

    default_error_messages = {
        "too_much_checks": "Number of attempts to check exceeded. Request a new code",
        "no_code": "This user does not have activation code sent",
    }

    def validate(self, attrs):
        try:
            user: models.User = attrs["email"]
            user.activation_code.increase_check_count()
            if not user.activation_code.is_check_count_ok():
                self.fail("too_much_checks")
            if user.activation_code.code != attrs["code"]:
                self.fields["code"].fail("wrong")
        except AttributeError:
            self.fail("no_code")
        return super().validate(attrs)


class ResendActivationCodeSerializer(serializers.Serializer):
    email = serializers.SlugRelatedField(
        slug_field="email", queryset=models.User.objects.filter(is_active=False)
    )

    default_error_messages = {
        "wait": "Please wait before ask a new code",
    }

    def validate(self, attrs):
        try:
            if not attrs["email"].activation_code.can_resend_code():
                self.fail("wait")
        except AttributeError:
            pass  # strange, but if no code - we can send new
        return super().validate(attrs)


class SendPasswordResetCodeSerializer(serializers.Serializer):
    email = serializers.SlugRelatedField(
        slug_field="email",
        queryset=models.User.objects.filter(is_active=True),
        error_messages={"does_not_exist": "User with email {value} does not exist.",},
    )

    default_error_messages = {
        "wait": "Please wait before ask a new code",
    }

    def validate(self, attrs):
        try:
            if not attrs["email"].password_reset_code.can_resend_code():
                self.fail("wait")
        except AttributeError:
            pass  # no code - we can send new
        return super().validate(attrs)


class ResetPasswordVerifyCodeSerializer(serializers.Serializer):
    code = serializers.CharField(
        write_only=True,
        max_length=6,
        min_length=6,
        error_messages={"wrong": "Wrong code"},
    )
    email = serializers.SlugRelatedField(
        slug_field="email", queryset=models.User.objects.filter(is_active=True)
    )

    default_error_messages = {
        "too_much_checks": "Number of attempts to check exceeded. Request a new code",
        "no_code": "This user does not have reset code sent",
        "wrong_code": "This user does not have reset code sent",
    }

    def validate(self, attrs):
        try:
            user: models.User = attrs["email"]
            user.password_reset_code.increase_check_count()
            if not user.password_reset_code.is_check_count_ok():
                self.fail("too_much_checks")
            if user.password_reset_code.code != attrs["code"]:
                self.fields["code"].fail("wrong")
        except AttributeError:
            self.fail("no_code")
        return super().validate(attrs)


class ResetPasswordByCodeSerializer(serializers.Serializer):
    default_error_messages = {
        "too_much_checks": "Number of attempts to check exceeded. Request a new code",
        "no_code": "This user does not have reset code sent",
    }

    code = serializers.CharField(
        write_only=True,
        max_length=6,
        min_length=6,
        error_messages={"wrong": "Wrong code"},
    )
    email = serializers.SlugRelatedField(
        slug_field="email", queryset=models.User.objects.filter(is_active=True)
    )
    password = serializers.CharField()
    repeat_password = serializers.CharField(
        required=False,
        allow_blank=True,
        error_messages={"no_match": "Passwords do not match"},
    )

    def create(self, validated_data):
        user = validated_data["email"]
        user.set_password(validated_data["password"])
        user.save()
        user.password_reset_code.delete()
        return user

    def validate_password(self, value):
        validate_password(value)
        return value

    def validate(self, attrs):
        try:
            user: models.User = attrs["email"]
            user.password_reset_code.increase_check_count()
            if not user.password_reset_code.is_check_count_ok():
                self.fail("too_much_checks")
            if user.password_reset_code.code != attrs["code"]:
                self.fields["code"].fail("wrong")
        except AttributeError:
            self.fail("no_code")

        if "repeat_password" in attrs:
            if attrs["repeat_password"] != attrs["password"]:
                self.fields["repeat_password"].fail("no_match")
        return super().validate(attrs)

    class Meta:
        swagger_schema_fields = {
            "example": {
                "code": "123456",
                "email": "user@example.com",
                "password": "p@ssw0rd",
                "repeat_password": "p@ssw0rd",
            }
        }


class MessageSerializer(serializers.Serializer):
    message = serializers.CharField()
