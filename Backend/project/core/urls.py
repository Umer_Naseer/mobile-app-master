from django.shortcuts import redirect
from django.urls import include, path
from django.conf.urls.static import static
from django.conf import settings

from django.contrib import admin
from accounts import views as accounts_views
from accounts.admin_api import views as accounts_admin_views
from . import views as core_views
from lessons import views as lessons_view
from api.v0.front.lessons import views as api_lessons_view


api_urls = [
    path(
        "",
        core_views.schema_view.with_ui("redoc", cache_timeout=0),
        name="schema-redoc-index",
    ),
    path(
        "swagger/",
        core_views.schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    path(
        "redoc/",
        core_views.schema_view.with_ui("redoc", cache_timeout=0),
        name="schema-redoc",
    ),
    # path('options/', core_views.OptionsView.as_view()), # ALL CHOICES, SETTINGS GOES HERE
    path("auth/login/", accounts_views.LoginView.as_view()),
    path("auth/register/", accounts_views.RegisterView.as_view()),
    path("auth/activate/", accounts_views.ActivateView.as_view()),
    path(
        "auth/resend-activation-code/",
        accounts_views.ResendActivationCodeView.as_view(),
    ),
    path(
        "auth/reset-password-send-code/",
        accounts_views.ResetPasswordCodeRequestView.as_view(),
    ),
    path(
        "auth/reset-password-verify-code/",
        accounts_views.ResetPasswordVerifyCodeView.as_view(),
    ),
    path(
        "auth/reset-password-by-code/", accounts_views.ResetPasswordCodeView.as_view()
    ),
    path("admin/users/", accounts_admin_views.AllUsersAPIView.as_view()),
    path("admin/users/active/", accounts_admin_views.ActiveUsersAPIView.as_view()),
    path(
        "admin/users/countries/", accounts_admin_views.UsersCountriesAPIView.as_view()
    ),
    path("admin/language/", lessons_view.LanguagesListView.as_view()),
    path("admin/language/create/", lessons_view.LanguageCreateView.as_view()),
    path("admin/language/<int:pk>/", lessons_view.LanguageView.as_view()),
    path("admin/language/<int:pk>/sections/", lessons_view.SectionsListView.as_view()),
    path(
        "admin/language/<int:pk>/section/create/",
        lessons_view.SectionCreateView.as_view(),
    ),
    path("admin/section-page/<int:pk>/", lessons_view.SectionPageUpdateView.as_view()),
    path("admin/section/<int:pk>/", lessons_view.SectionView.as_view()),
    path("admin/section/index/", api_lessons_view.SectionIndexView.as_view()),
    path(
        "admin/section/<int:pk>/lesson/",
        api_lessons_view.LessonCreateListView.as_view(),
    ),
    path("admin/lesson/index/", api_lessons_view.LessonIndexView.as_view()),
    path("admin/lesson/<int:pk>/", api_lessons_view.LessonView.as_view()),
    path("admin/lesson/<int:pk>/clear-media/", api_lessons_view.LessonClearMediaView.as_view()),
    path(
        "admin/lesson/<int:pk>/question/",
        api_lessons_view.ListCreateQuestionView.as_view(),
    ),
    path("admin/question/<int:pk>/", api_lessons_view.QuestionView.as_view()),
    path(
        "admin/question/<int:pk>/option/",
        api_lessons_view.ListCreateOptionView.as_view(),
    ),
    path("admin/option/<int:pk>/", api_lessons_view.OptionView.as_view()),
    path("admin/resource/", api_lessons_view.AddMediaView.as_view()),
    path("admin/resource/index/", api_lessons_view.MediaIndexView.as_view()),
    path("admin/resource/<int:pk>/", api_lessons_view.MediaView.as_view()),
]


mobile_api_urls = [
    path("languages/", include("api.v0.mobile.languages.urls")),
    path("sections/", include("api.v0.mobile.sections.urls")),
    path("profile/", include("api.v0.mobile.profile.urls")),
    path("", include("api.v0.mobile.lesson.urls")),
]

urlpatterns = (
    [
        # Examples:
        path("", lambda r: redirect("/api/v0")),
        path("admin/", admin.site.urls),
        path("api/v0/", include((api_urls, "core"), namespace="api_v0")),
        path(
            "api/v0/mobile/",
            include((mobile_api_urls, "core"), namespace="api_v0_mobile"),
        ),
    ]
    + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
)
