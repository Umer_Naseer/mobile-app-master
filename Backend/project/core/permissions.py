from rest_framework import permissions


class IsAdmin(permissions.BasePermission):
    """
    Permission to only allow access to admins.
    """

    def has_permission(self, request, view):
        return request.user.is_admin
