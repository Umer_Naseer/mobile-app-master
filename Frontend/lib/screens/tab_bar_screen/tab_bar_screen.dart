import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lingafriq/iteractor/server_manager.dart';
import 'package:lingafriq/screens/learning/active_learning/active_learning.dart';
import 'package:lingafriq/screens/learning/languages/languages.dart';
import 'package:lingafriq/screens/profile/profile.dart';
import 'package:lingafriq/ui_elements/questrial_regular_text.dart';
import 'package:lingafriq/utils/color.dart';

class TabBarScreen extends StatefulWidget {
  final String token;

  TabBarScreen(this.token);

  @override
  _TabBarScreenState createState() => _TabBarScreenState();
}

class _TabBarScreenState extends State<TabBarScreen> {
  int _selectedPage = 0;
  final PageStorageBucket bucket = PageStorageBucket();
  final _pages = [
    ActiveLearning(
      key: PageStorageKey('ActiveLearning'),
    ),
    //LessonWidget(55, null),
    Languages(
      key: PageStorageKey('Languages'),
    ),
    Profile(
      key: PageStorageKey('Profile'),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    print(getAuthToken());
    return Scaffold(
      body: PageStorage(
        child: _pages[_selectedPage],
        bucket: bucket,
      ),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.3),
              spreadRadius: 8,
              blurRadius: 5,
              offset: Offset(0, 5), // changes position of shadow
            ),
          ],
        ),
        child: BottomNavigationBar(
          //showUnselectedLabels: true,
          selectedItemColor: hexToColor("#2e1224"),
          unselectedItemColor: hexToColor("#CDCDCD"),
          currentIndex: _selectedPage,
          onTap: (int index) {
            setState(() {
              _selectedPage = index;
            });
          },
          items: [
            BottomNavigationBarItem(
              icon: _selectedPage == 0
                  ? new Image.asset('assets/active_learning_active.png')
                  : new Image.asset('assets/active_learning.png'),
              title: QuestrialRegularText("Active learning", 12, _getColor(0)),
            ),
            BottomNavigationBarItem(
              icon: _selectedPage == 1
                  ? new Image.asset('assets/languages_active.png')
                  : new Image.asset('assets/languages.png'),
              title: QuestrialRegularText("Languages", 12, _getColor(1)),
            ),
            BottomNavigationBarItem(
              icon: _selectedPage == 2
                  ? new Image.asset('assets/profile_active.png')
                  : new Image.asset('assets/profile.png'),
              title: QuestrialRegularText("Settings", 12, _getColor(2)),
            ),
          ],
        ),
      ),
    );
  }

  String _getColor(int index) => _selectedPage == index ? "#2e1224" : "#CDCDCD";
}
