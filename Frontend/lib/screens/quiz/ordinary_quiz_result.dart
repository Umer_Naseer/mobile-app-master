import 'dart:convert';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:lingafriq/iteractor/server_manager.dart';
import 'package:lingafriq/models/lesson.dart';
import 'package:lingafriq/models/quiz_item.dart';
import 'package:lingafriq/screens/quiz/quiz_body/quiz_title_options.dart';
import 'package:lingafriq/ui_elements/buttons.dart';
import 'package:lingafriq/ui_elements/questrial_regular_text.dart';
import 'package:lingafriq/utils/color.dart';
import 'package:lingafriq/utils/singleLessonNavigator.dart';

class OrdinaryQuizResult extends StatefulWidget {
  Lesson lesson;

  int quizCount = -1;
  int correctQuizCount = -1;
  bool get isPassed {
    return quizCount != -1 && quizCount == correctQuizCount;
  }

  OrdinaryQuizResult(this.lesson);

  @override
  _OrdinaryQuizResultState createState() => _OrdinaryQuizResultState();
}

class _OrdinaryQuizResultState extends State<OrdinaryQuizResult> {
  @override
  void initState() {
    widget.quizCount = widget.lesson.messages.length;
    widget.correctQuizCount = widget.lesson.messages
        .where((el) => el.selectedOptionId == el.correctOptionId)
        .length;
    widget.lesson.changeQOptionsAvailability(isActive: false);
    widget.lesson.clearVerified();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: hexToColor("#2E1224"),
      child: Scaffold(
        extendBodyBehindAppBar: true,
        backgroundColor: Colors.white,
        body: CustomScrollView(slivers: getSlivers(widget.lesson)),
      ),
    );
  }

  List<Widget> getSlivers(Lesson lesson) {
    List<Widget> list = [
      SliverAppBar(
        floating: false,
        pinned: true,
        expandedHeight: 180.0,
        backgroundColor: hexToColor("#2e1224"),
        flexibleSpace: LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
          double height = constraints.heightConstraints().constrainHeight();
          return FlexibleSpaceBar(
            centerTitle: true,
            title: QuestrialRegularText(
              widget.lesson.name,
              24,
              "#FFFFFF",
              maxLines: height < 150 ? 1 : 3,
              overflow: TextOverflow.ellipsis,
            ),
            background: Container(color: hexToColor("#2e1224")),
          );
        }),
      ),
      SliverPadding(
        padding: EdgeInsets.only(left: 24, right: 24, top: 32, bottom: 0),
        sliver: SliverList(
          delegate: SliverChildBuilderDelegate((context, index) {
            return Container(
              alignment: Alignment.centerLeft,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  RichText(
                    textAlign: TextAlign.left,
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: widget.isPassed
                              ? "Congratulations! "
                              : "You scored ",
                          style: QuestrialRegularText.getStyle("#404040", 16),
                        ),
                        TextSpan(
                          text: widget.isPassed
                              ? "You answered all the questions correctly."
                              : "${widget.correctQuizCount} correct answers ",
                          style: QuestrialRegularText.getStyle("#25A588", 16),
                        ),
                        if (!widget.isPassed)
                          TextSpan(
                            text:
                                "out of ${widget.quizCount}. See where you made a mistake and try again.",
                            style: QuestrialRegularText.getStyle("#404040", 16),
                          ),
                      ],
                    ),
                  ),
                  SizedBox(height: 24),
                  Container(
                      height: 2,
                      child: Row(
                        children: [
                          Expanded(
                              flex: widget.correctQuizCount,
                              child: Container(color: hexToColor("#25A588"))),
                          Expanded(
                              flex:
                                  (widget.quizCount - widget.correctQuizCount),
                              child: Container(color: hexToColor("#EDEDED")))
                        ],
                      ))
                ],
              ),
            );
          }, childCount: 1),
        ),
      )
    ];
    lesson.messages.forEach((element) {
      QuizItem quizItem = element;
      quizItem.isChecking = true;
      if (quizItem.questionResources.isNotEmpty) {
        list.add(SliverPadding(
          padding: EdgeInsets.only(left: 24, right: 24, top: 24),
          sliver: QuizHeader(quizItem.questionResources),
        ));
      }
      if (quizItem.options.isNotEmpty) {
        list.add(SliverPadding(
          padding: EdgeInsets.symmetric(vertical: 24, horizontal: 0),
          sliver: QuizOptions(quizItem),
        ));
      }
    });
    list.add(SliverPadding(padding: EdgeInsets.only(bottom: 24)));
    list.add(SliverToBoxAdapter(
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 16, horizontal: 24),
        child: Align(
            alignment: FractionalOffset.bottomCenter,
            child: OrangeAuthBtn(
                widget.isPassed ? "CONTINUE" : "TRY AGAIN", false, () {
              onContinueClick(context);
            })),
      ),
    ));
    return list;
  }

//  Future<Lesson> getData(int id) async {
//    Response response = await ServerManager.getLesson(id);
//    if (response.statusCode >= 200 && response.statusCode < 300) {
//      Map<String, Object> data = json.decode(utf8.decode(response.bodyBytes));
//      Lesson value = Lesson.fromJson(data);
//      return value;
//    } else {
//      throw Exception('Failed to load the lesson');
//    }
//  }

  void onContinueClick(BuildContext context) async {
    widget.lesson.messages
        .where((el) => el.selectedOptionId == el.correctOptionId)
        .forEach((el) {
      el.isVerified = true;
    });
    widget.lesson.changeQOptionsAvailability(isActive: true);
    if (widget.isPassed) {
      if (widget.lesson.funFact != null && widget.lesson.complete) {
        goToFunFact(context);
        return;
      }
      ServerManager.postCompleteLesson(widget.lesson.id).then((value) {
        if (value["success"] != null) {
          if (widget.lesson.funFact != null) {
            goToFunFact(context);
          } else {
            if (widget.lesson.next != null) {
              SingleLessonNavigator.loadNextLesson(widget.lesson.next, context);
            } else if (widget.lesson.manerismPages.isNotEmpty) {
              SingleLessonNavigator.showManerism(context, 0, widget.lesson.manerismPages);
            } else {
              Navigator.popUntil(context, ModalRoute.withName("Lessons"));
            }
          }
        } else if (value.values.first == "Lesson already done.") {
          if (widget.lesson.next != null) {
            SingleLessonNavigator.loadNextLesson(widget.lesson.next, context);
          } else if (widget.lesson.manerismPages.isNotEmpty) {
            SingleLessonNavigator.showManerism(context, 0, widget.lesson.manerismPages);
          } else {
            Navigator.popUntil(context, ModalRoute.withName("Lessons"));
          }
        } else {
          Scaffold.of(context).showSnackBar(SnackBar(
              content: Text(value.values.first), backgroundColor: Colors.red));
        }
      });
    } else {
      QuizItem item = widget.lesson.messages.firstWhere((el) => !el.isVerified);
      int index = widget.lesson.messages.indexOf(item);
      Navigator.pushNamed(context, "/ordinary_quiz",
          arguments: {"q_index": index, "lesson": widget.lesson});
    }
  }

  void goToFunFact(BuildContext context) {
    Navigator.pushReplacementNamed(context, "/wysiwyg_single", arguments: {
      "screenTitle": "Fun fact",
      "shownData": widget.lesson.funFact,
      "btnFunc": (context) {
        if (widget.lesson.next != null) {
          SingleLessonNavigator.loadNextLesson(widget.lesson.next, context);
        } else if (widget.lesson.manerismPages.isNotEmpty) {
          SingleLessonNavigator.showManerism(context, 0, widget.lesson.manerismPages);
        } else {
          Navigator.popUntil(context, ModalRoute.withName("Lessons"));
        }
      },
      "btnTitle": "CONTINUE",
      "backgroundUrl": widget.lesson.funFactImage
    });
  }
}
