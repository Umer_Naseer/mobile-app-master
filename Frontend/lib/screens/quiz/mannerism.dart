import 'package:chewie/chewie.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lingafriq/models/page.dart';
import 'package:lingafriq/ui_elements/audio_controls.dart';
import 'package:lingafriq/ui_elements/buttons.dart';
import 'package:lingafriq/ui_elements/loading_indicators.dart';
import 'package:lingafriq/ui_elements/questrial_regular_text.dart';
import 'package:lingafriq/utils/color.dart';
import 'package:lingafriq/utils/draft_js_to_textspan_flutter/draft_js_to_textspan_flutter.dart';
import 'package:lingafriq/utils/utils.dart';
import 'package:video_player/video_player.dart';

class MannerismScreen extends StatefulWidget {
  String screenTitle;
  PageItem mPageData;
  String btnTitle;
  Function btnFunc;
  String backgroundUrl;
  MannerismScreen(this.screenTitle, this.mPageData, this.btnFunc,
      {this.backgroundUrl, this.btnTitle = "CONTINUE"});

  @override
  _MannerismScreenState createState() => _MannerismScreenState();
}

class _MannerismScreenState extends State<MannerismScreen> {
  GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();
  VideoPlayerController _controller;
  ChewieController _chewieController;

  @override
  void initState() {
    super.initState();
    initChewie();
  }

  @override
  void dispose() {
    _controller.dispose();
    _chewieController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Container(
      color: hexToColor("#2E1224"),
      child: Scaffold(
          extendBodyBehindAppBar: true,
          backgroundColor: Colors.white,
          key: _scaffoldkey,
          body: CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                floating: false,
                pinned: true,
                expandedHeight: 180.0,
                backgroundColor: hexToColor("#2e1224"),
                flexibleSpace: LayoutBuilder(builder:
                    (BuildContext context, BoxConstraints constraints) {
                  double height =
                      constraints.heightConstraints().constrainHeight();
                  return FlexibleSpaceBar(
                    centerTitle: true,
                    title: QuestrialRegularText(
                      widget.screenTitle,
                      24,
                      "#FFFFFF",
                      maxLines: height < 150 ? 1 : 3,
                      overflow: TextOverflow.ellipsis,
                    ),
                    background: getBackground(),
                  );
                }),
              ),
              SliverList(
                  delegate: SliverChildBuilderDelegate(
                      (BuildContext context, int index) {
                return Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 24, vertical: 32),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      if (widget.mPageData.video != null && _chewieController != null)... [
                        Chewie(
                          controller: _chewieController,
                        ),
                      ] else if (widget.mPageData.video != null) ... [
                        Center(child: LoadingIndicator())
                      ],
                      if (widget.mPageData.image != null)
                        Align(
                          alignment: Alignment.centerLeft,
                          child: SizedBox(
                            width: MediaQuery.of(context).size.width * 0.5,
                            height: MediaQuery.of(context).size.height * 0.3,
                            child: InkWell(
                                onTap: () {
                                  showWidgetInDialog(context,
                                      Image.network(widget.mPageData.image));
                                },
                                child: Image.network(widget.mPageData.image)),
                          ),
                        ),
                      if (widget.mPageData.audio != null)...[
                        AudioControls(
                          widget.mPageData.audio,
                          key: Key(widget.mPageData.id.toString()),
                        ),
                        SizedBox(height: 5)
                      ],
                      Container(
                          child: DraftJSFlutter(widget.mPageData.message),
                          alignment: Alignment.centerLeft),
                      SizedBox(height: 48),
                      OrangeAuthBtn(widget.btnTitle, false, () async {
                        widget.btnFunc(context);
                      }),
                      SizedBox(height: 48)
                    ],
                  ),
                );
              }, childCount: 1))
            ],
          )));

  void initChewie() async {
    if (widget.mPageData.video != null) {
      _controller = VideoPlayerController.network(
          widget.mPageData.video);
      await _controller.initialize().then((v){setState(() {});});
      _chewieController = ChewieController(
        videoPlayerController: _controller,
        aspectRatio: _controller.value.aspectRatio,
        autoPlay: false,
        looping: false,
      );
    }
  }

  Widget getBackground() {
    if (widget.backgroundUrl != null) {
      return Image.network(
        widget.backgroundUrl,
        fit: BoxFit.cover,
      );
    } else {
      return Container(
        color: hexToColor("#2e1224"),
      );
    }
  }
}
