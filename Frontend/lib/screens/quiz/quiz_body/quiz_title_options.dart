
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:lingafriq/models/content.dart';
import 'package:lingafriq/models/option.dart';
import 'package:lingafriq/models/quiz_item.dart';
import 'package:lingafriq/ui_elements/audio_controls.dart';
import 'package:lingafriq/ui_elements/loading_indicators.dart';
import 'package:lingafriq/ui_elements/quiz_option_list_item.dart';
import 'package:lingafriq/utils/draft_js_to_textspan_flutter/draft_js_to_textspan_flutter.dart';
import 'package:lingafriq/utils/utils.dart';
import 'package:video_player/video_player.dart';

class QuizHeader extends StatefulWidget {
  final List<ContentItem> items;

  QuizHeader(this.items);

  @override
  _QuizHeaderState createState() => _QuizHeaderState();
}

class _QuizHeaderState extends State<QuizHeader> {
  Map<int, VideoPlayerController> _controllers = {};
  Map<int, ChewieController> _chewieControllers = {};

  @override
  Widget build(BuildContext context) {
    List<ContentItem> videoContentItems = widget.items.where((el) => el.typeContent == "video").toList();
    initChewies(videoContentItems);
    return SliverList(
      delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          ContentItem item = widget.items[index];
          Widget itemWidget;
          switch (item.typeContent) {
            case "text":
              itemWidget = DraftJSFlutter(item.content);
              break;
            case "image":
              itemWidget = Align(
                alignment: Alignment.centerLeft,
                child: SizedBox(
                  width: MediaQuery.of(context).size.width * 0.5,
                  height: MediaQuery.of(context).size.height * 0.3,
                  child: InkWell(
                      onTap: () {
                        showWidgetInDialog(context,
                            Image.network(item.content));
                      },
                      child: Image.network(item.content)),
                ),
              );
              break;
            case "audio":
              itemWidget = AudioControls(
                item.content,
                key: Key(item.id.toString()),
              );
              break;
            case "video":
              if (_chewieControllers[item.id] != null) {
                itemWidget = Chewie(
                  controller: _chewieControllers[item.id],
                );
              } else {
                itemWidget =
                    Center(child: LoadingIndicator());
              }
              break;
            default:
              return SizedBox.shrink();
          }
          return Container(
            padding: EdgeInsets.symmetric(vertical: 8),
            child: itemWidget,
          );
        },
        childCount: widget.items.length,
      ),
    );
  }

  void initChewies(List<ContentItem> videoContentItems) async {
    if (_chewieControllers.isNotEmpty) {
      return;
    }
    videoContentItems.forEach((el) async {
      if (el.content != null) {
        _controllers[el.id] = VideoPlayerController.network(
            el.content);
        await _controllers[el.id].initialize().then((v) {
          setState(() {});
        });
        _chewieControllers[el.id] = ChewieController(
          videoPlayerController: _controllers[el.id],
          aspectRatio: _controllers[el.id].value.aspectRatio,
          autoPlay: false,
          looping: false,
        );
        _chewieControllers[el.id].addListener(() {
          print("go on");
        });
      }
    });
  }
}



class QuizOptions extends StatefulWidget {
  QuizItem item;
  QuizOptions(this.item);

  @override
  State<StatefulWidget> createState() => _QuizOptionsState();
}

class _QuizOptionsState extends State<QuizOptions> {
  List<Option> options;

  @override
  void initState() {
    options = widget.item.options
        .where((el) => el.optionResources.isNotEmpty)
        .toList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SliverList(
      delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          Option currentItem = options[index];
          QuizItem iItem = widget.item;
          return SizedBox(
              child: QuizOptionListItem(currentItem.optionResources, () {
            setState(() {
              iItem.selectedOptionId = currentItem.id;
              iItem.isChecking = false;
            });
          },
                  isActive: iItem.optionsIsActive,
                  isHighlighted: iItem.selectedOptionId == currentItem.id,
                  isChecking: iItem.isChecking &&
                      currentItem.id == iItem.selectedOptionId,
                  isCorrect: iItem.correctOptionId == iItem.selectedOptionId));
        },
        childCount: options.length,
      ),
    );
  }
}
