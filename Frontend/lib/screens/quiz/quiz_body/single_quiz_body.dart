import 'package:flutter/material.dart';
import 'package:lingafriq/models/quiz_item.dart';
import 'package:lingafriq/screens/quiz/quiz_body/quiz_title_options.dart';

class QuizBody extends StatefulWidget {
  final QuizItem item;

  QuizBody(this.item);

  @override
  State<StatefulWidget> createState() => QuizBodyState();
}

class QuizBodyState extends State<QuizBody> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        QuizHeader(widget.item.questionResources),
        //QuizOptions(widget.item.options),
      ],
    );
  }
}
