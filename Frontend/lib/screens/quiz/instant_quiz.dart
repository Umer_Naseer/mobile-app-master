import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:lingafriq/iteractor/server_manager.dart';
import 'package:lingafriq/models/lesson.dart';
import 'package:lingafriq/models/quiz_item.dart';
import 'package:lingafriq/screens/quiz/quiz_body/quiz_title_options.dart';
import 'package:lingafriq/ui_elements/buttons.dart';
import 'package:lingafriq/ui_elements/loading_error.dart';
import 'package:lingafriq/ui_elements/loading_indicators.dart';
import 'package:lingafriq/ui_elements/questrial_regular_text.dart';
import 'package:lingafriq/utils/color.dart';
import 'package:lingafriq/utils/singleLessonNavigator.dart';
import 'package:lingafriq/utils/utils.dart';

class InstantQuiz extends StatefulWidget {
  final int id;
  final String background;
  final int quizIndex;
  bool isOrdinaryQuiz;

  InstantQuiz(this.id, this.background,
      {this.quizIndex = 0, this.isOrdinaryQuiz = false});

  @override
  _InstantQuizState createState() => _InstantQuizState();
}

class _InstantQuizState extends State<InstantQuiz> {
  bool get isOrdinary {
    return widget.isOrdinaryQuiz && lesson.messages.length > 1;
  }

  Future<Lesson> _lessonFuture;
  QuizItem item;
  bool isLoading = false;
  Lesson lesson;

  void reloadData() {
    setState(() {
      _lessonFuture = getData(widget.id);
    });
  }

  @override
  void initState() {
    _lessonFuture = getData(widget.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) => Container(
        color: hexToColor("#2E1224"),
        child: Scaffold(
          extendBodyBehindAppBar: true,
          backgroundColor: Colors.white,
          body: FutureBuilder<Lesson>(
              future: _lessonFuture,
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.hasError) {
                  print(snapshot.error);
                  return LoadingError(ServerManager.defaultError, () {
                    setState(() {
                      isLoading = true;
                    });
                  });
                }
                if (!snapshot.hasData) {
                  return Center(child: LoadingIndicator());
                }
                isLoading = false;
                lesson = snapshot.data;
                item = lesson.messages[widget.quizIndex];
                if (lesson.messages.isEmpty) {
                  return Container(child: Center(child: Text("No data")));
                }
                return CustomScrollView(slivers: getSlivers(item));
              }),
        ),
      );

  List<Widget> getSlivers(QuizItem quizItem) {
    List<Widget> list = [
      SliverAppBar(
        floating: false,
        pinned: true,
        expandedHeight: 180.0,
        backgroundColor: hexToColor("#2e1224"),
        actions: <Widget>[
          if (item != null && item.hint.isNotEmpty)
            SizedBox(
              width: 32,
              height: 32,
              child: FlatButton(
                  padding: EdgeInsets.all(0.0),
                  onPressed: () {
                    showHint(context, item.hint);
                  },
                  child: Image.asset('assets/flash.png')),
            ),
        ],
        flexibleSpace: LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
              double height = constraints.heightConstraints().constrainHeight();
          return FlexibleSpaceBar(
            centerTitle: true,
            title: QuestrialRegularText(
              lesson.name,
              24,
              "#FFFFFF",
              maxLines: height < 150 ? 1 : 3,
              overflow: TextOverflow.ellipsis,
            ),
            background: getBackground(),
          );
        }),
      )
    ];
    if (quizItem.questionResources.isNotEmpty) {
      list.add(SliverPadding(
        padding: EdgeInsets.only(left: 24, right: 24, top: 24),
        sliver: QuizHeader(quizItem.questionResources),
      ));
    }
    if (quizItem.options.isNotEmpty) {
      list.add(SliverPadding(padding: EdgeInsets.only(top: 24)));
      list.add(SliverPadding(
        padding: EdgeInsets.symmetric(vertical: 24, horizontal: 0),
        sliver: QuizOptions(quizItem),
      ));
      list.add(SliverPadding(padding: EdgeInsets.only(bottom: 24)));
    }
    list.add(
      SliverToBoxAdapter(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 16, horizontal: 24),
          child: Align(
              alignment: FractionalOffset.bottomCenter,
              child: OrangeAuthBtn(getBtnText(), false, () {
                item.isVerified = true;
                onContinueClick(lesson.id, context);
              })),
        ),
      ),
    );
    return list;
  }

  String getBtnText() {
    return isOrdinary ? "NEXT" : item.selectedOptionId == -1 ? "CHECK"
        : item.selectedOptionId == item.correctOptionId ? "CONTINUE" : "TRY AGAIN";
  }

  Widget getBackground() {
    if (widget.background != null) {
      return Image.network(
        widget.background,
        fit: BoxFit.cover,
      );
    } else {
      return Container(
        color: hexToColor("#2e1224"),
      );
    }
  }

  Future<Lesson> getData(int id) async {
    Response response = await ServerManager.getLesson(id);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      Map<String, Object> data = json.decode(utf8.decode(response.bodyBytes));
      print(data);
      Lesson lesson = Lesson.fromJson(data);
      return Future.delayed(Duration(milliseconds: 300)).then((value) => lesson);
    } else {
      throw Exception('Failed to load the lesson');
    }
  }

  void onContinueClick(int lessonId, BuildContext context) async {
    if (isOrdinary) {
      Navigator.pushNamed(context, "/ordinary_quiz",
          arguments: {"q_index": 1, "lesson": lesson});
    } else {
      if (item != null && !item.isChecking) {
        if (item.selectedOptionId == -1) {
          Scaffold.of(context)
              .showSnackBar(SnackBar(content: Text("Choose the answer!")));
          return;
        }
        setState(() {
          item.isChecking = true;
          item.optionsIsActive = false;
        });
      } else if (item != null &&
          item.isChecking &&
          item.selectedOptionId == item.correctOptionId) {
        if (lesson.funFact != null && lesson.complete) {
          goToFunFact(context);
          return;
        }
        ServerManager.postCompleteLesson(lessonId).then((value) {
          if (value["success"] != null) {
            if (lesson.funFact != null) {
              goToFunFact(context);
            } else {
              if (lesson.next != null) {
                SingleLessonNavigator.loadNextLesson(lesson.next, context);
              } else if (lesson.manerismPages.isNotEmpty) {
                SingleLessonNavigator.showManerism(context, 0, lesson.manerismPages);
              } else {
                Navigator.pop(context);
              }
            }
          } else if (value.values.first == "Lesson already done.") {
            if (lesson.next != null) {
              SingleLessonNavigator.loadNextLesson(lesson.next, context);
            } else if (lesson.manerismPages.isNotEmpty) {
              SingleLessonNavigator.showManerism(context, 0, lesson.manerismPages);
            } else {
              Navigator.pop(context);
            }
          } else {
            Scaffold.of(context).showSnackBar(SnackBar(
                content: Text(value.values.first),
                backgroundColor: Colors.red));
          }
        });
      } else {
        setState(() {
          item.selectedOptionId = -1;
          item.isChecking = false;
          item.optionsIsActive = true;
        });
      }
    }
  }

  void goToFunFact(BuildContext context) {
    Navigator.pushReplacementNamed(context, "/wysiwyg_single", arguments: {
      "screenTitle": "Fun fact",
      "shownData": lesson.funFact,
      "btnFunc": (context) {
        if (lesson.next != null) {
          SingleLessonNavigator.loadNextLesson(lesson.next, context);
        } else if (lesson.manerismPages.isNotEmpty) {
          SingleLessonNavigator.showManerism(context, 0, lesson.manerismPages);
        } else {
          Navigator.pop(context);
        }
      },
      "btnTitle": "CONTINUE",
      "backgroundUrl": lesson.funFactImage
    });
  }
}
