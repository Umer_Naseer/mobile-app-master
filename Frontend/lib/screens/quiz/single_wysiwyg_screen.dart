
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lingafriq/ui_elements/buttons.dart';
import 'package:lingafriq/ui_elements/questrial_regular_text.dart';
import 'package:lingafriq/utils/color.dart';
import 'package:lingafriq/utils/draft_js_to_textspan_flutter/draft_js_to_textspan_flutter.dart';

class SingleWysiwyg extends StatefulWidget {
  String screenTitle;
  Map<String, dynamic> shownData;
  String btnTitle;
  Function btnFunc;
  String backgroundUrl;
  SingleWysiwyg(this.screenTitle, this.shownData, this.btnFunc,
      {this.backgroundUrl, this.btnTitle = "CONTINUE"});

  @override
  _SingleWysiwygState createState() => _SingleWysiwygState();
}

class _SingleWysiwygState extends State<SingleWysiwyg> {
  GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) => Container(
      color: hexToColor("#2E1224"),
      child: Scaffold(
          extendBodyBehindAppBar: true,
          backgroundColor: Colors.white,
          key: _scaffoldkey,
          body: CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                floating: false,
                pinned: true,
                expandedHeight: 180.0,
                backgroundColor: hexToColor("#2e1224"),
                flexibleSpace: LayoutBuilder(builder:
                    (BuildContext context, BoxConstraints constraints) {
                      double height = constraints.heightConstraints().constrainHeight();
                  return FlexibleSpaceBar(
                    centerTitle: true,
                    title: QuestrialRegularText(
                      widget.screenTitle,
                      24,
                      "#FFFFFF",
                      maxLines: height < 150 ? 1 : 3,
                      overflow: TextOverflow.ellipsis,
                    ),
                    background: getBackground(),
                  );
                }),
              ),
              SliverList(
                  delegate: SliverChildBuilderDelegate(
                      (BuildContext context, int index) {
                return Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 24, vertical: 32),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                          child: DraftJSFlutter(widget.shownData),
                          alignment: Alignment.centerLeft),
                      SizedBox(height: 48),
                      OrangeAuthBtn(widget.btnTitle, false, () async {
                        widget.btnFunc(context);
                      }),
                      SizedBox(height: 48)
                    ],
                  ),
                );
              }, childCount: 1))
            ],
          )));

  Widget getBackground() {
    if (widget.backgroundUrl != null) {
      return Image.network(
        widget.backgroundUrl,
        fit: BoxFit.cover,
      );
    } else {
      return Container(
        color: hexToColor("#2e1224"),
      );
    }
  }
}
