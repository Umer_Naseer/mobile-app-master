import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:lingafriq/iteractor/server_manager.dart';
import 'package:lingafriq/models/lesson.dart';
import 'package:lingafriq/models/quiz_item.dart';
import 'package:lingafriq/screens/quiz/quiz_body/quiz_title_options.dart';
import 'package:lingafriq/ui_elements/buttons.dart';
import 'package:lingafriq/ui_elements/questrial_regular_text.dart';
import 'package:lingafriq/utils/color.dart';
import 'package:lingafriq/utils/utils.dart';

class OrdinaryQuiz extends StatefulWidget {
  final int quizIndex;
  Lesson lesson;

  OrdinaryQuiz(this.lesson, this.quizIndex);

  @override
  _OrdinaryQuizState createState() => _OrdinaryQuizState();
}

class _OrdinaryQuizState extends State<OrdinaryQuiz> {
  QuizItem item;

  @override
  void initState() {
    item = widget.lesson.messages[widget.quizIndex];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: hexToColor("#2E1224"),
      child: Scaffold(
        extendBodyBehindAppBar: true,
        backgroundColor: Colors.white,
        body: CustomScrollView(slivers: getSlivers(item)),
      ),
    );
  }

  void onContinueClick() async {
    int unverifiedQuizCount =
        widget.lesson.messages.where((el) => !el.isVerified).length;
    if (unverifiedQuizCount == 0) {
      Navigator.pushNamedAndRemoveUntil(
          context, "/ordinary_quiz_reslut", ModalRoute.withName('Lessons'),
          arguments: {"lesson": widget.lesson});
    } else {
      QuizItem item = widget.lesson.messages.firstWhere((el) => !el.isVerified);
      int nextIndex = widget.lesson.messages.indexOf(item);
      Navigator.pushNamed(context, "/ordinary_quiz",
          arguments: {"q_index": nextIndex, "lesson": widget.lesson});
    }
  }

  List<Widget> getSlivers(QuizItem quizItem) {
    List<Widget> list = [
      SliverAppBar(
        floating: false,
        pinned: true,
        expandedHeight: 180.0,
        backgroundColor: hexToColor("#2e1224"),
        actions: <Widget>[
          if (item != null && item.hint.isNotEmpty)
            SizedBox(
              width: 32,
              height: 32,
              child: FlatButton(
                  padding: EdgeInsets.all(0.0),
                  onPressed: () {
                    showHint(context, item.hint);
                  },
                  child: Image.asset('assets/flash.png')),
            ),
        ],
        flexibleSpace: LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
              double height = constraints.heightConstraints().constrainHeight();
          return FlexibleSpaceBar(
            centerTitle: true,
            title: QuestrialRegularText(
              widget.lesson.name,
              24,
              "#FFFFFF",
              maxLines: height < 150 ? 1 : 3,
              overflow: TextOverflow.ellipsis,
            ),
            background: Container(color: hexToColor("#2e1224")),
          );
        }),
      )
    ];
    if (quizItem.questionResources.isNotEmpty) {
      list.add(SliverPadding(
        padding: EdgeInsets.only(left: 24, right: 24, top: 24),
        sliver: QuizHeader(quizItem.questionResources),
      ));
    }
    if (quizItem.options.isNotEmpty) {
      list.add(SliverPadding(padding: EdgeInsets.only(top: 24)));
      list.add(SliverPadding(
        padding: EdgeInsets.symmetric(vertical: 24, horizontal: 0),
        sliver: QuizOptions(quizItem),
      ));
      list.add(SliverPadding(padding: EdgeInsets.only(bottom: 48)));
    }
    list.add(SliverToBoxAdapter(
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 16, horizontal: 24),
        child: Align(
            alignment: FractionalOffset.bottomCenter,
            child: OrangeAuthBtn("NEXT", false, () {
              item.isVerified = true;
              onContinueClick();
            })),
      ),
    ));
    return list;
  }

  Future<Lesson> getData(int id) async {
    Response response = await ServerManager.getLesson(id);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      Map<String, Object> data = json.decode(utf8.decode(response.bodyBytes));
      Lesson lesson = Lesson.fromJson(data);
      return Future.delayed(Duration(milliseconds: 300)).then((value) => lesson);
    } else {
      throw Exception('Failed to load the lesson');
    }
  }
}
