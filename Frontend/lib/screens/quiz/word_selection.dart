import 'package:chewie/chewie.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:lingafriq/iteractor/server_manager.dart';
import 'package:lingafriq/models/content.dart';
import 'package:lingafriq/models/lesson.dart';
import 'package:lingafriq/models/quiz_item.dart';
import 'package:lingafriq/models/ws_element.dart';
import 'package:lingafriq/ui_elements/audio_controls.dart';
import 'package:lingafriq/ui_elements/buttons.dart';
import 'package:lingafriq/ui_elements/loading_indicators.dart';
import 'package:lingafriq/ui_elements/popup_widget.dart';
import 'package:lingafriq/ui_elements/questrial_regular_text.dart';
import 'package:lingafriq/utils/color.dart';
import 'package:lingafriq/utils/draft_js_to_textspan_flutter/draft_js_to_textspan_flutter.dart';
import 'package:lingafriq/utils/singleLessonNavigator.dart';
import 'package:lingafriq/utils/utils.dart';
import 'package:video_player/video_player.dart';

class WordSelectionWidget extends StatefulWidget {
  final Lesson lesson;
  final int questionIndex;
  final String background;

  WordSelectionWidget(this.lesson, this.questionIndex, this.background);

  @override
  _WordSelectionState createState() => _WordSelectionState();
}

class _WordSelectionState extends State<WordSelectionWidget> {
  QuizItem quizItem;
  List<WSElement> elements;
  List<String> pool;
  bool isPassed = false;
  Map<int, VideoPlayerController> _controllers = {};
  Map<int, ChewieController> _chewieControllers = {};
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    //quizItem = widget.lesson.messages[widget.questionIndex];
    elements = WSElement.parseQuestion(widget.lesson.funFactText);
    pool = elements
        .where((element) => !element.isStatic)
        .map((e) => e.rightAnswer)
        .toList();
    pool.shuffle();
  }

  @override
  void dispose() {
    _controllers.forEach((key, value) {
      value.dispose();
    });
    _chewieControllers.forEach((key, value) {
      value.dispose();
    });
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Container(
        color: hexToColor("#2E1224"),
        child: Scaffold(
          extendBodyBehindAppBar: true,
          backgroundColor: Colors.white,
          key: _scaffoldKey,
          body: CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                floating: false,
                pinned: true,
                expandedHeight: 180.0,
                backgroundColor: hexToColor("#2e1224"),
                actions: <Widget>[
                  if (widget.lesson.infoMessage.isNotEmpty)
                    SizedBox(
                      width: 32,
                      height: 32,
                      child: FlatButton(
                          padding: EdgeInsets.all(0.0),
                          onPressed: () {
                            showHint(context, widget.lesson.infoMessage);
                          },
                          child: Image.asset('assets/flash.png')),
                    ),
                ],
                flexibleSpace: LayoutBuilder(builder:
                    (BuildContext context, BoxConstraints constraints) {
                  double height =
                      constraints.heightConstraints().constrainHeight();
                  return FlexibleSpaceBar(
                    centerTitle: true,
                    title: QuestrialRegularText(
                      widget.lesson.name,
                      24,
                      "#FFFFFF",
                      maxLines: height < 150 ? 1 : 3,
                      overflow: TextOverflow.ellipsis,
                    ),
                    background: getBackground(),
                  );
                }),
              ),
              SliverPadding(
                padding:
                    EdgeInsets.only(left: 24, right: 24, top: 24, bottom: 76),
                sliver: SliverToBoxAdapter(
                  child: Column(
//                    mainAxisSize: MainAxisSize.max,
//                    mainAxisAlignment: MainAxisAlignment.start,
//                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: getContent(),
                  ),
                ),
              )
            ],
          ),
        ),
      );

  List<Widget> getContent() {
    if (widget.lesson == null) {
      return [SizedBox()];
    }
    List<Widget> result = [];
    if (widget.lesson.lessonResources.isNotEmpty) {
      List<ContentItem> items = widget.lesson.lessonResources;
      List<ContentItem> videoContentItems =
          items.where((el) => el.typeContent == "video").toList();
      initChewies(videoContentItems);
      items.forEach((item) {
        Widget itemWidget;
        switch (item.typeContent) {
          case "text":
            itemWidget = DraftJSFlutter(item.content);
            break;
          case "image":
            itemWidget = Align(
              alignment: Alignment.centerLeft,
              child: SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                height: MediaQuery.of(context).size.height * 0.3,
                child: InkWell(
                    onTap: () {
                      showWidgetInDialog(context, Image.network(item.content));
                    },
                    child: Image.network(item.content)),
              ),
            );
            break;
          case "audio":
            itemWidget = AudioControls(
              item.content,
              key: Key(item.id.toString()),
            );
            break;
          case "video":
            if (_chewieControllers[item.id] != null) {
              itemWidget = Chewie(
                controller: _chewieControllers[item.id],
              );
            } else {
              itemWidget =
                  Center(child: LoadingIndicator());
            }
            break;
          default:
            return SizedBox.shrink();
        }
        result.add(itemWidget);
      });
    }
    result.addAll([
      RichText(
        text: TextSpan(
          style: TextStyle(
            fontFamily: "Questrial-Regular",
            height: 1.75,
            color: Color.fromARGB(
              255,
              64,
              64,
              64,
            ),
            fontSize: 17,
          ),
          children: <TextSpan>[
            for (WSElement element in elements) getTextSpan(element)
          ],
        ),
      ),
      SizedBox(height: 50),
      OrangeAuthBtn(isPassed ? "CONTINUE" : "CHECK", false, onContinueClick)
    ]);
    return result;
  }

  void initChewies(List<ContentItem> videoContentItems) async {
    if (_chewieControllers.isNotEmpty) {
      return;
    }
    videoContentItems.forEach((el) async {
      if (el.content != null) {
        _controllers[el.id] = VideoPlayerController.network(el.content);
        await _controllers[el.id].initialize().then((v) {
          setState(() {});
        });
        _chewieControllers[el.id] = ChewieController(
          videoPlayerController: _controllers[el.id],
          aspectRatio: _controllers[el.id].value.aspectRatio,
          autoPlay: false,
          looping: false,
        );
        _chewieControllers[el.id].addListener(() {
          print("go on");
        });
      }
    });
  }

  Widget getBackground() {
    if (widget.background != null) {
      return Image.network(
        widget.background,
        fit: BoxFit.cover,
      );
    } else {
      return Container(
        color: hexToColor("#2e1224"),
      );
    }
  }

  TextSpan getTextSpan(WSElement element) {
    if (element.isStatic) {
      return TextSpan(text: element.text);
    }

    void onTap() {
      if (element.isChecked && element.isCorrect()) {
        return;
      }
      if (element.text != null) {
        pool.add(element.text);
      }
      setState(() {
        element.text = null;
        element.isChecked = false;
      });
      showPickerDialog(context, element);
    }

    if (element.text == null) {
      TextStyle style = TextStyle(
          fontSize: 16,
          height: 1.5,
          fontFamily: "Questrial-Regular",
          color: Color.fromARGB(255, 183, 183, 183),
          backgroundColor: Color.fromARGB(255, 239, 239, 239));
      return TextSpan(
          text: upendSpaces(element.hint),
          style: style,
          recognizer: TapGestureRecognizer()..onTap = onTap);
    }
    if (!element.isChecked) {
      TextStyle style = TextStyle(
          fontSize: 16,
          height: 1.5,
          fontFamily: "Questrial-Regular",
          color: Color.fromARGB(255, 64, 64, 64),
          backgroundColor: Color.fromARGB(255, 239, 239, 239));
      return TextSpan(
          text: upendSpaces(element.text),
          style: style,
          recognizer: TapGestureRecognizer()..onTap = onTap);
    }
    if (element.isCorrect()) {
      TextStyle style = TextStyle(
          fontSize: 16,
          height: 1.5,
          fontFamily: "Questrial-Regular",
          color: Color.fromARGB(255, 37, 165, 136),
          backgroundColor: Color.fromARGB(255, 229, 242, 239));
      return TextSpan(
          text: upendSpaces(element.text),
          style: style,
          recognizer: TapGestureRecognizer()..onTap = onTap);
    } else {
      TextStyle style = TextStyle(
          fontSize: 16,
          height: 1.5,
          fontFamily: "Questrial-Regular",
          color: Color.fromARGB(255, 219, 63, 63),
          backgroundColor: Color.fromARGB(255, 247, 231, 231));
      return TextSpan(
          text: upendSpaces(element.text),
          style: style,
          recognizer: TapGestureRecognizer()..onTap = onTap);
    }
  }

  void showPickerDialog(BuildContext context, WSElement element) {
    new Picker(
        adapter: PickerDataAdapter<String>(pickerdata: [pool], isArray: true),
        hideHeader: true,
        title: QuestrialRegularText("Pool of words", 24, "#404040"),
        onConfirm: (Picker picker, List value) {
          String selected = picker.getSelectedValues()[0];
          pool.remove(selected);
          setState(() {
            element.text = selected;
          });
        }).showDialog(context);
  }

  String upendSpaces(String text) {
    return " $text ";
  }

  void onContinueClick() async {
    if (isPassed) {
      ServerManager.postCompleteLesson(widget.lesson.id).then((value) {
        if (value["success"] != null) {
          if (widget.lesson.next != null) {
            SingleLessonNavigator.loadNextLesson(widget.lesson.next, context);
          } else if (widget.lesson.manerismPages.isNotEmpty) {
            SingleLessonNavigator.showManerism(context, 0, widget.lesson.manerismPages);
          } else {
            Navigator.pop(_scaffoldKey.currentState.context);
          }
        } else if (value.values.first == "Lesson already done.") {
          if (widget.lesson.next != null) {
            SingleLessonNavigator.loadNextLesson(widget.lesson.next, context);
          } else if (widget.lesson.manerismPages.isNotEmpty) {
            SingleLessonNavigator.showManerism(context, 0, widget.lesson.manerismPages);
          } else {
            Navigator.pop(context);
          }

        } else {
          _scaffoldKey.currentState.showSnackBar(SnackBar(
              content: Text(value.values.first), backgroundColor: Colors.red));
        }
      });
    }
    if (pool.length == 0) {
      bool allCorrect = true;
      Iterable<WSElement> answers =
          elements.where((element) => !element.isStatic);
      setState(() {
        for (WSElement element in answers) {
          element.isChecked = true;
          if (!element.isCorrect()) {
            allCorrect = false;
          }
        }
        if (allCorrect) {
          if (!isPassed) {
            _scaffoldKey.currentState.showSnackBar(SnackBar(
                content: Text("All right! Press “Continue” to proceed.")));
          }
          isPassed = true;
        }
      });
    } else {
      _scaffoldKey.currentState.showSnackBar(
          SnackBar(content: Text("Please answer all the questions!")));
    }
  }
}
