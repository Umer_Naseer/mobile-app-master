import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:lingafriq/iteractor/server_manager.dart';
import 'package:lingafriq/models/languages_data.dart';
import 'package:lingafriq/ui_elements/languages_list_item.dart';
import 'package:lingafriq/ui_elements/loading_error.dart';
import 'package:lingafriq/ui_elements/loading_indicators.dart';
import 'package:lingafriq/ui_elements/questrial_regular_text.dart';
import 'package:lingafriq/utils/color.dart';
import 'package:route_observer_mixin/route_observer_mixin.dart';

class ActiveLearning extends StatefulWidget {
  ActiveLearning({Key key}) : super(key: key);

  bool isLoading = false;

  @override
  _ActiveLearningState createState() => _ActiveLearningState();
}

class _ActiveLearningState extends State<ActiveLearning> with RouteAware, RouteObserverMixin {
  List<LanguagesData> languagesData;

  @override
  void didPopNext() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) => Container(
        color: hexToColor("#2E1224"),
        child: Scaffold(
            extendBodyBehindAppBar: true,
            backgroundColor: Colors.white,
            body: FutureBuilder<List<LanguagesData>>(
                future: getData(),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (snapshot.hasError) {
                    return LoadingError(ServerManager.defaultError, () {
                      setState(() {
                        widget.isLoading = true;
                      });
                    });
                  }
                  if (!snapshot.hasData) {
                    return Center(child: LoadingIndicator());
                  }
                  widget.isLoading = false;
                  languagesData = snapshot.data;
                  return CustomScrollView(
                    slivers: <Widget>[
                      SliverAppBar(
                        floating: false,
                        pinned: true,
                        expandedHeight: 180.0,
                        backgroundColor: hexToColor("#2e1224"),
                        flexibleSpace: LayoutBuilder(builder:
                            (BuildContext context, BoxConstraints constraints) {
                          return FlexibleSpaceBar(
                            centerTitle: true,
                            title: QuestrialRegularText(
                                "Active Learning", 24, "#FFFFFF"),
                            background: Container(
                              color: hexToColor("#2e1224"),
                            ),
                          );
                        }),
                      ),
                      if (languagesData.length == 0)
                        SliverFillRemaining(
                          hasScrollBody: false,
                          child: Container(
                            alignment: Alignment.center,
                            child: QuestrialRegularText(
                              "The list is empty.\nStart learning now!",
                              24,
                              "#404040",
                              textAlign: TextAlign.center,
                            ),
                          ),
                        )
                      else
                        SliverList(
                            delegate: SliverChildBuilderDelegate(
                                (BuildContext context, int index) {
                          LanguagesData data = languagesData[index];
                          return LanguagesListItem(
                              data, true, CellType.language, data.background);
                        }, childCount: languagesData.length))
                    ],
                  );
                })),
      );

  Future<List<LanguagesData>> getData() async {
    final response = await ServerManager.getActiveLanguages();
    if ((response as Response).statusCode >= 200 &&
        (response as Response).statusCode < 300) {
      List<dynamic> array = json.decode(utf8.decode(response.bodyBytes));
      return array.map((item) => LanguagesData.fromJson(item)).toList();
    } else {
      throw Exception('Failed to load active languages');
    }
  }
}
