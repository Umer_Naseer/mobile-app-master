import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lingafriq/iteractor/server_manager.dart';
import 'package:lingafriq/models/languages_data.dart';
import 'package:lingafriq/ui_elements/buttons.dart';
import 'package:lingafriq/ui_elements/questrial_regular_text.dart';
import 'package:lingafriq/utils/color.dart';
import 'package:lingafriq/utils/draft_js_to_textspan_flutter/draft_js_to_textspan_flutter.dart';

class Introduction extends StatefulWidget {
  final LanguagesData languagesData;
  final bool hasAlreadyBegun;
  bool isLoading = false;
  Introduction(this.languagesData, this.hasAlreadyBegun);

  @override
  _IntroductionState createState() => _IntroductionState();
}

class _IntroductionState extends State<Introduction> {
  GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) => Container(
      color: hexToColor("#2E1224"),
      child: Scaffold(
          extendBodyBehindAppBar: true,
          backgroundColor: Colors.white,
          key: _scaffoldkey,
          body: CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                floating: false,
                pinned: true,
                expandedHeight: 180.0,
                backgroundColor: hexToColor("#2e1224"),
                flexibleSpace: LayoutBuilder(builder:
                    (BuildContext context, BoxConstraints constraints) {
                  return FlexibleSpaceBar(
                    titlePadding:
                        EdgeInsets.symmetric(vertical: 24.0, horizontal: 24.0),
                    title: QuestrialRegularText("Introduction", 24, "#FFFFFF"),
                    background: Container(
                      color: hexToColor("#2e1224"),
                    ),
                  );
                }),
              ),
              SliverList(
                  delegate: SliverChildBuilderDelegate(
                      (BuildContext context, int index) {
                Map data = widget.languagesData.description;
                return Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 24, vertical: 32),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                          child: DraftJSFlutter(data),
                          alignment: Alignment.centerLeft),
                      SizedBox(height: 48),
                      OrangeAuthBtn(widget.hasAlreadyBegun ? "CONTINUE LEARNING" : "BEGIN", false, () async {
                        setState(() {
                          widget.isLoading = true;
                        });
                        if (widget.hasAlreadyBegun) {
                          Navigator.pushReplacementNamed(context, "/sections", arguments: {
                            'isActive': true,
                            'id': widget.languagesData.id,
                            'background':  widget.languagesData.background != null ?
                                            widget.languagesData.background : null,
                            'isNeedToShownLastSection' : true
                          });
                        } else {
                          await activateLanguage(widget.languagesData);
                        }

                      }),
                      SizedBox(height: 48)
                    ],
                  ),
                );
              }, childCount: 1))
            ],
          )));

  Future activateLanguage(LanguagesData data) async {
    await ServerManager.postActivateLanguage(data.id).then((response) {
      if (response["success"] != null) {
        Navigator.pushReplacementNamed(context, "/sections", arguments: {
          'isActive': false,
          'id': data.id,
          'background': data.background != null ? data.background : null
        });
      } else {
        _scaffoldkey.currentState.showSnackBar(SnackBar(
            content: Text("${response.values.first}"),
            backgroundColor: Colors.red));
      }
    });
  }
}
