import 'dart:convert';

import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:lingafriq/iteractor/server_manager.dart';
import 'package:lingafriq/models/content.dart';
import 'package:lingafriq/models/lesson.dart';
import 'package:lingafriq/ui_elements/audio_controls.dart';
import 'package:lingafriq/ui_elements/buttons.dart';
import 'package:lingafriq/ui_elements/loading_error.dart';
import 'package:lingafriq/ui_elements/loading_indicators.dart';
import 'package:lingafriq/ui_elements/questrial_regular_text.dart';
import 'package:lingafriq/utils/color.dart';
import 'package:lingafriq/utils/draft_js_to_textspan_flutter/draft_js_to_textspan_flutter.dart';
import 'package:lingafriq/utils/singleLessonNavigator.dart';
import 'package:lingafriq/utils/utils.dart';
import 'package:video_player/video_player.dart';
import 'package:quiver/async.dart';

class LessonWidget extends StatefulWidget {
  final int id;
  final String background;

  LessonWidget(this.id, this.background);

  @override
  _LessonState createState() => _LessonState();
}

class _LessonState extends State<LessonWidget> {
  bool isLoading = false;
  Lesson lesson;

  Map<int, VideoPlayerController> _controllers = {};
  Map<int, ChewieController> _chewieControllers = {};

  @override
  void dispose() {
    _controllers.forEach((key, value) {
      value.dispose();
    });
    _chewieControllers.forEach((key, value) {
      value.dispose();
    });
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Container(
        color: hexToColor("#2E1224"),
        child: Scaffold(
          extendBodyBehindAppBar: true,
          backgroundColor: Colors.white,
          body: FutureBuilder<Lesson>(
              future: getData(widget.id),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.hasError) {
                  print(snapshot.error);
                  return LoadingError(ServerManager.defaultError, () {
                    setState(() {
                      isLoading = true;
                    });
                  });
                }
                if (!snapshot.hasData) {
                  return Center(child: LoadingIndicator());
                }
                isLoading = false;
                lesson = snapshot.data;
                List<ContentItem> videoContentItems = lesson.lessonResources
                    .where((el) => el.typeContent == "video")
                    .toList();
                initChewies(videoContentItems);
                return Stack(
                  children: <Widget>[
                    CustomScrollView(
                      slivers: <Widget>[
                        SliverAppBar(
                          floating: false,
                          pinned: true,
                          expandedHeight: 180.0,
                          backgroundColor: hexToColor("#2e1224"),
                          actions: <Widget>[
                            if (lesson.infoMessage.isNotEmpty)
                              SizedBox(
                                width: 32,
                                height: 32,
                                child: FlatButton(
                                    padding: EdgeInsets.all(0.0),
                                    onPressed: () {
                                      showHint(context, lesson.infoMessage);
                                    },
                                    child: Image.asset('assets/flash.png')),
                              ),
                          ],
                          flexibleSpace: LayoutBuilder(builder:
                              (BuildContext context,
                                  BoxConstraints constraints) {
                            double height = constraints
                                .heightConstraints()
                                .constrainHeight();
                            return FlexibleSpaceBar(
                              centerTitle: true,
                              title: QuestrialRegularText(
                                lesson.name,
                                24,
                                "#FFFFFF",
                                maxLines: height < 150 ? 1 : 3,
                                overflow: TextOverflow.ellipsis,
                              ),
                              background: getBackground(),
                            );
                          }),
                        ),
                        SliverPadding(
                          padding: EdgeInsets.only(
                              left: 24, right: 24, top: 24, bottom: 24),
                          sliver: SliverList(
                            delegate: SliverChildBuilderDelegate(
                              (BuildContext context, int index) {
                                ContentItem item =
                                    lesson.lessonResources[index];
                                Widget itemWidget;
                                switch (item.typeContent) {
                                  case "text":
                                    itemWidget = DraftJSFlutter(item.content);
                                    break;
                                  case "image":
                                    itemWidget = Align(
                                      alignment: Alignment.centerLeft,
                                      child: SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.5,
                                        height:
                                            MediaQuery.of(context).size.height *
                                                0.3,
                                        child: InkWell(
                                            onTap: () {
                                              showWidgetInDialog(context,
                                                  Image.network(item.content));
                                            },
                                            child: Image.network(item.content)),
                                      ),
                                    );
                                    break;
                                  case "audio":
                                    itemWidget = AudioControls(
                                      item.content,
                                      key: Key(item.id.toString()),
                                    );
                                    break;
                                  case "video":
                                    if (_chewieControllers[item.id] != null) {
                                      itemWidget = Chewie(
                                        controller: _chewieControllers[item.id],
                                      );
                                    } else {
                                      itemWidget =
                                          Center(child: LoadingIndicator());
                                    }
                                    break;
                                  default:
                                    return SizedBox.shrink();
                                }
                                return Container(
                                  padding: EdgeInsets.symmetric(vertical: 8),
                                  child: itemWidget,
                                );
                              },
                              childCount: lesson.lessonResources.length,
                            ),
                          ),
                        ),
                        SliverToBoxAdapter(
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 16, horizontal: 24),
                            child: Align(
                                alignment: FractionalOffset.bottomCenter,
                                child: OrangeAuthBtn("CONTINUE", false, () {
                                  onContinueClick(lesson.id, context);
                                })),
                          ),
                        )
                      ],
                    ),
                  ],
                );
              }),
        ),
      );

  Widget getBackground() {
    if (widget.background != null) {
      return Image.network(
        widget.background,
        fit: BoxFit.cover,
      );
    } else {
      return Container(
        color: hexToColor("#2e1224"),
      );
    }
  }

  void initChewies(List<ContentItem> videoContentItems) async {
    if (_chewieControllers.isNotEmpty) {
      return;
    }
    videoContentItems.forEach((el) async {
      if (el.content != null) {
        _controllers[el.id] = VideoPlayerController.network(el.content);
        await _controllers[el.id].initialize().then((v) {
          setState(() {});
        });
        _chewieControllers[el.id] = ChewieController(
          videoPlayerController: _controllers[el.id],
          aspectRatio: _controllers[el.id].value.aspectRatio,
          autoPlay: false,
          looping: false,
        );
        _chewieControllers[el.id].addListener(() {
          print("go on");
        });
      }
    });
  }

  Future<Lesson> getData(int id) async {
    Response response = await ServerManager.getLesson(id);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      Map<String, Object> data = json.decode(utf8.decode(response.bodyBytes));
      print(data);
      Lesson lesson = Lesson.fromJson(data);
      return Future.delayed(Duration(milliseconds: 300)).then((value) => lesson);
    } else {
      throw Exception('Failed to load the lesson');
    }
  }

  void onContinueClick(int lessonId, BuildContext context) async {
    ServerManager.postCompleteLesson(lessonId).then((value) {
      if (value["success"] != null) {
        if (lesson.funFact != null) {
          Navigator.pushReplacementNamed(context, "/wysiwyg_single",
              arguments: {
                "screenTitle": "Fun fact",
                "shownData": lesson.funFact,
                "btnFunc": (context) {
                  if (lesson.next != null) {
                    SingleLessonNavigator.loadNextLesson(lesson.next, context);
                  } else if (lesson.manerismPages.isNotEmpty) {
                    SingleLessonNavigator.showManerism(
                        context, 0, lesson.manerismPages);
                  } else {
                    Navigator.pop(context);
                  }
                },
                "btnTitle": "CONTINUE",
                "backgroundUrl": lesson.funFactImage
              });
        } else {
          if (lesson.next != null) {
            SingleLessonNavigator.loadNextLesson(lesson.next, context);
          } else if (lesson.manerismPages.isNotEmpty) {
            SingleLessonNavigator.showManerism(
                context, 0, lesson.manerismPages);
          } else {
            Navigator.pop(context);
          }
        }
      } else if (value.values.first == "Lesson already done.") {
        if (lesson.next != null) {
          SingleLessonNavigator.loadNextLesson(lesson.next, context);
        } else if (lesson.manerismPages.isNotEmpty) {
          SingleLessonNavigator.showManerism(context, 0, lesson.manerismPages);
        } else {
          Navigator.pop(context);
        }
      } else {
        Scaffold.of(context).showSnackBar(SnackBar(
            content: Text(value.values.first), backgroundColor: Colors.red));
      }
    });
  }
}
