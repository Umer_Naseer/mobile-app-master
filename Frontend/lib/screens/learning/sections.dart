import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:http/http.dart';
import 'package:lingafriq/iteractor/server_manager.dart';
import 'package:lingafriq/models/languages_data.dart';
import 'package:lingafriq/ui_elements/languages_list_item.dart';
import 'package:lingafriq/ui_elements/loading_error.dart';
import 'package:lingafriq/ui_elements/loading_indicators.dart';
import 'package:lingafriq/ui_elements/questrial_regular_text.dart';
import 'package:lingafriq/utils/color.dart';
import 'package:route_observer_mixin/route_observer_mixin.dart';

class SectionList extends StatefulWidget {
  bool isLoading = false;
  bool isActive;
  int id;
  bool isNeedToShownLastSection;
  String background;

  SectionList(this.isActive, this.id, this.background, {this.isNeedToShownLastSection = false});

  @override
  _SectionList createState() => _SectionList();
}

class _SectionList extends State<SectionList> with RouteAware, RouteObserverMixin {
  LanguagesData languagesData;

  @override
  void didPopNext() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) => Container(
        color: hexToColor("#2E1224"),
        child: Scaffold(
            extendBodyBehindAppBar: true,
            backgroundColor: Colors.white,
            body: FutureBuilder<LanguagesData>(
                future: getData(widget.isActive, widget.id),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (snapshot.hasError) {
                    return LoadingError(ServerManager.defaultError, () {
                      setState(() {
                        widget.isLoading = true;
                      });
                    });
                  }
                  if (!snapshot.hasData) {
                    return Center(child: LoadingIndicator());
                  }
                  widget.isLoading = false;
                  languagesData = snapshot.data;
                  LanguagesData data = languagesData.sections.firstWhere((el) => !el.isComplete, orElse: () => null);
                  if (widget.isNeedToShownLastSection && data != null) {
                    widget.isNeedToShownLastSection = false;
                    SchedulerBinding.instance.addPostFrameCallback((_) {
                      LanguagesData data = languagesData.sections.firstWhere((el) => !el.isComplete, orElse: () => null);
                      Navigator.pushNamed(context, "/lessons", arguments: {
                        'isActive': true,
                        'id': data.id,
                        'background': data.background != null ? data.background : null,
                        'mannerism': data.pages != null ? data.pages : null
                      }).then((value){
                        setState(() {});
                      });
                    });
                    return SizedBox.expand();
                  }
                  return CustomScrollView(
                    slivers: <Widget>[
                      SliverAppBar(
                        floating: false,
                        pinned: true,
                        expandedHeight: 180.0,
                        backgroundColor: hexToColor("#2e1224"),
                        flexibleSpace: LayoutBuilder(builder:
                            (BuildContext context, BoxConstraints constraints) {
                              double height = constraints.heightConstraints().constrainHeight();
                          return FlexibleSpaceBar(
                            centerTitle: true,
                            title: QuestrialRegularText(
                              languagesData.name,
                              24,
                              "#FFFFFF",
                              maxLines: height < 150 ? 1 : 3,
                              overflow: TextOverflow.ellipsis,
                            ),
                            background: getBackground(),
                          );
                        }),
                      ),
                      SliverList(
                          delegate: SliverChildBuilderDelegate(
                              (BuildContext context, int index) {
                        LanguagesData data = languagesData.sections[index];
                        bool isClickable = true;
                        if (index > 0) {
                          LanguagesData previouslyData =
                              languagesData.sections[index - 1];
                          isClickable = previouslyData.complete == 100;
                        }
                        return LanguagesListItem(
                          data,
                          widget.isActive,
                          CellType.sections,
                          widget.background,
                          parentState: this,
                          isClickable: isClickable,
                          mannerism: data.pages,
                        );
                      }, childCount: languagesData.sections.length))
                    ],
                  );
                })),
      );

  Widget getBackground() {
    if (widget.background != null) {
      return Image.network(
        widget.background,
        fit: BoxFit.cover,
      );
    } else {
      return Container(
        color: hexToColor("#2e1224"),
      );
    }
  }

  Future<LanguagesData> getData(bool isActive, int id) async {
    var response = await ServerManager.getActiveLanguageSections(id);
    if ((response as Response).statusCode >= 200 &&
        (response as Response).statusCode < 300) {
      var data = json.decode(utf8.decode(response.bodyBytes));
      LanguagesData value = LanguagesData.fromJson(data);
      print("value - $value");
      if (value != null) {
        return value;
      }
    } else {
      throw Exception('Failed to load sections');
    }
  }
}
