import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:lingafriq/iteractor/server_manager.dart';
import 'package:lingafriq/models/profile_data.dart';
import 'package:lingafriq/ui_elements/buttons.dart';
import 'package:lingafriq/ui_elements/loading_error.dart';
import 'package:lingafriq/ui_elements/loading_indicators.dart';
import 'package:lingafriq/ui_elements/questrial_regular_text.dart';
import 'package:lingafriq/utils/color.dart';
import 'package:lingafriq/utils/url_launcher.dart';

class Profile extends StatefulWidget {
  Profile({Key key}) : super(key: key);

  bool isLoading = false;

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  ProfileData profile;

  @override
  Widget build(BuildContext context) => Container(
        color: hexToColor("#2E1224"),
        child: Scaffold(
            extendBodyBehindAppBar: true,
            backgroundColor: Colors.white,
            body: FutureBuilder<ProfileData>(
                future: getData(),
                builder: (context, snapshot) {
                  if (snapshot.hasError) {
                    return LoadingError(ServerManager.defaultError, () {
                      setState(() {
                        widget.isLoading = true;
                      });
                    });
                  }
                  if (!snapshot.hasData) {
                    return Center(child: LoadingIndicator());
                  }
                  widget.isLoading = false;
                  profile = snapshot.data;
                  return CustomScrollView(
                    slivers: <Widget>[
                      SliverAppBar(
                        floating: false,
                        pinned: true,
                        expandedHeight: 180.0,
                        backgroundColor: hexToColor("#2e1224"),
                        flexibleSpace: LayoutBuilder(builder:
                            (BuildContext context, BoxConstraints constraints) {
                          return FlexibleSpaceBar(
                            centerTitle: true,
                            title:
                                QuestrialRegularText("Settings", 24, "#FFFFFF"),
                            background: Container(
                              color: hexToColor("#2e1224"),
                            ),
                          );
                        }),
                      ),
                      SliverFillRemaining(
                        hasScrollBody: false,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Flexible(
                              child: Padding(
                                padding: EdgeInsets.symmetric(horizontal: 24),
                                child: Column(
                                  children: [
                                    SizedBox(height: 40),
                                    Container(
                                      alignment: Alignment.centerLeft,
                                      child: QuestrialRegularText(
                                        "Email",
                                        13,
                                        "#a0a0a0",
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                    SizedBox(height: 4),
                                    Container(
                                      alignment: Alignment.centerLeft,
                                      child: QuestrialRegularText(
                                        profile.email,
                                        15,
                                        "#404040",
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                    SizedBox(height: 32),
                                    BorderBtn(
                                        "CHANGE PASSWORD", false, "#2e1224",
                                        () async {
                                      Navigator.pushNamed(
                                          context, "/changePass",
                                          arguments: "email");
                                    }),
                                    SizedBox(height: 32),
                                    BorderBtn("CONTACT US", false, "#2e1224",
                                        () {
                                      launchURL(
                                          context, "mailto", "hello@lingafriq.com");
                                    }),
                                    SizedBox(height: 32),
                                    Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        mainAxisSize: MainAxisSize.max,
                                        children: [
                                          Spacer(flex: 3),
                                          Container(
                                              height: 52,
                                              width: 52,
                                              child: InkWell(
                                                onTap: () {
                                                  launchURL(context, "url",
                                                      "https://lingafriq.com/");
                                                },
                                                child: Image.asset(
                                                    "assets/lingafriq.png"),
                                              )),
                                          Spacer(flex: 1),
                                          Container(
                                              height: 52,
                                              width: 52,
                                              child: InkWell(
                                                onTap: () {
                                                  launchURL(context, "url",
                                                      "https://twitter.com/LingAfriq");
                                                },
                                                child: Image.asset(
                                                    "assets/twitter.png"),
                                              )),
                                          Spacer(flex: 1),
                                          Container(
                                              height: 52,
                                              width: 52,
                                              child: InkWell(
                                                onTap: () {
                                                  launchURL(context, "url",
                                                      "https://www.instagram.com/lingafriq/");
                                                },
                                                child: Image.asset(
                                                    "assets/instagram.png"),
                                              )),
                                          Spacer(flex: 1),
                                          Container(
                                              height: 52,
                                              width: 52,
                                              child: InkWell(
                                                onTap: () {
                                                  launchURL(context, "url",
                                                      "https://www.linkedin.com/company/14800484");
                                                },
                                                child: Image.asset(
                                                    "assets/linkedin.png"),
                                              )),
                                          Spacer(flex: 1),
                                          Container(
                                              height: 52,
                                              width: 52,
                                              child: InkWell(
                                                onTap: () {
                                                  launchURL(context, "url",
                                                      "https://facebook.com/linqafriq");
                                                },
                                                child: Image.asset(
                                                    "assets/facebook.png"),
                                              )),
                                          Spacer(flex: 3),
                                        ]),
                                    Spacer(flex: 1),
                                    SizedBox(height: 50),
                                    BorderBtn("LOG OUT", false, "#db3f3f",
                                        () async {
                                      removeAuthToken();
                                      Navigator.pushNamedAndRemoveUntil(context,
                                          "/", (Route<dynamic> route) => false);
                                    }),
                                    SizedBox(height: 30)
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  );
                })),
      );

  Future<ProfileData> getData() async {
    final response = await ServerManager.getProfile();
    if ((response as Response).statusCode >= 200 &&
        (response as Response).statusCode < 300) {
      var data = json.decode(utf8.decode(response.bodyBytes));
      ProfileData value = ProfileData.fromJson(data);
      if (value != null) {
        return value;
      }
    } else {
      throw Exception('Failed to load polls');
    }
  }
}
