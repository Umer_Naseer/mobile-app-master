import 'dart:io';

import 'package:flutter/material.dart';
import 'package:lingafriq/iteractor/server_manager.dart';
import 'package:lingafriq/models/provider/auth.dart';
import 'package:lingafriq/screens/auth/auth_manager.dart';
import 'package:lingafriq/ui_elements/auth_text_field.dart';
import 'package:lingafriq/ui_elements/buttons.dart';
import 'package:lingafriq/ui_elements/questrial_regular_text.dart';
import 'package:lingafriq/utils/color.dart';
import 'package:provider/provider.dart';

class ChangePassword extends StatelessWidget {
  String email;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  ChangePassword(this.email);

  @override
  Widget build(BuildContext context) =>
      ChangeNotifierProvider<ChangePassProvider>(
        create: (context) => ChangePassProvider(),
        child: Scaffold(
          key: _scaffoldKey,
          extendBodyBehindAppBar: true,
          backgroundColor: Colors.white,
          body: CustomScrollView(slivers: <Widget>[
            SliverAppBar(
              floating: false,
              pinned: true,
              expandedHeight: 180.0,
              backgroundColor: hexToColor("#2e1224"),
              flexibleSpace: LayoutBuilder(
                  builder: (BuildContext context, BoxConstraints constraints) {
                return FlexibleSpaceBar(
                  centerTitle: true,
                  title: QuestrialRegularText("Change password", 24, "#FFFFFF"),
                  background: Container(
                    color: hexToColor("#2e1224"),
                  ),
                );
              }),
            ),
            SliverFillRemaining(
              hasScrollBody: false,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 24),
                child: Column(
                  children: [
                    Spacer(flex: 1),
                    Consumer<ChangePassProvider>(
                        builder: (_, provider, child) => AuthTextField(
                                "Current password",
                                provider.changePassControllers["old_password"],
                                onChangedFunc: () {
                              provider.checkController();
                              provider.deleteError('old_password');
                            },
                                isObscuredText: true,
                                errorMsg: provider.errors['old_password'],
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 8))),
                    SizedBox(height: 24),
                    Consumer<ChangePassProvider>(
                        builder: (_, provider, child) => AuthTextField(
                                "New password",
                                provider.changePassControllers["new_password"],
                                onChangedFunc: () {
                              provider.checkController();
                              provider.deleteError('new_password');
                            },
                                isObscuredText: true,
                                errorMsg: provider.errors['new_password'],
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 8))),
                    SizedBox(height: 24),
                    Consumer<ChangePassProvider>(
                        builder: (_, provider, child) => AuthTextField(
                                "Confirm password",
                                provider
                                    .changePassControllers["repeat_password"],
                                onChangedFunc: () {
                              provider.checkController();
                              provider.deleteError('repeat_password');
                            },
                                isObscuredText: true,
                                errorMsg: provider.errors['repeat_password'],
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 8))),
                    SizedBox(height: 64),
                    Consumer<ChangePassProvider>(
                        builder: (_, provider, child) => OrangeAuthBtn(
                              "CHANGE PASSWORD",
                              provider.buttonProcessing,
                              () async {
                                provider.setButtonProcessing(true);
                                await _continue(provider);
                              },
                            )),
                    Spacer(flex: 1)
                  ],
                ),
              ),
            )
          ]),
        ),
      );

  _continue(ChangePassProvider provider) async {
    try {
      await ServerManager.changePass(provider.changePassControllers)
          .then((response) {
        provider.setButtonProcessing(false);
        if (response['message'] != null) {
          removeAuthToken();
          Navigator.pushNamedAndRemoveUntil(_scaffoldKey.currentState.context,
              "/", (Route<dynamic> route) => false);
        } else {
          changeModelWithErrors(provider, response, _scaffoldKey);
        }
      });
    } on IOException catch (_) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("Check your internet connection and try again"),
        backgroundColor: Colors.red,
      ));
      provider.setButtonProcessing(false);
    }
  }
}
