import 'package:flutter/material.dart';
import 'package:lingafriq/ui_elements/buttons.dart';
import 'package:lingafriq/utils/color.dart';

class WelcomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Container(
        color: hexToColor("#2E1224"),
        child: Column(
          children: [
            Spacer(flex: 8),
            Stack(alignment: AlignmentDirectional.center, children: [
              Image(image: AssetImage("assets/africa.png")),
              Image(image: AssetImage("assets/logo.png"))
            ]),
            Spacer(flex: 5),
            WhiteBorderAuthBtn(
              "SIGN IN",
              false,
              () {
                Navigator.of(context).pushNamed("/signIn");
              },
              margin: EdgeInsets.only(top: 24, left: 16, right: 16),
            ),
            OrangeAuthBtn(
              "SIGN UP",
              false,
              () {
                Navigator.of(context).pushNamed("/signUp");
              },
              margin: EdgeInsets.only(top: 24, left: 16, right: 16),
            ),
            Spacer(flex: 4)
          ],
        ),
      );
}
