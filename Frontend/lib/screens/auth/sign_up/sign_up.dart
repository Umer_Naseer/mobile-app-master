import 'dart:io';

import 'package:flutter/material.dart';
import 'package:lingafriq/iteractor/server_manager.dart';
import 'package:lingafriq/models/provider/auth.dart';
import 'package:lingafriq/ui_elements/any_with_flex_in_scroll.dart';
import 'package:lingafriq/ui_elements/auth_header.dart';
import 'package:lingafriq/ui_elements/auth_text_field.dart';
import 'package:lingafriq/ui_elements/buttons.dart';
import 'package:lingafriq/ui_elements/questrial_regular_text.dart';
import 'package:provider/provider.dart';

import '../auth_manager.dart';

class SignUp extends StatelessWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) => ChangeNotifierProvider<AuthProvider>(
        create: (context) => AuthProvider(),
        child: Scaffold(
          key: _scaffoldKey,
          extendBodyBehindAppBar: true,
          backgroundColor: Colors.white,
          appBar: AppBar(
            elevation: 0,
            centerTitle: true,
            title: QuestrialRegularText("LingAfriq", 16, "#FFFFFF"),
            backgroundColor: Colors.transparent,
            iconTheme: IconThemeData(color: Colors.white),
          ),
          body: AnyInScroll(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                AuthHeader("Sign Up"),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24),
                  child: Column(
                    children: [
                      SizedBox(height: 64),
                      Consumer<AuthProvider>(
                          builder: (_, provider, child) => AuthTextField(
                                  "Email", provider.ucontrollers["email"],
                                  onChangedFunc: () {
                                authTextFieldChange(false, "email", provider);
                              },
                                  keyboardType: TextInputType.emailAddress,
                                  errorMsg: provider.errors['email'],
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 16, vertical: 8))),
                      SizedBox(height: 24),
                      Consumer<AuthProvider>(
                          builder: (_, provider, child) => AuthTextField(
                                  "Password", provider.ucontrollers["password"],
                                  onChangedFunc: () {
                                authTextFieldChange(
                                    false, "password", provider);
                              },
                                  errorMsg: provider.errors['password'],
                                  isObscuredText: true,
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 16, vertical: 8))),
                      SizedBox(height: 64),
                      Consumer<AuthProvider>(
                          builder: (_, provider, child) => OrangeAuthBtn(
                                  "CONTINUE", provider.buttonProcessing,
                                  () async {
                                provider.setButtonProcessing(true);
                                await signUp(provider, context);
                              })),
                      SizedBox(height: 64)
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      );

  signUp(AuthProvider provider, BuildContext context) async {
    try {
      await ServerManager.signUp(provider.ucontrollers).then((response) {
        provider.setButtonProcessing(false);
        if (response['isSuccess'] != null) {
          Navigator.pushNamed(context, "/signUpSecond",
              arguments: response['email']);
        } else {
          changeModelWithErrors(provider, response, _scaffoldKey);
        }
      });
    } on IOException catch (_) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("Check your internet connection and try again"),
        backgroundColor: Colors.red,
      ));
      provider.setButtonProcessing(false);
    }
  }
}
