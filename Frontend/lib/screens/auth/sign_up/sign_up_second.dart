import 'dart:io';

import 'package:flutter/material.dart';
import 'package:lingafriq/iteractor/server_manager.dart';
import 'package:lingafriq/models/provider/auth.dart';
import 'package:lingafriq/ui_elements/any_with_flex_in_scroll.dart';
import 'package:lingafriq/ui_elements/auth_header.dart';
import 'package:lingafriq/ui_elements/auth_text_field.dart';
import 'package:lingafriq/ui_elements/buttons.dart';
import 'package:lingafriq/ui_elements/questrial_regular_text.dart';
import 'package:provider/provider.dart';

import '../auth_manager.dart';

class SignUpSecond extends StatelessWidget {
  String email;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  SignUpSecond(this.email);

  @override
  Widget build(BuildContext context) => ChangeNotifierProvider<AuthProvider>(
        create: (context) => AuthProvider(),
        child: Scaffold(
          key: _scaffoldKey,
          extendBodyBehindAppBar: true,
          backgroundColor: Colors.white,
          appBar: AppBar(
            elevation: 0,
            centerTitle: true,
            title: QuestrialRegularText("LingAfriq", 16, "#FFFFFF"),
            backgroundColor: Colors.transparent,
            iconTheme: IconThemeData(color: Colors.white),
          ),
          body: AnyInScroll(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                AuthHeader("Sign Up"),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24),
                  child: Column(
                    children: [
                      SizedBox(height: 64),
                      QuestrialRegularText(
                          "Place the code we’ve sent you to email in the field below to complete the registration process.",
                          14,
                          "#404040",
                          textAlign: TextAlign.left),
                      SizedBox(height: 32),
                      Consumer<AuthProvider>(
                          builder: (_, provider, child) => TextFieldWithResend(
                                  "Code",
                                  provider.firstController,
                                  provider.resentBtnIsLoading,
                                  onChangedFunc: () {
                                //provider.checkController();
                                provider.deleteError('code');
                              }, resendFunc: () async {
                                provider.setResendBtnProcessing(true);
                                await _resendActivationCode(provider);
                              }, errorMsg: provider.errors['code'])),
                      SizedBox(height: 64),
                      Consumer<AuthProvider>(
                          builder: (_, provider, child) => OrangeAuthBtn(
                                  "SIGN UP", provider.buttonProcessing,
                                  () async {
                                provider.setButtonProcessing(true);
                                await _activate(provider);
                              })),
                      SizedBox(height: 64)
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      );

  _resendActivationCode(AuthProvider provider) async {
    try {
      await ServerManager.resendActivationCode(email).then((response) {
        provider.setButtonProcessing(false);
        provider.setResendBtnProcessing(false);
        if (response['message'] != null) {
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: QuestrialRegularText("Success", 14, "#FFFFFF",
                textAlign: TextAlign.left),
            backgroundColor: Colors.green,
          ));
        } else {
          changeModelWithErrors(provider, response, _scaffoldKey);
        }
      });
    } on IOException catch (_) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("Check your internet connection and try again"),
        backgroundColor: Colors.red,
      ));
      provider.setButtonProcessing(false);
    }
  }

  _activate(AuthProvider provider) async {
    try {
      await ServerManager.activateWithCode(provider.firstController.text, email)
          .then((response) {
        provider.setButtonProcessing(false);
        processAuth(response, provider, _scaffoldKey);
      });
    } on IOException catch (_) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("Check your internet connection and try again"),
        backgroundColor: Colors.red,
      ));
      provider.setButtonProcessing(false);
    }
  }
}
