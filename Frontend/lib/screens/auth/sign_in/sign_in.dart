import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lingafriq/iteractor/server_manager.dart';
import 'package:lingafriq/models/provider/auth.dart';
import 'package:lingafriq/ui_elements/any_with_flex_in_scroll.dart';
import 'package:lingafriq/ui_elements/auth_header.dart';
import 'package:lingafriq/ui_elements/auth_text_field.dart';
import 'package:lingafriq/ui_elements/buttons.dart';
import 'package:lingafriq/ui_elements/interactive_text.dart';
import 'package:lingafriq/ui_elements/questrial_regular_text.dart';
import 'package:provider/provider.dart';

import '../auth_manager.dart';

class SignIn extends StatelessWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) => ChangeNotifierProvider<AuthProvider>(
        create: (context) => AuthProvider(),
        child: Scaffold(
          key: _scaffoldKey,
          extendBodyBehindAppBar: true,
          backgroundColor: Colors.white,
          appBar: AppBar(
            elevation: 0,
            centerTitle: true,
            title: QuestrialRegularText("LingAfriq", 16, "#FFFFFF"),
            backgroundColor: Colors.transparent,
            iconTheme: IconThemeData(color: Colors.white),
          ),
          body: AnyInScroll(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                AuthHeader("Sign in"),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24),
                  child: Column(
                    children: [
                      SizedBox(height: 64),
                      Consumer<AuthProvider>(
                        builder: (_, provider, child) => AuthTextField(
                            "Email", provider.icontrollers["email"],
                            onChangedFunc: () {
                          authTextFieldChange(true, "email", provider);
                        },
                            errorMsg: provider.errors['email'],
                            keyboardType: TextInputType.emailAddress,
                            contentPadding: EdgeInsets.symmetric(
                                horizontal: 16, vertical: 8)),
                      ),
                      SizedBox(height: 24),
                      Consumer<AuthProvider>(
                          builder: (_, provider, child) => AuthTextField(
                                  "Password", provider.icontrollers["password"],
                                  onChangedFunc: () {
                                authTextFieldChange(true, "password", provider);
                              },
                                  errorMsg: provider.errors['password'],
                                  isObscuredText: true,
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 16, vertical: 8))),
                      SizedBox(height: 64),
                      Consumer<AuthProvider>(
                          builder: (_, provider, child) => OrangeAuthBtn(
                                  "SIGN IN", provider.buttonProcessing,
                                  () async {
                                provider.setButtonProcessing(true);
                                await signIn(provider);
                              })),
                      SizedBox(height: 64),
                      InteractiveText("Forgot your password?", () {
                        Navigator.of(context).pushNamed("/forgotPass");
                      }),
                      SizedBox(height: 64)
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      );

  signIn(AuthProvider provider) async {
    try {
      await ServerManager.signIn(provider.icontrollers).then((response) {
        provider.setButtonProcessing(false);
        processAuth(response, provider, _scaffoldKey);
      });
    } on IOException catch (_) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("Check your internet connection and try again"),
        backgroundColor: Colors.red,
      ));
      provider.setButtonProcessing(false);
    }
  }
}
