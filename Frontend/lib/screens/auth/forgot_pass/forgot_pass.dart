import 'dart:io';

import 'package:flutter/material.dart';
import 'package:lingafriq/iteractor/server_manager.dart';
import 'package:lingafriq/models/provider/auth.dart';
import 'package:lingafriq/ui_elements/any_with_flex_in_scroll.dart';
import 'package:lingafriq/ui_elements/auth_header.dart';
import 'package:lingafriq/ui_elements/auth_text_field.dart';
import 'package:lingafriq/ui_elements/buttons.dart';
import 'package:lingafriq/ui_elements/questrial_regular_text.dart';
import 'package:provider/provider.dart';

import '../auth_manager.dart';

class ForgotPass extends StatelessWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) => ChangeNotifierProvider<ForgotProvider>(
        create: (context) => ForgotProvider(),
        child: Scaffold(
          key: _scaffoldKey,
          extendBodyBehindAppBar: true,
          backgroundColor: Colors.white,
          appBar: AppBar(
            elevation: 0,
            centerTitle: true,
            title: QuestrialRegularText("LingAfriq", 16, "#FFFFFF"),
            backgroundColor: Colors.transparent,
            iconTheme: IconThemeData(color: Colors.white),
          ),
          body: AnyInScroll(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                AuthHeader("Forgot Password"),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24),
                  child: Column(
                    children: [
                      SizedBox(height: 64),
                      QuestrialRegularText(
                          "Place your email in the field below. We will send you an email with the code to recover your password.",
                          14,
                          "#404040",
                          textAlign: TextAlign.start),
                      SizedBox(height: 32),
                      Consumer<ForgotProvider>(
                          builder: (_, provider, child) =>
                              AuthTextField("Email", provider.firstController,
                                  onChangedFunc: () {
                                provider.checkController();
                                provider.deleteError('email');
                              },
                                  errorMsg: provider.errors['email'],
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 16, vertical: 8),
                                  keyboardType: TextInputType.emailAddress)),
                      SizedBox(height: 64),
                      Consumer<ForgotProvider>(
                          builder: (_, provider, child) => OrangeAuthBtn(
                                  "CONTINUE", provider.buttonProcessing,
                                  () async {
                                provider.setButtonProcessing(true);
                                await forgotPass(provider, context);
                              })),
                      SizedBox(height: 64)
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      );

  forgotPass(ForgotProvider provider, BuildContext context) async {
    try {
      await ServerManager.forgotPass(provider.firstController).then((response) {
        provider.setButtonProcessing(false);
        if (response['message'] != null) {
          Navigator.pushNamed(context, "/forgotPassCode",
              arguments: provider.firstController.text);
        } else {
          changeModelWithErrors(provider, response, _scaffoldKey);
        }
      });
    } on IOException catch (_) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("Check your internet connection and try again"),
        backgroundColor: Colors.red,
      ));
      provider.setButtonProcessing(false);
    }
  }
}
