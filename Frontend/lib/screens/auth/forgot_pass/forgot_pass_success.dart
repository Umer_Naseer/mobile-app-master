import 'package:flutter/material.dart';
import 'package:lingafriq/ui_elements/any_with_flex_in_scroll.dart';
import 'package:lingafriq/ui_elements/auth_header.dart';
import 'package:lingafriq/ui_elements/buttons.dart';
import 'package:lingafriq/ui_elements/questrial_regular_text.dart';
import 'package:lingafriq/utils/color.dart';

class ForgotSuccessScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Container(
        color: hexToColor("#2E1224"),
        child: Scaffold(
          extendBodyBehindAppBar: true,
          backgroundColor: Colors.white,
          appBar: AppBar(
            elevation: 0,
            automaticallyImplyLeading: false,
            centerTitle: true,
            title: QuestrialRegularText("LingAfriq", 16, "#FFFFFF"),
            backgroundColor: Colors.transparent,
            iconTheme: IconThemeData(color: Colors.white),
          ),
          body: AnyInScroll(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                AuthHeader("Forgot Password"),
                Flexible(
                  child: Container(
                      child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Column(
                      children: [
                        SizedBox(height: 64),
                        Image(image: AssetImage("assets/success_icon.png")),
                        SizedBox(height: 30),
                        QuestrialRegularText(
                            "Success! Now you can sign in.", 14, "#404040"),
                        SizedBox(height: 32),
                        OrangeAuthBtn("SIGN IN", false, () async {
                          Navigator.of(context).pushNamedAndRemoveUntil(
                              "/", ModalRoute.withName("home"));
                          Navigator.of(context).pushNamed("/signIn");
                        }),
                        SizedBox(height: 64)
                      ],
                    ),
                  )),
                )
              ],
            ),
          ),
        ),
      );
}
