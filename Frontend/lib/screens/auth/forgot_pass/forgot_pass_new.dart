import 'dart:io';

import 'package:flutter/material.dart';
import 'package:lingafriq/iteractor/server_manager.dart';
import 'package:lingafriq/models/provider/auth.dart';
import 'package:lingafriq/ui_elements/any_with_flex_in_scroll.dart';
import 'package:lingafriq/ui_elements/auth_header.dart';
import 'package:lingafriq/ui_elements/auth_text_field.dart';
import 'package:lingafriq/ui_elements/buttons.dart';
import 'package:lingafriq/ui_elements/questrial_regular_text.dart';
import 'package:provider/provider.dart';

import '../auth_manager.dart';

class ForgotNewPasswords extends StatelessWidget {
  Map<String, String> credentials;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  ForgotNewPasswords(this.credentials);

  @override
  Widget build(BuildContext context) => ChangeNotifierProvider<ForgotProvider>(
        create: (context) => ForgotProvider(),
        child: Scaffold(
          key: _scaffoldKey,
          extendBodyBehindAppBar: true,
          backgroundColor: Colors.white,
          appBar: AppBar(
            elevation: 0,
            automaticallyImplyLeading: false,
            centerTitle: true,
            title: QuestrialRegularText("LingAfriq", 16, "#FFFFFF"),
            backgroundColor: Colors.transparent,
            iconTheme: IconThemeData(color: Colors.white),
          ),
          body: AnyInScroll(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                AuthHeader("Forgot Password"),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24),
                  child: Column(
                    children: [
                      SizedBox(height: 40),
                      Consumer<ForgotProvider>(
                          builder: (_, provider, child) => AuthTextField(
                                  "New password",
                                  provider.newPassControllers["password"],
                                  onChangedFunc: () {
                                provider.checkController();
                                provider.deleteError('password');
                              },
                                  isObscuredText: true,
                                  errorMsg: provider.errors['password'],
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 16, vertical: 8))),
                      SizedBox(height: 24),
                      Consumer<ForgotProvider>(
                          builder: (_, provider, child) => AuthTextField(
                                  "Confirm password",
                                  provider
                                      .newPassControllers["repeat_password"],
                                  onChangedFunc: () {
                                provider.checkController();
                                provider.deleteError('repeat_password');
                              },
                                  isObscuredText: true,
                                  errorMsg: provider.errors['repeat_password'],
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 16, vertical: 8))),
                      SizedBox(height: 64),
                      Consumer<ForgotProvider>(
                          builder: (_, provider, child) => OrangeAuthBtn(
                                "RECOVER PASSWORD",
                                provider.buttonProcessing,
                                () async {
                                  provider.setButtonProcessing(true);
                                  await _continue(provider);
                                },
                              )),
                      SizedBox(height: 40)
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      );

  _continue(ForgotProvider provider) async {
    credentials["password"] =
        provider.newPassControllers["password"]?.text ?? "";
    credentials["repeat_password"] =
        provider.newPassControllers["repeat_password"]?.text ?? "";
    try {
      await ServerManager.forgotComplete(credentials).then((response) {
        provider.setButtonProcessing(false);
        if (response['message'] != null) {
          Navigator.of(_scaffoldKey.currentState.context)
              .pushNamedAndRemoveUntil(
                  "/forgotPassSuccess", ModalRoute.withName("home"));
        } else {
          changeModelWithErrors(provider, response, _scaffoldKey);
        }
      });
    } on IOException catch (_) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("Check your internet connection and try again"),
        backgroundColor: Colors.red,
      ));
      provider.setButtonProcessing(false);
    }
  }
}
