import 'dart:io';

import 'package:flutter/material.dart';
import 'package:lingafriq/iteractor/server_manager.dart';
import 'package:lingafriq/models/provider/auth.dart';
import 'package:lingafriq/ui_elements/any_with_flex_in_scroll.dart';
import 'package:lingafriq/ui_elements/auth_header.dart';
import 'package:lingafriq/ui_elements/auth_text_field.dart';
import 'package:lingafriq/ui_elements/buttons.dart';
import 'package:lingafriq/ui_elements/questrial_regular_text.dart';
import 'package:provider/provider.dart';

import '../auth_manager.dart';

class ForgotPassCode extends StatelessWidget {
  String email;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  ForgotPassCode(this.email);

  @override
  Widget build(BuildContext context) => ChangeNotifierProvider<ForgotProvider>(
        create: (context) => ForgotProvider(),
        child: Scaffold(
          key: _scaffoldKey,
          extendBodyBehindAppBar: true,
          backgroundColor: Colors.white,
          appBar: AppBar(
            elevation: 0,
            centerTitle: true,
            title: QuestrialRegularText("LingAfriq", 16, "#FFFFFF"),
            backgroundColor: Colors.transparent,
            iconTheme: IconThemeData(color: Colors.white),
          ),
          body: AnyInScroll(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                AuthHeader("Forgot Password"),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24),
                  child: Column(
                    children: [
                      SizedBox(height: 64),
                      QuestrialRegularText(
                        "Place the code we’ve sent you to email in the field below.",
                        14,
                        "#404040",
                        textAlign: TextAlign.left,
                      ),
                      SizedBox(height: 32),
                      Consumer<ForgotProvider>(
                          builder: (_, provider, child) => TextFieldWithResend(
                                  "Code",
                                  provider.firstController,
                                  provider.resentBtnIsLoading,
                                  onChangedFunc: () {
                                provider.checkController();
                                provider.deleteError('code');
                              }, resendFunc: () async {
                                provider.setResendBtnProcessing(true);
                                await _resendForgotCode(provider);
                              }, errorMsg: provider.errors['code'])),
                      SizedBox(height: 64),
                      Consumer<ForgotProvider>(
                          builder: (_, provider, child) => OrangeAuthBtn(
                                "CONTINUE",
                                provider.buttonProcessing,
                                () async {
                                  provider.setButtonProcessing(true);
                                  await _continue(provider);
                                },
                              )),
                      SizedBox(height: 64)
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      );

  _resendForgotCode(ForgotProvider provider) async {
    try {
      await ServerManager.forgotPass(TextEditingController(text: email))
          .then((response) {
        provider.setButtonProcessing(false);
        provider.setResendBtnProcessing(false);
        if (response['message'] != null) {
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: QuestrialRegularText("Success", 14, "#FFFFFF",
                textAlign: TextAlign.left),
            backgroundColor: Colors.green,
          ));
        } else {
          changeModelWithErrors(provider, response, _scaffoldKey);
        }
      });
    } on IOException catch (_) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("Check your internet connection and try again"),
        backgroundColor: Colors.red,
      ));
      provider.setButtonProcessing(false);
    }
  }

  _continue(ForgotProvider provider) async {
    try {
      print(email);
      print("CODE" + provider.firstController.text);
      await ServerManager.forgotVerifyCode(email, provider.firstController.text)
          .then((response) {
        provider.setButtonProcessing(false);
        provider.setResendBtnProcessing(false);
        if (response['message'] != null) {
          Navigator.of(_scaffoldKey.currentState.context)
              .pushNamedAndRemoveUntil(
                  "/forgotNewPasswords", ModalRoute.withName("home"),
                  arguments: {
                "code": provider.firstController.text,
                "email": email
              });
        } else {
          changeModelWithErrors(provider, response, _scaffoldKey);
        }
      });
    } on IOException catch (_) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("Check your internet connection and try again"),
        backgroundColor: Colors.red,
      ));
      provider.setButtonProcessing(false);
    }
  }
}
