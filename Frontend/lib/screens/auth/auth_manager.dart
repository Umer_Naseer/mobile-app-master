import 'package:flutter/material.dart';

processAuth(Map response, var screenModel, GlobalKey<ScaffoldState> key) {
  if (response['token'] != null) {
    Navigator.of(key.currentState.context).pushNamedAndRemoveUntil(
        "/tab_bar", (Route<dynamic> route) => false,
        arguments: response['token']);
  } else {
    changeModelWithErrors(screenModel, response, key);
  }
}

changeModelWithErrors(
    var screenModel, Map response, GlobalKey<ScaffoldState> key) {
  print("#### Server errors ####");
  print(response);
  screenModel.setErrors(response);
  screenModel.setButtonProcessing(false);
  if (response.containsKey("non_field_errors")) {
    key.currentState.showSnackBar(SnackBar(
        content: Text(response["non_field_errors"]),
        backgroundColor: Colors.red[600]));
  }
  if (response.containsKey("error")) {
    key.currentState.showSnackBar(SnackBar(
        content: Text(response["error"]), backgroundColor: Colors.red[600]));
  }
}

authTextFieldChange(bool isSignIn, String validationName, var provider) {
  provider.checkControllers(isSignIn);
  provider.deleteError(validationName);
}
