class ProfileData {
  String email;

  ProfileData(this.email);

  ProfileData.fromJson(Map<String, dynamic> json) {
    email = json['email'];
  }

}