import 'package:lingafriq/models/page.dart';

class LanguagesData {
  int id;
  String name;
  int complete;
  int lessonTypeId;
  List<LanguagesData> sections;
  List<LanguagesData> lessons;
  Map<String, dynamic> description;
  String background;
  bool inActive;
  List<PageItem> pages;

  bool get isComplete => complete == 100;

  LanguagesData({this.id, this.name, this.complete, this.sections, this.lessons, this.background, this.inActive, this.pages});

  LanguagesData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    lessonTypeId = json['lesson_type'];
    var completeVar = json['complete'];
    if (completeVar is bool) {
      complete = completeVar ? 100 : 0;
    } else if (completeVar is int) {
      complete = completeVar;
    }
    List<dynamic> arraySections = json['sections'];
    if (arraySections != null) {
      sections =
          arraySections.map((item) => LanguagesData.fromJson(item)).toList();
    }
    List<dynamic> arrayLessons = json['lessons'];
    if (arrayLessons != null) {
      lessons =
          arrayLessons.map((item) => LanguagesData.fromJson(item)).toList();
    }
    description = json['description'];
    background = json['background'];
    inActive = json['in_active'];
    List<dynamic> arrayPages = json['pages'];
    if (arrayPages != null) {
      pages =
          arrayPages.map((item) => PageItem.fromJson(item)).toList();
    }
  }
}
