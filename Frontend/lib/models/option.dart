import 'package:lingafriq/models/content.dart';

class Option {
  int id;
  String message;
  bool correctAnswer;
  List<ContentItem> optionResources;

  Option({this.id, this.message, this.correctAnswer, this.optionResources});

  Option.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    message = json['message'];
    correctAnswer = json['correct_answer'];
    if (json['option_resources'] != null) {
      optionResources = new List<ContentItem>();
      json['option_resources'].forEach((v) {
        optionResources.add(new ContentItem.fromJson(v));
      });
    }
  }
}