import 'package:flutter/cupertino.dart';
import 'package:lingafriq/models/content.dart';
import 'package:lingafriq/models/page.dart';
import 'package:lingafriq/models/quiz_item.dart';
import 'package:lingafriq/models/ws_element.dart';

class Lesson {
  int id;
  String name;
  int lessonType;
  String lessonTypeDisplay;
  List<QuizItem> messages;
  String infoMessage;
  List<ContentItem> lessonResources;
  bool complete;
  int next;
  Map<String, dynamic> funFact;
  String funFactText;
  String funFactImage;
  // It's for redirect to manerism after complete lesson
  List<PageItem> manerismPages = [];

  Lesson(
      {this.id,
        this.name,
        this.lessonType,
        this.lessonTypeDisplay,
        this.messages,
        this.infoMessage,
        this.lessonResources,
        this.complete});

  void changeQOptionsAvailability({@required bool isActive}) {
    if (messages.length < 1) { return; }
    messages.forEach((el){
      el.optionsIsActive = isActive;
    });
  }

  void clearVerified() {
    if (messages.length < 1) { return; }
    messages.forEach((el){
      el.isVerified = false;
    });
  }

  Lesson.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    if (json["next"] is int) {
      next = json['next'];
    } else {
      List<dynamic> arrayPages = json['next'];
      if (arrayPages != null) {
        manerismPages =
            arrayPages.map((item) => PageItem.fromJson(item)).toList();
      }
    }
    lessonType = json['lesson_type'];
    lessonTypeDisplay = json['lesson_type_display'];
    if (json['messages'] != null) {
      messages = new List<QuizItem>();
      json['messages'].forEach((v) {
        messages.add(new QuizItem.fromJson(v));
      });
    }
    infoMessage = json['info_message'];
    lessonResources = new List<ContentItem>();
    if (json['lesson_resources'] != null) {
      json['lesson_resources'].forEach((v) {
        lessonResources.add(new ContentItem.fromJson(v));
      });
      lessonResources.sort((a, b) => a.index.compareTo(b.index));
    }
    complete = json['complete'];
    funFact = json['fun_fact'];
    if (funFact != null) {
      funFactText = "";
      List<WSElement> resources = new List<WSElement>();
      json['fun_fact']['blocks'].forEach((v) {
        resources.add(WSElement.fromJson(v));
      });
      resources.forEach((element) {
                funFactText = funFactText + "\n" + element.text;
      });
    }
    funFactImage = json['fun_fact_image'];
  }
}