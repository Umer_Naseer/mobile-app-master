import 'package:flutter/material.dart';

class DefaultFormNotifier extends ChangeNotifier {
  bool buttonProcessing = false;
  bool resentBtnIsLoading = false;
  Map<String, String> errors = {};
  bool isButtonDisabled = true;

  void setButtonProcessing(bool procesing) {
    buttonProcessing = procesing;
    notifyListeners();
  }

  void setResendBtnProcessing(bool isLoading) {
    resentBtnIsLoading = isLoading;
    notifyListeners();
  }

  void setErrors(Map<String, String> errors) {
    this.errors.clear();
    this.errors.addAll(errors);
    notifyListeners();
  }

  void deleteError(String key) {
    if (this.errors.containsKey(key)) {
      this.errors.remove(key);
      notifyListeners();
    }
  }

  void deleteAllErrors() {
    if (errors.isNotEmpty) {
      this.errors.clear();
      notifyListeners();
    }
  }
}
