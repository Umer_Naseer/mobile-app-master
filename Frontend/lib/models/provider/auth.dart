import 'package:flutter/material.dart';
import 'package:lingafriq/models/provider/default.dart';

class AuthProvider extends DefaultFormNotifier {

  TextEditingController firstController = TextEditingController();

  Map<String, TextEditingController> ucontrollers = {
    //for sign up
    "email": TextEditingController(),
    "password": TextEditingController(),
  };

  Map<String, TextEditingController> icontrollers = {
    //for sign in
    "email": TextEditingController(),
    "password": TextEditingController(),
  };

  void checkControllers(bool isSignIn) {
    var controller = (isSignIn ? icontrollers : ucontrollers);
    controller.keys.forEach((key) {
      if (controller[key].text.isEmpty) {
        notifyListeners();
        isButtonDisabled = true;
        return;
      }
    });
    notifyListeners();
    isButtonDisabled = false;
  }
}

class ForgotProvider extends DefaultFormNotifier {
  bool resentBtnIsLoading = false;
  TextEditingController firstController = TextEditingController();

  Map<String, TextEditingController> newPassControllers = {
    "password": TextEditingController(),
    "repeat_password": TextEditingController(),
  };

  void checkController() {
    newPassControllers.keys.forEach((key) {
      if (newPassControllers[key].text.isEmpty) {
        notifyListeners();
        isButtonDisabled = true;
        return;
      }
    });
    notifyListeners();
    isButtonDisabled = firstController.text.isEmpty;
  }
}

class ChangePassProvider extends DefaultFormNotifier {
  bool resentBtnIsLoading = false;
  TextEditingController firstController = TextEditingController();

  Map<String, TextEditingController> changePassControllers = {
    "old_password" : TextEditingController(),
    "new_password": TextEditingController(),
    "repeat_password": TextEditingController(),
  };

  void checkController() {
    changePassControllers.keys.forEach((key) {
      if (changePassControllers[key].text.isEmpty) {
        notifyListeners();
        isButtonDisabled = true;
        return;
      }
    });
    notifyListeners();
    isButtonDisabled = firstController.text.isEmpty;
  }
}
