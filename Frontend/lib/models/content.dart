class ContentItem {
  int id;
  String typeContent;
  Object content;
  int index;

  ContentItem({this.id, this.typeContent, this.content, this.index});

  ContentItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    typeContent = json['type_content'];
    content = json['content'];
    index = json['index'];
  }
}