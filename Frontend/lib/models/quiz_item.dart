import 'content.dart';
import 'option.dart';

class QuizItem {
  int id;
  String message;
  String hint;
  int selectedOptionId = -1;
  int correctOptionId = -1;
  bool isChecking = false;
  bool optionsIsActive = true;
  bool isVerified = false;
  List<Option> options;
  List<ContentItem> questionResources;
  int pageNumber;
  bool congrats;

  QuizItem(
      {this.id,
        this.message,
        this.hint,
        this.options,
        this.questionResources,
        this.pageNumber,
        this.congrats});

  QuizItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    message = json['message'];
    hint = json['hint'];
    if (json['options'] != null) {
      options = new List<Option>();
      json['options'].forEach((v) {
        options.add(new Option.fromJson(v));
      });
    }
    questionResources = new List<ContentItem>();
    if (json['question_resources'] != null) {
      json['question_resources'].forEach((v) {
        questionResources.add(new ContentItem.fromJson(v));
      });
      questionResources.sort((a, b) => a.index.compareTo(b.index));
    }
    correctOptionId = options.firstWhere((el) => el.correctAnswer == true, orElse: () => null)?.id;
    pageNumber = json['page_number'];
    congrats = json['congrats'];

  }
}