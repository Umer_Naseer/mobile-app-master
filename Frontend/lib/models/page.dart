

class PageItem {
  int id;
  Map<String, dynamic> message;
  int pageNumber;
  bool congrats;
  String image;
  String audio;
  String video;

  PageItem(
      {this.id,
        this.message,
        this.pageNumber,
        this.congrats});

  PageItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    message = json['message'];
    pageNumber = json['page_number'];
    congrats = json['congrats'];
    image = json['image'];
    audio = json['audio'];
    video = json['video'];
  }
}