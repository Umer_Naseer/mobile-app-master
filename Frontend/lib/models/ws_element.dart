

extension RegExpExtension on RegExp {
  List<String> allMatchesWithSep(String input, [int start = 0]) {
    void addToResult(List<String> result, String item) {
      if (item != null && item.isNotEmpty) {
        result.add(item);
      }
    }

    var result = <String>[];
    for (var match in allMatches(input, start)) {
      addToResult(result, input.substring(start, match.start));
      addToResult(result, match[0]);
      start = match.end;
    }
    addToResult(result, input.substring(start));
    return result;
  }
}

extension StringExtension on String {
  List<String> splitWithDelim(RegExp pattern) =>
      pattern.allMatchesWithSep(this);
}

class WSElement {
  String text;
  bool isStatic;
  String hint;
  String rightAnswer;
  bool isChecked = false;

  WSElement({this.text, this.isStatic, this.hint, this.rightAnswer});

  bool isCorrect() {
    return text == rightAnswer;
  }

  WSElement.fromJson(Map<String, dynamic> json) {
    text = json['text'];
  }

  static List<WSElement> parseQuestion(String text) {
    List<WSElement> elements = List();
    List<String> res = text.splitWithDelim(RegExp(r"\[.*?\]"));
    res = res.where((element) => element.length > 0).toList();
    for (String item in res) {
      if (item.startsWith("[") && item.endsWith("]")) {
        String pairStr = item.replaceFirst("[", "").replaceFirst("]", "");
        List<String> pair = pairStr.split("/");
        elements.add(WSElement(text: null, isStatic: false, hint: pair[0], rightAnswer: pair[1]));
      } else {
        elements.add(WSElement(text: item, isStatic: true, hint: null, rightAnswer: null));
      }
    }
    return elements;
  }
}