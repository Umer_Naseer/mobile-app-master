import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';

class AudioControls extends StatefulWidget {
  final String url;

  AudioControls(this.url, {Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _AudioControlsState();
}

class _AudioControlsState extends State<AudioControls> {
  bool isPlaying = false;
  AudioPlayer audioPlayer;

  @override
  void initState() {
    super.initState();
    audioPlayer = AudioPlayer();
    audioPlayer.onPlayerStateChanged.listen((AudioPlayerState s) {
      if (mounted) {
        setState(() => isPlaying = s == AudioPlayerState.PLAYING);
      }
    });
  }

  @override
  void dispose() {
    if (audioPlayer != null) {
      audioPlayer.release();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            width: 32,
            height: 32,
            child: ButtonTheme(
              minWidth: 32,
              height: 32,
              child: FlatButton(
                padding: EdgeInsets.all(0.0),
                onPressed: playStopAudio,
                child: isPlaying
                    ? Image.asset('assets/stop.png')
                    : Image.asset('assets/play.png'),
              ),
            ),
          ),
          SizedBox(
            width: 12,
          ),
          isPlaying ? Text("Tap to stop audio") : Text("Tap to play audio"),
        ],
      );

  void playStopAudio() async {
    if (isPlaying) {
      await audioPlayer.stop();
    } else {
      await audioPlayer.play(widget.url);
    }
  }
}
