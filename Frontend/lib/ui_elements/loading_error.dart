import 'package:flutter/material.dart';
import 'package:lingafriq/ui_elements/buttons.dart';
import 'package:lingafriq/ui_elements/questrial_regular_text.dart';

class LoadingError extends StatelessWidget {
  final String errorMessage;
  final Function() retryListener;

  LoadingError(this.errorMessage, this.retryListener);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Card(
        margin: EdgeInsets.symmetric(horizontal: 24),
        child: Padding(
          padding: EdgeInsets.all(16),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              QuestrialRegularText(
                errorMessage,
                16,
                '#000000',
                textAlign: TextAlign.center,
              ),
              OrangeAuthBtn("RETRY", false, retryListener)
            ],
          ),
        ),
      ),
    );
    ;
  }
}
