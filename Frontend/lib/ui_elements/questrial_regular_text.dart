import 'package:flutter/cupertino.dart';
import 'package:lingafriq/utils/color.dart';

class DecoratedText extends StatelessWidget {
  final String name;
  final String text;
  final double size;
  final String hexColor;
  final TextAlign textAlign;
  final TextOverflow overflow;
  final int maxLines;

  DecoratedText(this.name, this.text, this.size, this.hexColor,
      {this.textAlign, this.maxLines, this.overflow = TextOverflow.ellipsis});

  @override
  Widget build(BuildContext context) => Text(
        text,
        textAlign: textAlign,
        overflow: overflow,
        maxLines: maxLines,
        style: TextStyle(
            color: hexToColor(hexColor), fontFamily: name, fontSize: size),
      );
}

class QuestrialRegularText extends DecoratedText {
  QuestrialRegularText(String text, double size, String hexColor,
      {TextAlign textAlign = TextAlign.center, int maxLines, TextOverflow overflow})
      : super("Questrial-Regular", text, size, hexColor,
      textAlign: textAlign, maxLines: maxLines, overflow:overflow);

  static getStyle(String hexColor, double size) => TextStyle(
      color: hexToColor(hexColor), fontFamily: "Questrial-Regular", fontSize: size);
}
