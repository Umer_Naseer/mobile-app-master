import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lingafriq/ui_elements/loading_indicators.dart';
import 'package:lingafriq/ui_elements/questrial_regular_text.dart';
import 'package:lingafriq/utils/color.dart';

class OrangeAuthBtn extends StatelessWidget {
  final Function onPressed;
  final String title;
  final bool isLoading;
  final bool isDisabled;
  final EdgeInsets margin;

  OrangeAuthBtn(this.title, this.isLoading, this.onPressed,
      {this.isDisabled = false,
      this.margin = const EdgeInsets.only(top: 0, left: 0, right: 0)});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 52,
      margin: margin,
      child: RaisedButton(
        highlightElevation: 0,
        disabledElevation: 0,
        elevation: 0,
        onPressed: isDisabled
            ? null
            : () async {
                await onPressed();
              },
        color: hexToColor("#FD8C42"),
        child: isLoading
            ? Center(child: buttonLoadingIndication())
            : Center(child: QuestrialRegularText(title, 16, "#141414")),
      ),
    );
  }
}

class WhiteBorderAuthBtn extends StatelessWidget {
  final Function onPressed;
  final String title;
  final bool isLoading;
  bool isDisabled = false;
  EdgeInsets margin;

  WhiteBorderAuthBtn(this.title, this.isLoading, this.onPressed,
      {this.margin = const EdgeInsets.only(top: 0, left: 0, right: 0)});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 52,
      margin: margin,
      child: RaisedButton(
        highlightElevation: 0,
        disabledElevation: 0,
        elevation: 0,
        onPressed: isDisabled
            ? null
            : () async {
                await onPressed();
              },
        color: Colors.transparent,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(0.0),
            side: BorderSide(color: Colors.white)),
        child: isLoading
            ? Center(child: buttonLoadingIndication())
            : Center(child: QuestrialRegularText(title, 16, "#FFFFFF")),
      ),
    );
  }
}

class BorderBtn extends StatelessWidget {
  final Function onPressed;
  final String title;
  final bool isLoading;
  bool isDisabled = false;
  EdgeInsets margin;
  String hexColor;

  BorderBtn(this.title, this.isLoading, this.hexColor, this.onPressed,
      {this.margin = const EdgeInsets.only(top: 0, left: 0, right: 0)});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 52,
      margin: margin,
      child: RaisedButton(
        highlightElevation: 0,
        disabledElevation: 0,
        elevation: 0,
        onPressed: isDisabled
            ? null
            : () async {
                await onPressed();
              },
        color: Colors.transparent,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(0.0),
            side: BorderSide(color: hexToColor(hexColor))),
        child: isLoading
            ? Center(child: buttonLoadingIndication())
            : Center(child: QuestrialRegularText(title, 16, hexColor)),
      ),
    );
  }
}
