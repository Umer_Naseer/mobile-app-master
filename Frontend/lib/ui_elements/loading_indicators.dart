import 'package:flutter/material.dart';
import 'package:lingafriq/utils/color.dart';

class LoadingIndicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Container(
        alignment: Alignment.center,
        height: 160.0,
        child: CircularProgressIndicator(
          valueColor: new AlwaysStoppedAnimation<Color>(hexToColor("#2e1224")),
        ),
      );
}

Container buttonLoadingIndication({String sHexColor = "#291020"}) {
  String colorHex = sHexColor;

  return Container(
    height: 24,
    width: 24,
    child: Center(
        child: CircularProgressIndicator(
      valueColor: AlwaysStoppedAnimation<Color>(hexToColor(colorHex)),
    )),
  );
}
