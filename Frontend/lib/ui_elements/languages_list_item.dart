import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:lingafriq/iteractor/server_errors.dart';
import 'package:lingafriq/iteractor/server_manager.dart';
import 'package:lingafriq/models/languages_data.dart';
import 'package:lingafriq/models/lesson.dart';
import 'package:lingafriq/models/page.dart';
import 'package:lingafriq/ui_elements/questrial_regular_text.dart';
import 'package:lingafriq/utils/color.dart';

class LanguagesListItem extends StatelessWidget {
  final LanguagesData languagesData;
  final bool isActive;
  final CellType type;
  GlobalKey scaffoldKey;
  String background;
  State parentState;
  bool isClickable;
  List<PageItem> mannerism;

  LanguagesListItem(
      this.languagesData, this.isActive, this.type, this.background,
      {this.parentState,
      this.isClickable = true,
      this.scaffoldKey,
      this.mannerism});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 24, vertical: 0),
      child: InkWell(
        onTap: () {
          switch (type) {
            case CellType.language:
              if (isActive) {
                Navigator.pushNamed(context, "/sections", arguments: {
                  'isActive': isActive,
                  'id': languagesData.id,
                  'background': background != null ? background : null
                }).then((value) {
                  if (parentState != null) {
                    parentState.setState(() {});
                  }
                });
              } else {
                Navigator.pushNamed(context, "/introduction", arguments: {
                  "data": languagesData,
                  "started": languagesData.inActive
                }).then((value) {
                  if (parentState != null) {
                    parentState.setState(() {});
                  }
                });
              }
              break;
            case CellType.sections:
              if (isClickable) {
                Navigator.pushNamed(context, "/lessons", arguments: {
                  'isActive': isActive,
                  'id': languagesData.id,
                  'background': background != null ? background : null,
                  'mannerism': mannerism != null ? mannerism : null
                }).then((value) {
                  if (parentState != null) {
                    parentState.setState(() {});
                  }
                });
              }
              break;
            case CellType.lessons:
              if (isClickable) {
                if (mannerism == null) {
                  switch (languagesData.lessonTypeId) {
                    case 0:
                      Navigator.pushNamed(context, "/lesson", arguments: {
                        "id": languagesData.id
                        //"background" : languagesData.background
                      }).then((value) {
                        if (parentState != null) {
                          parentState.setState(() {});
                        }
                      });
                      break;
                    case 1:
                      Navigator.pushNamed(context, "/instant_quiz",
                              arguments: {"id": languagesData.id, "q_index": 0})
                          .then((value) {
                        if (parentState != null) {
                          parentState.setState(() {});
                        }
                      });
                      break;
                    case 2:
                      Navigator.pushNamed(context, "/instant_quiz", arguments: {
                        "id": languagesData.id,
                        "q_index": 0,
                        "isOrdinary": true
                      }).then((value) {
                        if (parentState != null) {
                          parentState.setState(() {});
                        }
                      });
                      break;
                    case 3:
                      getData(languagesData.id);
                      break;
                    default:
                      Scaffold.of(context).showSnackBar(SnackBar(
                          content: Text(ErrorMsg.somethingWentWrong),
                          backgroundColor: Colors.red));
                      break;
                  }
                } else {
                  setManenerismFunc(context, 0);
                }
              }
              break;
          }
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(height: 24),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Expanded(
                    child: QuestrialRegularText(languagesData.name, 17,
                        isClickable ? "#404040" : "#d3d3d3",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.start)),
                SizedBox(width: 16),
                if (isActive)
                  Stack(
                    children: <Widget>[
                      if (languagesData.complete != null)
                        if (languagesData.complete < 100)
                          if (type != CellType.lessons)
                            QuestrialRegularText(
                                isClickable ? "${languagesData.complete}%" : "",
                                16,
                                languagesData.complete == 0
                                    ? "#2e1224"
                                    : "#25a588")
                          else
                            QuestrialRegularText("", 16, "#25a588")
                        else
                          Image(
                              image:
                                  AssetImage("assets/success_icon_green.png"))
                    ],
                  ),
                SizedBox(width: 16),
                if (isClickable) Image(image: AssetImage("assets/next.png"))
              ],
            ),
            SizedBox(height: 16),
            Divider(height: 1, color: hexToColor("#d3d3d3"))
          ],
        ),
      ),
    );
  }

  Future setManenerismFunc(BuildContext context, int index) {
    PageItem pageItem = mannerism[index];
    return Navigator.pushNamed(context, "/mannerism", arguments: {
      "screenTitle": pageItem.congrats ? "Congratulations" : "Mannerism",
      "shownData": pageItem,
      "btnFunc": pageItem.congrats
          ? (context) {
              Navigator.popUntil(context, ModalRoute.withName("Sections"));
            }
          : (context) {
              setManenerismFunc(context, index + 1);
            },
      "btnTitle": pageItem.congrats ? "Go to sections" : "NEXT"
    });
  }

  Future<Lesson> getData(int id) async {
    var response;
    response = await ServerManager.getSingleLesson(id);
    if ((response as Response).statusCode >= 200 &&
        (response as Response).statusCode < 300) {
      var data = json.decode(utf8.decode(response.bodyBytes));
      Lesson value = Lesson.fromJson(data);
      print("value - $value");
      if (value != null) {
        Navigator.pushNamed(scaffoldKey.currentContext, "/word_selection",
            arguments: {'lesson': value, 'q_index': 0}).then((value) {
          if (parentState != null) {
            parentState.setState(() {});
          }
        });
      }
    } else {
      Scaffold.of(scaffoldKey.currentContext).showSnackBar(SnackBar(
          content: Text(ErrorMsg.somethingWentWrong),
          backgroundColor: Colors.red));
    }
  }
}

enum CellType { language, sections, lessons }
