
import 'package:flutter/material.dart';
import 'package:lingafriq/models/content.dart';
import 'package:lingafriq/ui_elements/questrial_regular_text.dart';
import 'package:lingafriq/utils/color.dart';
import 'package:lingafriq/utils/draft_js_to_textspan_flutter/draft_js_to_textspan_flutter.dart';
import 'package:lingafriq/utils/utils.dart';

class QuizOptionListItem extends StatelessWidget {
  List<ContentItem> optionResources;
  bool isHighlighted;
  bool isCorrect;
  bool isChecking;
  bool isActive;
  Function func;

  QuizOptionListItem(this.optionResources, this.func,
      {this.isActive = true,
      this.isHighlighted = false,
      this.isChecking = false,
      this.isCorrect = false});

  @override
  Widget build(BuildContext context) {
    ContentItem text = optionResources
        .firstWhere((el) => el.typeContent == 'text', orElse: () {});
    ContentItem image = optionResources
        .firstWhere((el) => el.typeContent == 'image', orElse: () {});
    return Container(
      color: _getColor(),
      child: InkWell(
        onTap: () {
          if (!isActive) {
            return;
          }
          func();
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(height: 16),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  if (image != null && image.content is String) ...[
                    LimitedBox(
                        maxWidth: 100,
                        maxHeight: 62,
                        child: InkWell(
                            onTap: () {
                              showWidgetInDialog(
                                  context, Image.network(image.content));
                            },
                            child: Image.network(image.content))),
                    SizedBox(width: 16)
                  ],
                  if (text != null && text.content != null) ... [
                    Expanded(
                      flex: 1,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            DraftJSFlutter(text.content)
                          ]),
                    ),
                  ] else Spacer(flex: 1),
                  Container(width: 10),
                  Container(
                      height: 20,
                      width: 20,
                      alignment: Alignment.center,
                      child: _getImage())
                ],
              ),
              SizedBox(height: 16),
              Divider(height: 1, color: hexToColor("#d3d3d3"))
            ],
          ),
        ),
      ),
    );
  }

  Color _getColor() {
    if (isChecking) {
      return isCorrect ? hexToColor("#E5F2EF") : hexToColor("#F7E7E7");
    } else {
      return Colors.white;
    }
  }

  Image _getImage() {
    if (isChecking) {
      return Image.asset(isCorrect
          ? "assets/option_success.png"
          : "assets/option_incorrect.png");
    } else {
      return Image.asset(
          isHighlighted ? "assets/option_active.png" : "assets/option.png");
    }
  }
}
