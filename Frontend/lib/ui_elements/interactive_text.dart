import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:lingafriq/ui_elements/questrial_regular_text.dart';

class InteractiveText extends StatelessWidget {
  EdgeInsets margin;
  TextAlign textAlign;
  String text;
  String intractiveText;
  Function func;

  InteractiveText(this.intractiveText, this.func,
      {this.textAlign = TextAlign.center,
      this.text = "",
      this.margin = const EdgeInsets.only(left: 35, right: 35, top: 0)});

  @override
  Widget build(BuildContext context) => Container(
        margin: margin,
        child: RichText(
          textAlign: textAlign,
          text: TextSpan(
            children: [
              TextSpan(
                text: text,
                style: QuestrialRegularText.getStyle("#9C9C9C", 14),
              ),
              TextSpan(
                text: intractiveText,
                style: QuestrialRegularText.getStyle("#3E3E3E", 14),
                recognizer: TapGestureRecognizer()
                  ..onTap = () {
                    func();
                  },
              ),
            ],
          ),
        ),
      );
}
