import 'package:flutter/material.dart';

class AnyInScroll extends StatelessWidget {
  final Widget child;

  AnyInScroll({@required this.child});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return SingleChildScrollView(
          child: ConstrainedBox(
              constraints: BoxConstraints(minHeight: constraints.maxHeight),
              child: IntrinsicHeight(
                child: child,
              )));
    });
  }
}
