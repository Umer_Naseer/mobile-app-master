import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lingafriq/ui_elements/loading_indicators.dart';
import 'package:lingafriq/ui_elements/questrial_regular_text.dart';
import 'package:lingafriq/utils/color.dart';

class AuthTextField extends StatelessWidget {
  String placeholder;
  bool isObscuredText;
  String errorMsg;
  TextEditingController controller;
  EdgeInsets contentPadding;
  final TextInputType keyboardType;
  final Function onChangedFunc;

  AuthTextField(this.placeholder, this.controller,
      {this.onChangedFunc,
      this.contentPadding,
      this.errorMsg,
      this.isObscuredText = false,
      this.keyboardType = TextInputType.text});

  @override
  Widget build(BuildContext context) => Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextField(
              onChanged: (text) {
                onChangedFunc();
              },
              style: errorMsg == null
                  ? QuestrialRegularText.getStyle("#404040", 14)
                  : QuestrialRegularText.getStyle("#DB3F3F", 14),
              cursorColor:
                  errorMsg == null ? Colors.black : hexToColor("#DB3F3F"),
              obscureText: isObscuredText,
              keyboardType: keyboardType,
              textAlignVertical: TextAlignVertical.center,
              controller: controller,
              decoration: errorMsg == null
                  ? _standartDecoration(placeholder, contentPadding)
                  : _errorDecoration(placeholder, contentPadding),
            ),
            if (errorMsg != null) ...[
              SizedBox(height: 8),
              QuestrialRegularText(errorMsg, 12, "#DB3F3F",
                  textAlign: TextAlign.right)
            ]
          ],
        ),
      );
}

InputDecoration _standartDecoration(
        String placeholder, EdgeInsets contentPadding) =>
    InputDecoration(
      hintText: placeholder,
      hintStyle: QuestrialRegularText.getStyle("#9D9D9D", 14),
      contentPadding: contentPadding,
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(0)),
          borderSide: BorderSide(color: hexToColor("#D3D3D3"))),
      border: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(0)),
          borderSide: BorderSide(color: hexToColor("#D3D3D3"))),
      enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(0)),
          borderSide: BorderSide(color: hexToColor("#D3D3D3"))),
    );

InputDecoration _errorDecoration(
        String placeholder, EdgeInsets contentPadding) =>
    InputDecoration(
      hintText: placeholder,
      hintStyle: QuestrialRegularText.getStyle("#DB3F3F", 14),
      contentPadding: contentPadding,
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(0)),
          borderSide: BorderSide(color: hexToColor("#DB3F3F"))),
      border: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(0)),
          borderSide: BorderSide(color: hexToColor("#DB3F3F"))),
      enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(0)),
          borderSide: BorderSide(color: hexToColor("#DB3F3F"))),
    );

class TextFieldWithResend extends StatelessWidget {
  final String placeholder;
  String errorMsg;
  final bool isLoading;
  final TextEditingController controller;
  final Function onChangedFunc;
  final Function resendFunc;

  TextFieldWithResend(this.placeholder, this.controller, this.isLoading,
      {this.onChangedFunc, this.resendFunc, this.errorMsg});

  @override
  Widget build(BuildContext context) => Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            height: 44,
            decoration: BoxDecoration(
              border: Border.all(
                  color: errorMsg == null
                      ? hexToColor("#D3D3D3")
                      : hexToColor("#DB3F3F"),
                  width: 1,
                  style: BorderStyle.solid),
            ),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Expanded(
                    child: TextField(
                  maxLines: 1,
                  onChanged: (text) {
                    onChangedFunc();
                  },
                  cursorColor:
                      errorMsg == null ? Colors.black : hexToColor("#DB3F3F"),
                  style: errorMsg == null
                      ? QuestrialRegularText.getStyle("#404040", 14)
                      : QuestrialRegularText.getStyle("#DB3F3F", 14),
                  textAlignVertical: TextAlignVertical.bottom,
                  controller: controller,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(bottom: 20, left: 16),
                    hintText: placeholder,
                    hintStyle: QuestrialRegularText.getStyle(
                        errorMsg == null ? "#D3D3D3" : "#DB3F3F", 14),
                    focusedBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    border: InputBorder.none,
                  ),
                )),
                Container(
                  decoration: BoxDecoration(
                      border: Border(
                          left: BorderSide(
                              color: hexToColor("#D3D3D3"), width: 1))),
                  width: 92,
                  margin: EdgeInsets.symmetric(vertical: 5),
                  child: InkWell(
                    onTap: () {
                      if (resendFunc != null) {
                        resendFunc();
                      }
                    },
                    child: Center(
                        child: !isLoading
                            ? QuestrialRegularText("Send again", 12, "#000000")
                            : buttonLoadingIndication()),
                  ),
                )
              ],
            ),
          ),
          if (errorMsg != null) ...[
            SizedBox(height: 8),
            QuestrialRegularText(errorMsg, 12, "#DB3F3F",
                textAlign: TextAlign.right)
          ]
        ],
      );
}
