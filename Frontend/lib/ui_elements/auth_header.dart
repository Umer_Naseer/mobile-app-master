import 'package:flutter/cupertino.dart';
import 'package:lingafriq/ui_elements/questrial_regular_text.dart';

class AuthHeader extends StatelessWidget {
  String title;

  AuthHeader(this.title);

  @override
  Widget build(BuildContext context) => Stack(
        children: [
          Container(
              width: MediaQuery.of(context).size.width,
              child: Image(
                  image: AssetImage("assets/auth_header_bg.png"),
                  fit: BoxFit.fitWidth)),
          Positioned.fill(
            bottom: 43,
            child: Align(
              alignment: Alignment.bottomCenter,
              child: QuestrialRegularText(title, 32, "#FFFFFF"),
            ),
          )
        ],
      );
}
