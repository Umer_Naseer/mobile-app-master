import 'package:flutter/material.dart';

class PopupWidget extends StatelessWidget {

  final double width;
  final double height;
  final Widget body;

  PopupWidget(this.width, this.height, this.body);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: SizedBox(
        width: width,
        height: height,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: body,
        ),
      ),
    );
  }
}