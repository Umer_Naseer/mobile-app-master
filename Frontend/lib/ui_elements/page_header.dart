import 'package:flutter/cupertino.dart';
import 'package:lingafriq/ui_elements/questrial_regular_text.dart';
import 'package:lingafriq/utils/color.dart';

class PageHeader extends StatelessWidget {
  String title;

  PageHeader(this.title);

  @override
  Widget build(BuildContext context) => Stack(
        children: [
          Container(
            width: double.infinity,
            height: 200,
            color: hexToColor("#2e1224"),
          ),
          Positioned(
            bottom: 32,
            left: 24,
            child: QuestrialRegularText(title, 32, "#FFFFFF"),
          )
        ],
      );
}
