
import 'dart:convert';

import 'package:http/http.dart';

import 'server_manager.dart';

class ServerErrors {

  static const general = "detail";
  static Map<String, String> _errors = {};

  static Map<String, String> parse(Response response, bool isSending) {

    if (response.statusCode >= 400 && response.statusCode < 500) {
      return ServerErrors.parse4xx(response, true);
    } else if (response.statusCode > 500) {
      return {"non_field_errors": ServerManager.serverError};
    } else {
      return {"non_field_errors": ServerManager.defaultError};
    }
  }

  static Map<String, String> parse4xx(Response response, bool isSending) {
    var data = json.decode(response.body);
    _errors.clear();

    if (data is Map) {
      if (data.length == 1) {
        var errKeys = data.keys;
        var firstErrKey = errKeys.first;
        var errValue = data[firstErrKey];
        if (errValue is List) {
          var errorValue = errValue.first as String;
          _errors[firstErrKey] = errorValue;
        } else if (errValue is String) {
          _errors[firstErrKey] = errValue;
        } else if ((errValue as Map).isNotEmpty) {
          return parse(errValue, isSending);
        }
        return _errors;
      } else {
        for(var item in data.entries){
          if (item.value is List) {
            var value = (item.value as List).first;
            _errors[item.key] = value;
          } else if (item.value is String) {
            var value = item.value as String;
            _errors[item.key] = value;
          } else if ((item.value as Map).isNotEmpty) {
            parse(item.value, isSending);
          }
        }
        return _errors;
      }

//      if (data['detail'] != null) {
//        _errors['detail'] = data['detail'];
//        return _errors;
//      }
//      for (var key in (data as Map).keys) {
//        _errors[key] = [data[key] as List].first.first;
//      }

    } else {
      var response = isSending ? {general: ErrorMsg.sendingFailureEN} : {general:ErrorMsg.loadFailureEN};
      return response;
    }
  }

  static Map<String, String> parceErrDict(){

  }

}

class ErrorMsg {
  static var somethingWentWrong = "Something went wrong. Please try again later";
  static var sendingFailure = "Failed to send data. Please check your internet connection.";
  static var loadFailure = "Failed to load data. Please check your internet connection.";
  static var sendingFailureShortEN = "Failed to send data. Please check your internet connection.";
  static var sendingFailureEN = "Failed to send data. Please check your internet connection.";
  static var loadFailureEN = "Failed to load data. Please check your internet connection.";
  static var loadFailureShortEN = "Failed to load data. Please check your internet connection.";
  static var sendingFailureRU = "Не удалось отправить данные. Пожалуйста, проверьте подключение к интернету.";
  static var loadFailureRU = "Не удалось загрузить данные. Пожалуйста, проверьте подключение к интернету.";
  static var ageError = "You must agree terms & conditions";
  static var photoSizeError = "File size can't be greater then 5MB";
  static var enterRefCode = "Please, enter referral code";
  static var invalidPhoneNum = "Invalid phone number";
  static var incorrectPassword = "Incorrect password";
  static var noCurrencyInWallet = "Недостаточно средств в кошельке";
  static var notAllData = "Указаны не все данные";
  static var NoWalletsForTranslation = "Недостаточно средств в кошельке";
}