import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:http_interceptor/http_interceptor.dart';
import 'package:lingafriq/iteractor/server_errors.dart';
import 'package:lingafriq/main.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NotAuthorizedInterceptor implements InterceptorContract {
  @override
  Future<RequestData> interceptRequest({RequestData data}) async => data;

  @override
  Future<ResponseData> interceptResponse({ResponseData data}) async {
    if (data.statusCode == 401) {
      await removeAuthToken();
      InitialWidget.navKey.currentState
          .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
      InitialWidget.navKey.currentState.pushNamed("/signIn");
    }
    return data;
  }
}

class UrlPath {
  static const String rootUrl = "https://api.lingafriq.4-com.pro";

  //Auth
  static const String signUp = "/api/v0/auth/register/";
  static const String signUpActivateWithCode = "/api/v0/auth/activate/";
  static const String resendActivationCode = "/api/v0/auth/resend-activation-code/";
  static const String signIn = "/api/v0/auth/login/";
  static const String forgotFirstStep = "/api/v0/auth/reset-password-send-code/";
  static const String forgotVerifyCode = "/api/v0/auth/reset-password-verify-code/";
  static const String forgotComplete = "/api/v0/auth/reset-password-by-code/";

  //Languages
  static const String language = "/api/v0/mobile/languages/";
  static const String activateLanguage = "/api/v0/mobile/active/language/";

  //Active
  static const String activeLanguages = "/api/v0/mobile/active/languages/";
  static const String activeLanguageSection = "/api/v0/mobile/active/language/";
  static const String activeSectionLessons = "/api/v0/mobile/active/section/";

  //Lesson
  static const String singleLesson = "/api/v0/mobile/lesson/";

  //Profile
  static const String activeLesson = "/api/v0/mobile/active/lesson/";
  static const String profile = "/api/v0/mobile/profile/email/";
  static const String changePass = "/api/v0/mobile/profile/change-password/";

}

String _createURLWithComponents(String path, {String params = ""}) {
  String url;
  url = "${UrlPath.rootUrl}${path}$params";

  return url;
}

Future<String> getAuthToken() async {
  final prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');
  print(token);
  return token;
}

Future<bool> removeAuthToken() async {
  final prefs = await SharedPreferences.getInstance();
  final bool value = await prefs.remove("token");
  return value;
}

Future _writeAuthToken(String token) async {
  print(token);
  final prefs = await SharedPreferences.getInstance();
  prefs.setString('token', token);
}

class ServerManager {
  static const String defaultLoadError =
      "Failed to load data. It may be internet connection problem or server error.";
  static const String defaultNetworkError =
      "Please check your internet connection";
  static const String defaultError = "Something went wrong";
  static const String serverError = "Server error. Please try again later";

  static http.Client client = HttpClientWithInterceptor.build(interceptors: [
    NotAuthorizedInterceptor(),
  ]);

  static Future<Map> signIn(Map controllers) async {
    Map<String, String> body = {
      "password": controllers["password"].text,
      "email": controllers["email"].text,
    };

    var url = _createURLWithComponents(UrlPath.signIn);

    var bodyEncoded = json.encode(body);
    var response = await client.post(
      url,
      body: bodyEncoded,
      headers: {"Content-Type": "application/json"},
    );

    return _authResponse(response);
  }

  static Future<Map> signUp(Map controllers) async {
    Map<String, String> body = {
      "password": controllers["password"].text,
      "email": controllers["email"].text,
    };

    var url = _createURLWithComponents(UrlPath.signUp);

    var bodyEncoded = json.encode(body);
    var response = await client.post(
      url,
      body: bodyEncoded,
      headers: {"Content-Type": "application/json"},
    );

    var data;

    if (response.statusCode >= 200 && response.statusCode <= 299) {
      data = json.decode(response.body);

      if (data["email"] != null) {
        return {"isSuccess": true, "email": data["email"]};
      }
    } else if (response.statusCode >= 400 && response.statusCode < 500) {
      return ServerErrors.parse(response, true);
    } else {
      throw Exception();
    }
  }

  static Future<Map> activateWithCode(String code, String email) async {
    Map<String, String> body = {
      "code": code,
      "email": email,
    };

    var url = _createURLWithComponents(UrlPath.signUpActivateWithCode);

    var bodyEncoded = json.encode(body);
    var response = await client.post(
      url,
      body: bodyEncoded,
      headers: {"Content-Type": "application/json"},
    );

    return _authResponse(response);
  }

  static Future resendActivationCode(String email) async {
    Map<String, String> body = {"email": email};

    var url = _createURLWithComponents(UrlPath.resendActivationCode);

    var bodyEncoded = json.encode(body);
    var response = await client.post(
      url,
      body: bodyEncoded,
      headers: {"Content-Type": "application/json"},
    );

    var data;

    if (response.statusCode >= 200 && response.statusCode <= 299) {
      data = json.decode(response.body);
      if (data["message"] != null) {
        return {"message": data["message"]};
      }
    } else if (response.statusCode >= 400 && response.statusCode < 500) {
      return ServerErrors.parse(response, true);
    } else {
      throw Exception();
    }
  }

  static Future forgotPass(TextEditingController controller) async {
    Map<String, String> body = {"email": controller.text};

    var url = _createURLWithComponents(UrlPath.forgotFirstStep);

    var bodyEncoded = json.encode(body);
    var response = await client.post(
      url,
      body: bodyEncoded,
      headers: {"Content-Type": "application/json"},
    );

    var data;

    if (response.statusCode >= 200 && response.statusCode <= 299) {
      data = json.decode(response.body);
      if (data["message"] != null) {
        return {"message": data["message"]};
      }
    } else if (response.statusCode >= 400 && response.statusCode < 500) {
      return ServerErrors.parse(response, true);
    } else {
      throw Exception();
    }
  }

  static Future forgotVerifyCode(String email, String code) async {
    Map<String, String> body = {"code": code, "email": email};

    var url = _createURLWithComponents(UrlPath.forgotVerifyCode);

    var bodyEncoded = json.encode(body);
    var response = await client.post(
      url,
      body: bodyEncoded,
      headers: {"Content-Type": "application/json"},
    );

    var data;

    if (response.statusCode >= 200 && response.statusCode <= 299) {
      data = json.decode(response.body);
      if (data["message"] != null) {
        return {"message": data["message"]};
      }
    } else if (response.statusCode >= 400 && response.statusCode < 500) {
      return ServerErrors.parse(response, true);
    } else {
      throw Exception();
    }
  }

  static Future defaultAuthGet(String url) async {
    String token = await getAuthToken();
    print(token);
    final response = await client.get(
      url,
      headers: {'Authorization': 'token $token'},
    );
    return response;
  }

  static Future forgotComplete(Map<String, String> params) async {

    var url = _createURLWithComponents(UrlPath.forgotComplete);

    var bodyEncoded = json.encode(params);
    var response = await client.post(
      url,
      body: bodyEncoded,
      headers: {"Content-Type": "application/json"},
    );

    var data;

    if (response.statusCode >= 200 && response.statusCode <= 299) {
      data = json.decode(response.body);
      if (data["message"] != null) {
        return {"message": data["message"]};
      }
    } else if (response.statusCode >= 400 && response.statusCode < 500) {
      return ServerErrors.parse(response, true);
    } else {
      throw Exception();
    }
  }

  static Future getLanguage() async {
    String url = _createURLWithComponents(UrlPath.language);
    return defaultAuthGet(url);
  }

  static Future<Map> postActivateLanguage(int languageId) async {

    var url = _createURLWithComponents("${UrlPath.activateLanguage}/${languageId}/");
    String token = await getAuthToken();
    var response = await client.post(
        url,
        headers: {"Content-Type": "application/json", 'Authorization': 'token $token'}
    );

    var data;

    if (response.statusCode >= 200 && response.statusCode <= 299) {
      data = json.decode(response.body);
      data["success"] = true;
      return data;
    } else if (response.statusCode >= 400 && response.statusCode < 500) {
      return ServerErrors.parse(response, true);
    } else {
      throw Exception();
    }
  }

  static Future<Map> postCompleteLesson(int lessonId) async {

    var url = _createURLWithComponents("${UrlPath.activeLesson}/$lessonId/");
    String token = await getAuthToken();
    var response = await client.post(
        url,
        headers: {"Content-Type": "application/json", 'Authorization': 'token $token'}
    );

    var data;

    if (response.statusCode >= 200 && response.statusCode <= 299) {
      data = json.decode(response.body);
      data["success"] = true;
      return data;
    } else if (response.statusCode >= 400 && response.statusCode < 500) {
      return ServerErrors.parse(response, true);
    } else {
      throw Exception();
    }
  }

  static Future getActiveLanguages() async {
    String url = _createURLWithComponents(UrlPath.activeLanguages);
    return defaultAuthGet(url);
  }

  static Future getActiveLanguageSections(int id) async {
    String url = _createURLWithComponents(UrlPath.activeLanguageSection);
    url += "$id/sections/";
    return defaultAuthGet(url);
  }

  static Future getActiveSectionsLessons(int id) async {
    String url = _createURLWithComponents(UrlPath.activeSectionLessons);
    url += "$id/lessons/";
    return defaultAuthGet(url);
  }

  static Future getSingleLesson(int id) async {
    String url = _createURLWithComponents("${UrlPath.singleLesson}$id/");
    return defaultAuthGet(url);
  }

  static Future getLesson(int id) async {
    String url = _createURLWithComponents("${UrlPath.activeLesson}$id/");
    return defaultAuthGet(url);
  }

  static Future getProfile() async {
    String url = _createURLWithComponents(UrlPath.profile);
    return defaultAuthGet(url);
  }

  static Future changePass(Map controllers) async {
    Map<String, String> body = {
      "old_password": controllers["old_password"].text,
      "new_password": controllers["new_password"].text,
      "repeat_password": controllers["repeat_password"].text,
    };
    String token = await getAuthToken();
    var url = _createURLWithComponents(UrlPath.changePass);

    var bodyEncoded = json.encode(body);
    var response = await client.put(
      url,
      body: bodyEncoded,
      headers: {"Content-Type": "application/json", 'Authorization': 'token $token'},
    );

    var data;

    if (response.statusCode >= 200 && response.statusCode <= 299) {
      data = json.decode(response.body);
      if (data["message"] != null) {
        return {"message": data["message"]};
      }
    } else if (response.statusCode >= 400 && response.statusCode < 500) {
      return ServerErrors.parse(response, true);
    } else {
      throw Exception();
    }
  }

}

_authResponse(dynamic response) {
  var data;

  if (response.statusCode == 200) {
    data = json.decode(response.body);
    if (data['token'] != null || data["key"] != null) {
      String value = data['token'] != null ? data['token'] : data["key"];
      _writeAuthToken(value);
      return {"token": value};
    }
  } else {
    return ServerErrors.parse(response, true);
  }
}
