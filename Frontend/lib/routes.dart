import 'package:flutter/material.dart';
import 'package:lingafriq/models/languages_data.dart';
import 'package:lingafriq/models/page.dart';
import 'package:lingafriq/screens/auth/forgot_pass/forgot_pass.dart';
import 'package:lingafriq/screens/auth/forgot_pass/forgot_pass_code.dart';
import 'package:lingafriq/screens/auth/forgot_pass/forgot_pass_new.dart';
import 'package:lingafriq/screens/auth/forgot_pass/forgot_pass_success.dart';
import 'package:lingafriq/screens/auth/sign_in/sign_in.dart';
import 'package:lingafriq/screens/auth/sign_up/sign_up.dart';
import 'package:lingafriq/screens/auth/sign_up/sign_up_second.dart';
import 'package:lingafriq/screens/learning/languages/introduction.dart';
import 'package:lingafriq/screens/learning/lesson_widget.dart';
import 'package:lingafriq/screens/learning/lessons.dart';
import 'package:lingafriq/screens/learning/sections.dart';
import 'package:lingafriq/screens/profile/change_pass.dart';
import 'package:lingafriq/screens/quiz/instant_quiz.dart';
import 'package:lingafriq/screens/quiz/mannerism.dart';
import 'package:lingafriq/screens/quiz/ordinary_quiz.dart';
import 'package:lingafriq/screens/quiz/ordinary_quiz_result.dart';
import 'package:lingafriq/screens/quiz/single_wysiwyg_screen.dart';
import 'package:lingafriq/screens/quiz/word_selection.dart';
import 'package:lingafriq/screens/tab_bar_screen/tab_bar_screen.dart';
import 'package:lingafriq/screens/welcome_screen/welcome_screen.dart';

import 'iteractor/server_manager.dart';
import 'models/lesson.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;

    switch (settings.name) {
      case "/":
        return MaterialPageRoute(
          settings: RouteSettings(name: "home"),
          builder: (_) => FutureBuilder(
            future: getAuthToken(),
            builder: (BuildContext context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                bool isExist = snapshot.data != null && snapshot.data != "";
                return isExist ? TabBarScreen(snapshot.data) : WelcomeScreen();
              } else {
                return Scaffold();
              }
            },
          ),
        );
      case "/signIn":
        return MaterialPageRoute(builder: (context) => SignIn());
      case "/signUp":
        return MaterialPageRoute(builder: (context) => SignUp());
      case "/signUpSecond":
        if (args is String) {
          return MaterialPageRoute(builder: (context) => SignUpSecond(args));
        }
        return _errorRoute("signSecond");
      case "/forgotPass":
        return MaterialPageRoute(builder: (context) => ForgotPass());
      case "/forgotPassCode":
        if (args is String) {
          return MaterialPageRoute(builder: (context) => ForgotPassCode(args));
        }
        return _errorRoute("forgotPassCode");
      case "/forgotNewPasswords":
        if (args is Map) {
          return MaterialPageRoute(
              builder: (context) => ForgotNewPasswords(args));
        }
        return _errorRoute("forgotNewPasswords");
      case "/forgotPassSuccess":
        return MaterialPageRoute(builder: (context) => ForgotSuccessScreen());
      case "/tab_bar":
        if (args is String) {
          print("tab_bar - $args");
          return MaterialPageRoute(builder: (context) => TabBarScreen(args));
        }
        return _errorRoute("tab_bar");
      case "/sections":
        if (args is Map) {
          bool isActive = args["isActive"];
          int id = args["id"];
          String background = args["background"];
          bool isNeedToShownLastSection = false;
          if (args["isNeedToShownLastSection"] != null) {
            isNeedToShownLastSection = args["isNeedToShownLastSection"];
          }
          return MaterialPageRoute(
              settings: RouteSettings(name: "Sections"),
              builder: (context) => SectionList(isActive, id, background, isNeedToShownLastSection: isNeedToShownLastSection));
        }
        return _errorRoute("sections");
      case "/lessons":
        if (args is Map) {
          bool isActive = args["isActive"];
          int id = args["id"];
          String background = args["background"];
          List<PageItem> mannerism = args["mannerism"];
          return MaterialPageRoute(
              settings: RouteSettings(name: "Lessons"),
              builder: (context) => LessonsList(
                    isActive,
                    id,
                    background,
                    mannerism: mannerism,
                  ));
        }
        return _errorRoute("lessons");
      case "/lesson":
        if (args is Map) {
          int id = args["id"];
          String background = args["background"];
          return MaterialPageRoute(
              builder: (context) => LessonWidget(id, background));
        }
        return _errorRoute("lessons");
      case "/word_selection":
        if (args is Map) {
          Lesson lesson = args["lesson"];
          int questionIndex = args["q_index"];
          String background = args["background"];
          return MaterialPageRoute(
              builder: (context) =>
                  WordSelectionWidget(lesson, questionIndex, background));
        }
        return _errorRoute("lessons");
      case "/instant_quiz":
        if (args is Map) {
          int id = args["id"];
          int quizIndex = args["q_index"];
          String background = args["background"];
          bool isOrdinary = args["isOrdinary"];
          return MaterialPageRoute(
              builder: (context) => InstantQuiz(id, background,
                  quizIndex: quizIndex,
                  isOrdinaryQuiz: isOrdinary != null ? isOrdinary : false));
        }
        return _errorRoute("iQuiz");
      case "/ordinary_quiz":
        if (args is Map) {
          int quizIndex = args["q_index"];
          Lesson lesson = args["lesson"];
          return MaterialPageRoute(
              builder: (context) => OrdinaryQuiz(lesson, quizIndex));
        }
        return _errorRoute("oQuiz");
      case "/ordinary_quiz_reslut":
        if (args is Map) {
          Lesson lesson = args["lesson"];
          return MaterialPageRoute(
              builder: (context) => OrdinaryQuizResult(lesson));
        }
        return _errorRoute("oQuiz");
      case "/introduction":
        if (args is Map) {
          LanguagesData langData = args["data"];
          bool hasStarted = args["started"];
          return MaterialPageRoute(
              builder: (context) => Introduction(langData, hasStarted));
        }
        return _errorRoute("introduction");
      case "/wysiwyg_single":
        if (args is Map) {
          String screenTitle = args["screenTitle"];
          Object shownData = args["shownData"];
          Function btnFunc = args["btnFunc"];
          String backgroundUrl = args["backgroundUrl"];
          String btnTitle = args["btnTitle"];
          return MaterialPageRoute(
              builder: (context) => SingleWysiwyg(
                    screenTitle,
                    shownData,
                    btnFunc,
                    backgroundUrl: backgroundUrl,
                    btnTitle: btnTitle,
                  ));
        }
        return _errorRoute("wysiwyg");
      case "/mannerism":
        if (args is Map) {
          String screenTitle = args["screenTitle"];
          Object shownData = args["shownData"];
          Function btnFunc = args["btnFunc"];
          String backgroundUrl = args["backgroundUrl"];
          String btnTitle = args["btnTitle"];
          return MaterialPageRoute(
              builder: (context) => MannerismScreen(
                screenTitle,
                shownData,
                btnFunc,
                backgroundUrl: backgroundUrl,
                btnTitle: btnTitle,
              ));
        }
        return _errorRoute("introduction");
      case "/changePass":
        if (args is String) {
          return MaterialPageRoute(builder: (context) => ChangePassword(args));
        }
        return _errorRoute("changePass");
      default:
        return _errorRoute("default");
    }
  }

  static Route<dynamic> _errorRoute(String screen) => MaterialPageRoute(
      builder: (_) => Scaffold(
            body: Center(
              child: Text(screen + "ERROR",
                  style: TextStyle(fontSize: 24, color: Colors.red)),
            ),
          ));
}
