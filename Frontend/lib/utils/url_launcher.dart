

import 'package:flutter/material.dart';
import 'package:lingafriq/iteractor/server_manager.dart';
import 'package:url_launcher/url_launcher.dart';

Future<void> launchURL(BuildContext context, String scheme, String path) async {
  final Uri params = Uri(
    scheme: scheme,
    path: path,
  );
  String url = scheme == "url" ? path : params.toString();
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(scheme == "url"
            ? ServerManager.defaultError
            : "Phone services is not available"),
        backgroundColor: Colors.red[600]));
  }
}

//Using launchURL(context, "mailto", "example@gmail.com");