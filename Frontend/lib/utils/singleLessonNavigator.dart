import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:lingafriq/iteractor/server_errors.dart';
import 'package:lingafriq/iteractor/server_manager.dart';
import 'package:lingafriq/models/lesson.dart';
import 'package:lingafriq/models/page.dart';

class SingleLessonNavigator {
  static Future<Lesson> loadNextLesson(int id, BuildContext context) async {
    var response;
    response = await ServerManager.getSingleLesson(id);
    if ((response as Response).statusCode >= 200 &&
        (response as Response).statusCode < 300) {
      var data = json.decode(utf8.decode(response.bodyBytes));
      Lesson value = Lesson.fromJson(data);
      if (value != null) {
        switch (value.lessonType) {
          case 0:
            Navigator.pushNamedAndRemoveUntil(context,
                "/lesson", ModalRoute.withName("Lessons"),
                arguments: {
                  "id": value.id
                  //"background" : languagesData.background
                });
            break;
          case 1:
            Navigator.pushNamedAndRemoveUntil(context,
                "/instant_quiz", ModalRoute.withName("Lessons"),
                arguments: {"id": value.id, "q_index": 0});
            break;
          case 2:
            Navigator.pushNamedAndRemoveUntil(context,
                "/instant_quiz", ModalRoute.withName("Lessons"),
                arguments: {"id": value.id, "q_index": 0, "isOrdinary": true});
            break;
          case 3:
            Navigator.pushNamedAndRemoveUntil(context,
                "/word_selection", ModalRoute.withName("Lessons"),
                arguments: {'lesson': value, 'q_index': 0});

            break;
          default:
            Scaffold.of(context).showSnackBar(SnackBar(
                content: Text(ErrorMsg.somethingWentWrong),
                backgroundColor: Colors.red));
            break;
        }
      }
    } else {
      Scaffold.of(context).showSnackBar(SnackBar(
          content: Text(ErrorMsg.somethingWentWrong),
          backgroundColor: Colors.red));
    }
  }

  static Future showManerism(BuildContext context, int index, List<PageItem> mannerism) {
    PageItem pageItem = mannerism[index];
    return Navigator.pushReplacementNamed(context, "/mannerism", arguments: {
      "screenTitle": pageItem.congrats ? "Congratulations" : "Mannerism",
      "shownData": pageItem,
      "btnFunc": pageItem.congrats
          ? (context) {
        Navigator.popUntil(context, ModalRoute.withName("Sections"));
      }
          : (context) {
        SingleLessonNavigator.showManerism(context, index + 1, mannerism);
      },
      "btnTitle": pageItem.congrats ? "Go to sections" : "NEXT"
    });
  }
}
