import 'package:flutter/material.dart';

Color hexToColor(String code) {
  if (code == null) {
    return null;
  }
  return Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
}
