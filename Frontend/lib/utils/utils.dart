import 'package:flutter/material.dart';
import 'package:lingafriq/ui_elements/questrial_regular_text.dart';

import 'color.dart';

void showWidgetInDialog(BuildContext context, Widget child) {
  double width = MediaQuery.of(context).size.width * 0.9;
  showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          backgroundColor: Colors.transparent,
          elevation: 16,
          child: Container(
            color: Colors.transparent,
            width: width,
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Positioned(
                  right: 0,
                  top: -30,
                  child: Container(
                    color: Colors.transparent,
                    height: 16,
                    width: 16,
                    child: InkWell(
                      onTap: Navigator.of(context).pop,
                      child: Image.asset('assets/close.png'),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(0),
                  child: child,
                ),
              ],
            ),
          ),
        );
      });
}

void showHint(BuildContext context, String message) {
  double width = MediaQuery.of(context).size.width * 0.8;
  showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          elevation: 16,
          child: Container(
            width: width,
            child: Stack(
              children: [
                Positioned(
                  right: 10,
                  top: 10,
                  child: Container(
                    height: 16,
                    width: 16,
                    child: InkWell(
                      onTap: Navigator.of(context).pop,
                      child: Image.asset('assets/close.png'),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(24),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      QuestrialRegularText("Hint", 24, "#404040"),
                      SizedBox(height: 16),
                      Container(height: 1, color: hexToColor("#D3D3D3")),
                      SizedBox(height: 16),
                      QuestrialRegularText(message, 16, "#404040",
                          textAlign: TextAlign.left),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      });
}
