import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:route_observer_mixin/route_observer_mixin.dart';

import 'routes.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    return runApp(MultiProvider(
      providers: [RouteObserverProvider()],
      child: InitialWidget(),
    ));
  });
}

class InitialWidget extends StatelessWidget {
  static GlobalKey<NavigatorState> navKey = GlobalKey(debugLabel: "nav_key");
  static Map<int, Color> color = {
    50: Color.fromRGBO(46, 18, 36, 1),
    100: Color.fromRGBO(46, 18, 36, 1),
    200: Color.fromRGBO(46, 18, 36, 1),
    300: Color.fromRGBO(46, 18, 36, 1),
    400: Color.fromRGBO(46, 18, 36, 1),
    500: Color.fromRGBO(46, 18, 36, 1),
    600: Color.fromRGBO(46, 18, 36, 1),
    700: Color.fromRGBO(46, 18, 36, 1),
    800: Color.fromRGBO(46, 18, 36, 1),
    900: Color.fromRGBO(46, 18, 36, 1),
  };
  MaterialColor primeColor = MaterialColor(0xFF2E1224, color);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        navigatorObservers: [RouteObserverProvider.of(context)],
        navigatorKey: navKey,
        theme: ThemeData(
          primarySwatch: primeColor,
          primaryColor: Colors.black,
          accentColor: Color.fromARGB(255, 66, 147, 33),
        ),
        initialRoute: "/",
        onGenerateRoute: RouteGenerator.generateRoute
        //ds
        );
  }
}
