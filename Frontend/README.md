### LingAfriq technical info

#### This project is powered by [Flutter 1.20.0 channel master](https://flutter.dev/) & [Dart 2.9.0](https://dart.dev/)

**Modules:**

 - **Assets** - static images
 - **Fonts** - fonts used in the application
 - **Components** -  that make main structure and logic of the application
    - *main.dart* - starting point for a Flutter application
	- *routes.dart* - app routing
    - *interactor package* - requests to the backend
    - *models package* - all models of objects
	- *screens package* - app screens
	- *ui_elements package* - specific ui elements
	- *utils package* - app utils
 - **Dependencies**:

     - *[provider: ^4.0.4](https://pub.dev/packages/provider)*
     - *[shared_preferences: ^0.5.7+3](https://pub.dev/packages/shared_preferences)*
     - *[route_observer_mixin: ^1.2.0](https://pub.dev/packages/route_observer_mixin)*
     - *[http: ^0.12.0+4](https://pub.dev/packages/http)*
     - *[flutter_svg: ^0.17.1](https://pub.dev/packages/flutter_svg)*
     - *[url_launcher: ^5.5.0](https://pub.dev/packages/url_launcher)*
     - *[http_interceptor: ^0.2.0](https://pub.dev/packages/http_interceptor)*
     - *[audioplayers: '0.15.1'](https://pub.dev/packages/audioplayers)*
     - *[chewie: ^0.9.10](https://pub.dev/packages/chewie)*
     - *[video_player: ^0.10.11+2](https://pub.dev/packages/video_player)*
     - *[flutter_launcher_icons: "^0.7.3"](https://pub.dev/packages/flutter_launcher_icons)*
     - *[firebase_core: ^0.4.5](https://pub.dev/packages/firebase_core/versions)*
     - *[flutter_picker](github.com/yangyxd/flutter_picker.git)*

***

